SET mypath=%~dp0
cd %mypath:~0,-1%
cd src
rem pyinstaller --distpath ../dist --workpath ../build --icon=logo.ico --noconsole Wicoti.py
pyinstaller --distpath ../dist --workpath ../build --icon=logo.ico Wicoti.py

cd ../dist
rm Wicoti.zip
zip -r Wicoti.zip .
