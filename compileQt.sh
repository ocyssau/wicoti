#!/bin/bash
#http://pyqt.sourceforge.net/Docs/PyQt5/designer.html
pyuic5 ./QtDesigner/MainWindow.ui > src/MainWindowQt5.py
pyuic5 ./QtDesigner/ConfigDialogBox.ui > src/ConfigDialogBoxQt5.py
pyuic5 ./QtDesigner/ConnexionDialogBox.ui > src/ConnexionDialogBoxQt5.py

#pyrcc5
pyrcc5 ./QtDesigner/Main.qrc > src/Main_rc.py
