#!/usr/bin/python
from Connector.Catia import ConverterFactory
#from Converter import Catia as CatiaConverter
from Application.Catia import CatiaApplication, ProductWalker
#import os
import tempfile
import os

# #
#
class CatiaConverterTest():
    
    ##
    #
    def converterFactoryTest(self):
        ##converter = getattr(CatiaConverter, "ProductinstanceToRanchbe")
        ##converter = CatiaConverter.ProductinstanceToRanchbe
        converter = ConverterFactory.getConverter("catproduct", "ranchbe")
        converter.witness = 1
        converter = ConverterFactory.getConverter("catproduct", "ranchbe")
        assert(converter.witness == 1)

    ##
    #
    def productToRanchbeTest(self):
        ##Application.Catia.Catia
        catia = CatiaApplication.get()
        if catia.connected == False:
            catia.connect()
        
        ## Load children in productDocument.Product instance
        ## Application.Catia.ProductDocument
        productDocument = catia.getActiveDocument()
        if len(productDocument.getProduct().children) == 0:
            walker = ProductWalker(productDocument)
            walker.loadChildren(productDocument.getProduct())
        converter = ConverterFactory.getConverter("catproduct", "ranchbe")
        try:
            rbData = converter.convert(productDocument)
            print(rbData.toJson())
        except Exception as e:
            print(str(e))

    ##
    #
    def tempFileTest(self):
        ## @var SpooledTemporaryFile file
        file = tempfile.SpooledTemporaryFile(mode='w+')
        print(str(file.name))
        file.close()
        
        file = tempfile.TemporaryFile(mode='w+', suffix="stp")
        print(file.name)
        file.close()
        
        file = tempfile.NamedTemporaryFile(mode='w+', suffix="igs")
        print(file.name)
        file.write("Bye bye world!")
        print(os.path.isfile(file.name)) #True
        file.close()
        print(os.path.isfile(file.name)) #False

    ##
    #
    def uuidTest(self):
        import uuid
        uid = uuid.uuid4()
        print(uid)
        filename = uuid.uuid4().hex
        print(filename)

    ##
    #
    def productWalkerTest(self):
        ##Application.Catia.Catia
        catia = CatiaApplication.get()
        if catia.connected == False:
            catia.connect()
        
        ## Application.Catia.ProductDocument
        productDocument = catia.getActiveDocument()
        walker = ProductWalker(productDocument)
        children = walker.loadChildren(productDocument.getProduct())
        for child in children:
            print(child.partNumber)
            print(child.fileLink.name)

try:
    myTest = CatiaConverterTest()
    #myTest.productWalkerTest()
    #myTest.tempFileTest()
    #myTest.uuidTest()
    #myTest.converterFactoryTest()
    myTest.productToRanchbeTest()
    
except Exception as e:
    print(str(e))

