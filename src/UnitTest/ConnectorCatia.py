#! /usr/bin/python
from Application.Catia import CatiaApplication
from Connector.Catia import ProductWalker


class ConnectorWalkerTest():


    def connect(self):
        self.catia = CatiaApplication.get()
        self.catia.connect()
    
    ## Type of com object
    def test1(self):
        catia = self.catia
        activeDocument = catia.getActiveDocument()
        product = activeDocument.getProduct()
        
        walker = ProductWalker(activeDocument)
        walker.loadChildren(product)
        
        selection = self.catia.newSelection()
        selection.Clear()
        
        for child in product.children:
            try:
                referenceProduct = child.getReferenceProduct()
                if child.getFileLink().type == ".CATProduct":
                    walker.loadChildren(referenceProduct, child.catiaDepth, child.catiaPath)
                print(referenceProduct.name)
                selection.Add(child.comObject)
            except:
                referenceProduct = None
            print(child.name)
        

test = ConnectorWalkerTest()
test.connect()
test.test1()

