#! /usr/bin/python
from Application.Catia import CatiaApplication, ProductWalker, Product, ProductInstance
import sys
if sys.platform == "win32":
    import win32com.client


##
#
#
#
class CatiaApplicationTest():
    
    def connect(self):
        self.catia = CatiaApplication.get()
        self.catia.connect()
        catia2 = CatiaApplication.get()
        assert(self.catia == catia2)
    
    
    def test1(self):
        catia = self.catia
        catia.setLang("en")
        ver = catia.getVersion()
        print(ver)
        filename = ""
        #catia.readDocument(filename)
        #catia.openDocument(filename)
        #catia.normalizeView
        activeDocument = catia.getActiveDocument()
        print(type(activeDocument))
        #catia.closeAllDocuments()
        #catia.quit()

class ProductInstanceTest():
    
    def testFactory(self):
        catia = CatiaApplication.get()
        if catia.connected == False:
            catia.connect()
        activeDocument = catia.getActiveDocument()
        ret = activeDocument.getArrayCopy()
        print(ret)
        
        product = activeDocument.getProduct()
        ret = product.getArrayCopy()
        print(ret)
        
        catia.products.add(product)
        catia.documents.add(activeDocument)
        product2 = catia.products.get(product.comObject.name)
        assert(product2 == product)
        
        ##Use factory
        product1 = catia.getFactory().getProduct(product.comObject)
        assert(product1 == product)
        
        ## Test with document
        document = catia.getFactory().getDocument(activeDocument.comObject)
        assert(document == activeDocument)
        
        # get children from catia product link to product Instance

    
    ## get children and load reference product
    def test2(self):
        catia = CatiaApplication.get()
        #catiaDocument = catia->readDocument(fromFile).activeDocument
        activeDocument = catia.getActiveDocument()
        product = activeDocument.getProduct()
        ret = product.getArrayCopy()
        print(ret)


class CatiaFactoryAndCollectionTest():
    
    def test1(self):
        catia = CatiaApplication.get()
        if catia.connected == False:
            catia.connect()
            
        activeDocument = catia.getActiveDocument()
        ret = activeDocument.getArrayCopy()
        print(ret)
        
        product = activeDocument.getProduct()
        ret = product.getArrayCopy()
        print(ret)
        
        catia.products.add(product)
        catia.documents.add(activeDocument)
        ''' Objects are indexed by her comObject name '''
        product2 = catia.products.get(product.comObject.name)
        assert(product2 == product)
    
    def testDocument(self):
        catia = CatiaApplication.get()
        if catia.connected == False:
            catia.connect()
            
        activeDocument = catia.getActiveDocument()
        ret = activeDocument.getArrayCopy()
        print(ret)
        
        ## Test with document
        document = catia.getFactory().getDocument(activeDocument.comObject)
        assert(document == activeDocument)
        
    def testProduct(self):
        catia = CatiaApplication.get()
        if catia.connected == False:
            catia.connect()
        
        activeDocument = catia.getActiveDocument()
        product = activeDocument.getProduct()
        
        product2 = catia.products.get(product.comObject.name)
        assert(product2 == product)
        
        ## Use factory
        product1 = catia.getFactory().getProduct(product.comObject)
        assert(product1 == product)
        
    
    ## get children and load reference product
    def productWalkerTest(self):
        catia = CatiaApplication.get()
        if catia.connected == False:
            catia.connect()
        
        activeDocument = catia.getActiveDocument()
        product = activeDocument.getProduct()
        product.populate()
        
        #child = ProductInstance(product.comObject.Products.item(3))
        #product.children.append(child)
        
        assert(product.__class__ == Product)
        
        walker = ProductWalker()
        walker.loadChildren(product)
        
        for i in range(0,len(product.children)):
            child = product.children[i]
            try:
                referenceProduct = child.getReferenceProduct()
                print(referenceProduct.name)
            except:
                referenceProduct = None
            print(child.name)



# # Super class for applications that been acceded through DCOM service
#    
class CatiaCom():
    
    comApplication = None
    
    ##
    #
    def connect(self):
        if not self.comApplication:
            self.comApplication = win32com.client.Dispatch("Catia.application")

    ## Type of com object
    def test3(self):
        self.comApplication.ActiveDocument
        ti = self.comApplication._oleobj_.GetTypeInfo()
        clsid = ti.GetTypeAttr()[0]
        print(clsid)
        pass


class CatiaSelectionInTree():


    def connect(self):
        self.catia = CatiaApplication.get()
        self.catia.connect()
    
    ## Type of com object
    def test1(self):
        catia = self.catia
        activeDocument = catia.getActiveDocument()
        product = activeDocument.getProduct()
        
        walker = ProductWalker()
        walker.loadChildren(product, 1, "/")
        
        for child in product.children:
            try:
                referenceProduct = child.getReferenceProduct()
                if child.getFileLink().type == ".CATProduct":
                    walker.loadChildren(referenceProduct, child.catiaDepth, child.catiaPath)
                print(referenceProduct.name)
            except:
                referenceProduct = None
            print(child.name)


        selection = self.catia.newSelection()
        selection.Clear()
        
        instance = self.factoryFromPath("/2/1/", activeDocument)
        selection.Add(instance.comObject)


        '''
        selection = self.catia.newSelection()
        selection.Add(productInstance.comObject)
        count = selection.Count
        self.catia.comApplication.StartCommand("Load")
        selection.Clear()
        '''


    ## Type of com object
    def factoryFromPathTest(self):
        activeDocument = self.catia.getActiveDocument()
        instance = self.factoryFromPath("/2/1/", activeDocument)
        print(instance)
        
        
    # # Factory for build instance
    #
    # @param string path String as id/id/id wheres id are Item index of the parents in Products relations
    # @param Application.Catia. ProductDocument rootComDocument
    #
    @staticmethod
    def factoryFromPath(path, rootDocument) :
        print(path)
        pathAsArray = path.strip('/').strip().split('/')
        comRootProducts = rootDocument.comObject.Product.Products
        
        comProducts = comRootProducts
        for itemId in pathAsArray :
            if (itemId == ''): 
                continue
            comInstance = comProducts.Item(int(itemId))
            print(comInstance.Name)
            comProducts = comInstance.Products

        instance = ProductInstance(comInstance)
        instance.catiaPath = path
        return instance
        
    # #
    #
    def selectFromCatiaTreeTest(self) :
        selection = self.catia.comApplication.ActiveDocument.Selection
        c = selection.Count2
        ## @var treeItem QTreeWidgetItem
        for i in range(1, c+1):
            item = selection.Item2(i)
            if(item.Type == "Product"):
                productInstance = item.LeafProduct
                ''' get path from child to head '''
                parent = productInstance.parent.parent
                selection.add(parent)
                if(selection.Count2 == c):
                    break;
                print(selection.Count2)
                print(selection.item2(c+1).Type)
                while selection.item2(c+1).Type == "Product":
                    parent = parent.parent
                    selection.remove2(c+1)
                    selection.add(parent)
        

#test = CatiaApplicationTest()
#test.connect()
#test.test1()

test = ProductInstanceTest()
#test.test2()
#test.testFactory()

test = CatiaFactoryAndCollectionTest()
#test.test1()
#test.testDocument()
#test.testProduct()
#test.productWalkerTest()

test = CatiaSelectionInTree()
test.connect()
#test.test1()
#test.factoryFromPathTest()
test.selectFromCatiaTreeTest()


#catia = CatiaCom()
#catia.connect()
#catia.test3()
