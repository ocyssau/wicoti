import os
import uuid
from builtins import staticmethod
from Application.Catia import ProductInstance

##
#
#
class Catia():

	def __init__(self):
		self.outputFormat = False
		self.outputExtension = False
		self.inputFile = False
		self.outputFile = False
		self.data = ""  # (String)

	def setInputFile(self, file):
		self.inputFile = file
		return self

	def setOutputFile(self, file):
		self.outputFile = file
		return self

	def setOutputExtension(self, extension):
		self.outputFormat = self._getFormat(extension)
		self._outputExtension = extension
		return self

	def getInputFile(self):
		return self.inputFile

	def getOutputFile(self):
		if(not self.outputFile):
			self.outputFile = self.inputFile + '.' + self._outputExtension
		return self.outputFile

	def _getFormat(self, extension):
		return False

	def _getCatia(self):
		if(not self.application):
			self.connect()
		return self.application

	def getResult(self):
		return self.data

##
#
#
class RbPdmData():

	def __init__(self):
		self.id = uuid.uuid4().hex

		self.jsonFilename = self.id + ".json"
		self.stepFilename = self.id + ".stp"
		self.jpgFilename = self.id + ".jpg"
		self.igsFilename = self.id + ".igs"
		self.stlFilename = self.id + ".stl"
		
		self.datas = {
			"fromType": "",
			"toType": "",
			"files": [],
			"properties": {
				"fullName": "",
				"name": "",
				"number": "",
				"path": "",
				"product": {
					"name": "",
					"number": "",
					"nomenclature": "",
					"version": "",
					"definition": "",
					"description": ""
				}
			},
			"children": {},
			"links": {}
		}
		
		self.linksPrototype = {
			"isComponent": False,
			"isRoot": True,
			"linType": "CATLinkTypeProduct",
			"lnk": "",
			"instance": ""
		}
		
		self.childPrototype = {
			"name": "",
			"ofProductId": None,
			"ofProductUid": None,
			"ofProductName": "",
			"ofProductNumber": "",
			"ofDocumentName": "",
			"nomenclature": "",
			"quantity": None,
			"description": "",
			"position": None
		}
		
		self.feedbacks = []
		self.errors = []
		
	def toJson(self, pretty=True):
		''' @return string '''
		import json
		if pretty:
			return json.dumps(self.datas, sort_keys=False, indent=6)
		else:
			return json.dumps(self.datas)

	def setData(self, name, datas):
		'''
		@param string name
		@param mixed datas
		@return RbPdmData
		'''
		self.datas[name] = datas;
		return self

	def getDatas(self):
		'''
		@return dict
		'''
		return self.datas
	
	def saveTo(self, path):
		'''
		Save the Pdm data set to files in path
		'''
		toFile = os.path.join(path, self.jsonFilename)
		jsonData = self.toJson()
		f = open(toFile, "w")
		f.write(jsonData)
		f.close()
		return toFile

##
#
#
class ProductinstanceToRanchbe(Catia):

	def __init__(self):
		super().__init__()
		self.fromType = 'productinstance'
		self.toType = 'ranchbe'

	# #
	#
	# @var Application.Catia.Product product
	#
	def convert(self, product):
		result = RbPdmData()
		result.setData('fromType', self.fromType)
		result.setData('toType', self.toType)
		
		result.setData('files', [
			result.jsonFilename,
			result.stepFilename,
		])
		
		try:
			result.setData('properties', product.getArrayCopy())
			
			# get children from catia product link to product Instance
			try:
				product.getReferenceProduct()
			except:
				if len(product.getReferenceProduct().children) > 0 :
					children = product.getReferenceProduct().children
					pass
			
			# Get links to loaded file from catia app
			links = product.getProductInstanceLinks()  # list
			
			result.setData('children', children)
			result.setData('links', links)

		except Exception as e:
			print("Unable to export properties : ", str(e))
			raise e
		
		try:
			product.toStep(result.stepFilename)
			result.setData('stepdatas', open(result.stepFilename, "r").read())
			result.setData('step', {
				'file' : result.stepFilename,
				'type' : 'step'
			})
		except Exception as e:
			print("Unable to export to step : ", str(e))
			raise e
		
		return result

##
#
#
#
class CatproductToRanchbe(Catia):

	# #
	#
	#
	def __init__(self):
		super().__init__()
		self.fromType = 'catproduct'
		self.toType = 'ranchbe'

	# #
	#
	# @var Application.Catia.ProductDocument productDocument
	#
	def convert(self, productDocument, withStep = False):
		result = RbPdmData()
		result.setData('fromType', self.fromType)
		result.setData('toType', self.toType)
		
		result.setData('files', [
			result.jsonFilename,
			result.stepFilename,
		])
		
		try:
			result.setData('properties', productDocument.getArrayCopy())
			
			# get children from catia product link to product Instance
			# @var children Application.Catia.Instances
			children = productDocument.getProduct().getChildren()
			
			# Get links to loaded file from catia app
			# @var links Application.Catia.FileLinks
			#links = productDocument.getLinks()
			
			result.setData('children', self.serializeChildren(children.toDict()))
			#result.setData('links', links.toDict())

		except Exception as e:
			raise Exception("Error when trying to export properties of %s : %s: " % (productDocument.name, str(e)))
		
		if withStep == True :
			try:
				productDocument.toStep(result.stepFilename)
				result.setData('stepdatas', open(result.stepFilename, "r").read())
				result.setData('step', {
					'file' : result.stepFilename,
					'type' : 'step'
				})
			except:
				raise Exception("Error when trying to generate STEP : "+str(e))
		
		return result
	
	# #
	#
	@staticmethod
	def serializeChildren(children):
		result = []
		for child in children:
			result2 = None
			children2 = None
			if(isinstance(child, ProductInstance)):
				serialized = child.getArrayCopy()
				if(child.isComponent()):
					if(child.getReferenceProduct().getChildren().count() == 0):
						child.productWalker.loadChildren(child, child.catiaDepth, child.catiaPath)
					children2 = child.getReferenceProduct().getChildren()
					result2 = CatproductToRanchbe.serializeChildren(children2.toDict())
					serialized['children'] = result2
				
				result.append(serialized)
		return result
	
##
#
#
#
class CatpartToRanchbe(Catia):
	
	def __init__(self):
		super().__init__()
		self.fromType = 'catpart'
		self.toType = 'ranchbe'

	# #
	# @var Catia.PartDocument part 
	def convert(self, partDocument):
		result = RbPdmData()
		result.setData('fromType', self.fromType)
		result.setData('toType', self.toType)
		
		try :
			partDocument.loadPhysicalProperties()
		except Exception as e:
			result.feedbacks.append(str(e))
			
		try:
			partDocument.loadMaterials()
		except Exception as e:
			result.feedbacks.append(str(e))
			
		try:
			partDocument.loadGravityCenter()
		except Exception as e:
			result.feedbacks.append(str(e))
		
		result.setData('properties', partDocument.getArrayCopy())
		
		try:
			partDocument.toJpg(result.jpgFilename)
			result.setData('picture', {
				'file' : result.jpgFilename,
				'type' : 'jpg',
				'data' : open(result.jpgFilename, "r").read()
			})
		except Exception as e:
			result.errors.append(str(e))
		
		# # Convert 3d Igs */
		try:
			partDocument.toIgs(result.igsFilename)
			result.setData('convert', {
				'file' : result.igsFilename,
				'type' : 'igs',
				'data' : open(result.igsFilename, "r").read()
			})
		except Exception as e:
			result.errors.append(str(e))
		
		# # Convert 3d STL */
		try:
			partDocument.toStl(result.stlFilename)
			result.setData('3d', {
				'file' : result.stlFilename,
				'type' : 'stl',
				'data' : open(result.stlFilename, "r").read()
			})
		except Exception as e:
			result.errors.append(str(e))
		
		return result

##
#
#
class CatpartToJpg(Catia):
	
	def convert(self, partDocument, extension):
		try:
			if(extension=='jpg'):
				partDocument.toJpg()
			elif(extension=='wrl'):
				partDocument.toWrl()
			elif(extension=='stl'):
				partDocument.toStl()
			elif(extension=='igs'):
				partDocument.toIgs()
		except Exception as e:
			print(str(e))
