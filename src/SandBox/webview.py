# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qtWebEngine_example.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets, QtNetwork, QtWebEngineCore, QtWebEngineWidgets
from pprint import pprint

class Ui_MainWindow(object):
    
    cookies = []
    webView = None
    
    def connect(self, MainWindow):
        MainWindow.resize(1200, 600)
        widget = QtWidgets.QWidget(MainWindow)
        webView = self.getWebview()
        ## QWebEnginePage
        webView.page().loadFinished.connect(self.loadFinishedHandle)
        profile = webView.page().profile()
        profile.downloadRequested.connect(self.downloadHandle)
        
        qurl = QtCore.QUrl("http://rbsier/auth/authenticate")
        query = QtCore.QUrlQuery()
        query.addQueryItem('username', 'admin')
        query.addQueryItem('password', 'admin00')
        query.addQueryItem('rememberme', '1')
        qurl.setQuery(query)
        
        print('QUERY:',qurl.query())
        
        request = QtWebEngineCore.QWebEngineHttpRequest()
        request.setUrl(qurl)
        request.setMethod(QtWebEngineCore.QWebEngineHttpRequest.Post)
        request.setHeader(b'Content-type', b'application/x-www-form-urlencoded')
        
        a = QtCore.QByteArray()
        print(query.query())
        a.append(query.query())
        request.setPostData(a)
        pprint(request.postData())
        
        cookieStore = self.webView.page().profile().cookieStore()
        cookieStore.deleteAllCookies()
        self.getCookies()
        
        webView.load(request)
        webView.show()
        
    ## @param QWebEngineDownloadItem
    def downloadHandle(self, download):
        print("download")
        
        old_path = download.path()
        suffix = QtCore.QFileInfo(old_path).suffix()
        path, _ = QtWidgets.QFileDialog.getSaveFileName(self.webView, "Save File", old_path, "*."+suffix)
        if path:
            download.setPath(path)
            download.accept()        
        
    def downloadProgressHandle(self, int, int2):
        print("progress "+str(int))
        
    def loadFinishedHandle(self, ok):
        print("finished")
        #self.webView.close()
        
    def getCookies(self):
        # Cookies
        profile = self.webView.page().profile()
        # @var cookieStore QWebEngineCookieStore
        cookieStore = profile.cookieStore()
        cookieStore.cookieAdded.connect(self.onCookieAdded)
        #cookieStore.loadAllCookies()
        
    def onCookieAdded(self, cookie):
        print('cookie added')
        print("name",bytearray(cookie.name()).decode())
        print("value",bytearray(cookie.value()).decode())
        print("path",cookie.path())
        print("domain",cookie.domain())
        print("session cookie",cookie.isSessionCookie())
        print("expirationDate",cookie.expirationDate().toString())
        print("secure",cookie.isSecure())
        print("httponly",cookie.isHttpOnly())
        for c in self.cookies:
            if c.hasSameIdentifier(cookie):
                return
        self.cookies.append(QtNetwork.QNetworkCookie(cookie))
        self.toJson()

    def toJson(self):
        cookies_list_info = []
        for c in self.cookies:
            data = {"name": bytearray(c.name()).decode(), "domain": c.domain(), "value": bytearray(c.value()).decode(),
                    "path": c.path(), "expirationDate": c.expirationDate().toString(), "secure": c.isSecure(),
                    "httponly": c.isHttpOnly()}
            cookies_list_info.append(data)
        print("Cookie as list of dictionary:")
        print(cookies_list_info)
    
    def getWebview(self):
        if self.webView == None:
            self.webView = QtWebEngineWidgets.QWebEngineView()
            self.webView.setObjectName("webView")
        return self.webView
    
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        webView = self.getWebview()
        webView.setParent(self.centralwidget)
        self.gridLayout.addWidget(webView, 0, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))

from PyQt5 import QtWebEngineWidgets

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    #ui.setupUi(MainWindow)
    #MainWindow.show()
    ui.connect(MainWindow)
    #ui.webView.load(QtCore.QUrl("http://rbsier/workplace/wildspace/index/"))


sys.exit(app.exec_())

