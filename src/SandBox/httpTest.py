#!/usr/bin/python
#import sys
#from PyQt5 import QtCore, QtGui, QtWidgets, QtNetwork
#from PyQt5.QtCore import QCoreApplication, QUrl
#from PyQt5.QtCore import *
#from PyQt5.QtGui import *
#from PyQt5.QtWidgets import *
#from PyQt5.QtWebEngineWidgets import *
#import http.client

import json
import requests
#from requests import Request, Session
import os

from Connector.Ranchbe import Ranchbe
from PyQt5.QtCore import QUrl, QUrlQuery
from PyQt5 import QtNetwork
from Main import Config

# #
#
#
#
class HttpTest():
    
    ranchbe = None
    
    ##
    #
    def connectToRanchbe(self):
        config = Config(os.path.realpath("../config.ini"))
        self.ranchbe = Ranchbe()
        self.ranchbe.setConfig(config)
        qurl = self.ranchbe.getConnectQurl('admin', 'admin00')
        connexion = self.ranchbe.restService()
        connexion.connect(qurl)

    ##
    #
    def jsonRequest(self):
        restService = self.ranchbe.restService()
        
        data = {"docfiles":{
            "docfile1":{"name": "Product1", "parent":{"description": "FOR TEST"}},
            "docfile2":{"name": "Product2", "parent":{"description": "FOR TEST 2"}}
            }}
        
        qurl = self.ranchbe.getQurl("/application/ping")
        #qurl = self.ranchbe.getQurl("/service/application/ping/post")
        
        r = restService.post(qurl, json.dumps(data), options={"dataType":"json"})
        print(r.text)


    ##
    #
    def postRequest(self):
        restService = self.ranchbe.restService()
        
        qurl = self.ranchbe.getQurl("/service/application/ping/post")
        query = QUrlQuery()
        query.addQueryItem("basecamp", "/home/splash")
        query.addQueryItem("layout", "popup")
        qurl.setQuery(query)
        #session = restService.restRequest
        #r = session.request('post', qurl.toString(), data={"jsonData":json.dumps(data)}, headers=headers)
        
        r = restService.post(qurl)
        print(r.text)


    ##
    #
    def getRequest(self):
        restService = self.ranchbe.restService()
        
        qurl = self.ranchbe.getQurl("/service/application/ping/post")        
        query = QUrlQuery()
        query.addQueryItem("basecamp", "/home/splash")
        query.addQueryItem("layout", "popup")
        qurl.setQuery(query)
        
        r = restService.get(qurl)
        print(r.text)
    

    ##
    #
    def HTTPConnection(self):
        '''
        connexion = http.client.HTTPConnection('ranchbe.sierbla.int', 80, timeout=10)
        connexion = http.client.HTTPConnection("daguerre3.sierbla.int")
        connexion.request("GET", "/ranchbe/home")
        response = connexion.getresponse()
        print(response.status, response.reason)
        print(response.read())
        '''
        pass


    def qtnetwork(self):
        '''
        if er == QtNetwork.QNetworkReply.NoError:
            bytesString = reply.readAll()
            print(str(bytesString, 'utf-8'))
            
        else:
            print("Error occured: ", er)
            print(reply.errorString())
            
        QCoreApplication.quit()    
        '''
        url = "http://www.something.com"
        req = QtNetwork.QNetworkRequest(QUrl(url))
        nam = QtNetwork.QNetworkAccessManager()
        #nam.finished.connect(handleResponse)
        nam.get(req)

        pass

    ##
    #
    def requestsLib(self):
        
        #params = {'password': 'password', 'username': 'o_cyssau', 'rememberme': 1, 'submit': 'Signin'}
        
        url = 'http://localhost?pass=otot&user=totot&remember=1&submit=1'
        r = requests.get(url)
        print(r.url)
        #print(r.status_code)
        #print(r.headers)
        #print(r.encoding)
        #print(r.json)
        #print(r.text)
        
        #url = 'http://daguerre3.sierbla.int/ranchbe/home'
        #r = requests.post(url)
        #print(r.status_code)
        #print(r.headers)
        #print(r.encoding)
        #print(r.json)
        #print(r.text)
        

myTest = HttpTest()
myTest.connectToRanchbe()
#myTest.getRequest()
#myTest.postRequest()
#myTest.jsonRequest()


