

## SOAP CLIENT for Ranchbe SOAP Service
# NOT USED


'''

[soapclient]
options.trace = 1
options.soap_version = 2
options.cache_wsdl = 0
options.encoding = ISO-8859-1
'''

class ServiceClient():
    registry = list()
    connexionFailed=False
    
    ##
    #
    def __init__(self,serviceName,config):
        userHandle  = config.get('rbserver','user')
        passwd      = config.get('rbserver','password')
        soapOptions = config.get('soapclient')
        serverUrl   = config.get('rbserver','url')
        
        #//Create a soap var from UserPass class
        soapAuthenticator = SoapVar(User(userHandle, passwd), SOAP_ENC_OBJECT, 'UserPass')
        authHeader = SoapHeader("http://schemas.xmlsoap.org/ws/2002/07/utility","headerAuthentify",soapAuthenticator,false)
        
        serviceName = 'document'
        wsdl = "$serverUrl/$serviceName"
        
        print('Try to connect to ' + wsdl + "\n")
        if self.connexionFailed == True:
            raise Exception('connexion failed')
        super().__init__(wsdl, soapOptions)
        print('Successful connexion to ' + wsdl)
        self.__setSoapHeaders(authHeader)
        
    ##
    #
    def singleton(self,serviceName):
        serviceName = serviceName.lower
        if not self.registry[serviceName]:
            try:
                self.registry[serviceName] = SoapClient(serviceName)
            except:
                print( 'Connexion to '+serviceName+' failed ' + "\n")
                self.connexionFailed = True
        return self.registry[serviceName]
