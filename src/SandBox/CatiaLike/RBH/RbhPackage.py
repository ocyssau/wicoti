#! /usr/bin/env python
class Ou:
    children =[]
    parent = None
    name=""
    def __init__(self, name, parent=""):
        self.name = name
        self.children = []
        if isinstance(parent,Ou):
            self.parent=parent
            parent.children.append(self)
        else:
            App=Application()
            App.children.append(self)
    def addChild(self, child):
        self.children.append(child)
class Application(Ou):
    ous=[]
    instance = None
    
    def __new__(classe, *args, **kargs):
        if classe.instance is None:
            classe.instance = object.__new__(classe, *args, **kargs)
            classe.instance.name="rbh"
        return classe.instance

    def __init__(self):
        pass

    @staticmethod
    def getChildByName(obj,name):
        for n in obj.children:
            if n.name==name:
                return n

    @staticmethod
    def getChildById(obj,uid):
        for n in obj.children:
            if id(n)==uid:
                return n

    def getByPathByName(self,path):
        obj=self
        for name in path:
            obj=Application.getChildByName(obj,name)
        return obj

    def getByPathById(self,path):
        obj=self
        for uid in path:
            obj=Application.getChildById(obj,uid)
        return obj

class Section(Ou):
    surface=0
    IGx=0
    IGy=0
    IGz=0
    Perimeter=0
class Packaging(Ou):
    salesUnit="m"
    weight=0
    volume=0
    surface=0
    section=0
    length=0
    width=0
    thickness=0
class Recette(Ou):
    ingredient=[]
    qty=[]
    unit=[]
    materials=[]
    volumeEnd=0
    def setForVolume(self,vol):
        self.volumeEnd = vol
    def addIngredient(self,name,qty,unit,material):
        if isinstance(material,Material):
            self.ingredient.append(name)
            self.qty.append(qty)
            self.unit.append(unit)
            self.materials.append(material)
        else:
            print material , "is not authorized"
    def getIngredient(self,name):
        i=self.ingredient.index(name)
        ret=[]
        ret.append(self.qty[i])
        ret.append(self.unit[i])
        ret.append(self.materials[i])
        return ret
class BuildSystem(Ou):
    Materials=[]
    RefSurface=None
class BuildSurface(Ou):
    surface=0
    thickness=0
    volume=0
    azimut=0
    hauteur=0
    layers=[]
    material=0
    def setOrientation(self,azimut,hauteur):
        self.azimut=azimut
        self.hauteur=hauteur
    def newLayer(self,name,pos=-1):
        if pos < 0:
            pos=len(self.layers)
        self.layers.insert(pos,BuildSurface(name,self))
        return self.layers[pos]
    def addLayer(self,layer,pos=-1):
        if pos >= 0:
            pos=len(self.layers)
        self.layers.insert(pos,layer)
    def setMaterial(self,mat):
        self.children.append(mat)
        self.material=mat
class function(Ou):
    strFun=""
    def setFunc(fun):
        self.fun=fun
class functionProcessor(Ou):
    @staticmethod
    def run(function):
        print function
class Parameter(Ou):
    value=0
    unit="m"
