import RbhPackage
class Used(RbhPackage.Ou):
    material=""
    packaging=""
    weight=0
    volume=0
    density=0
class Lineic(Used):
    length=0
    section=0
class Surfacic(Used):
    length=0
    width=0
    surface=0
    section=0
    thickness=0
class Bulk(Used):
    pass
class Beton(Bulk):
    materials=[]
    recette=0
    qty=[]
    def setRecette(self,recette):
        self.recette=recette
    def getMaterial(self,name):
        for mat in self.materials:
            if mat.name == name:
                return mat
    def setVolume(self,vol):
        self.volume=vol
    def prepare(self):
        self.weight=0
        self.density=0
        self.qty=[]
        i=0
        for matname in self.recette.ingredient:
            mat=self.recette.materials[i]
            self.materials.append(mat.newUsed())
            i=i+1
        for mat in self.materials:
            self.weight=self.weight + mat.weight
            rec=self.recette.getIngredient(mat.name)
            if rec[1] == "m3":
                mat.volume=rec[0]*(self.volume/self.recette.volumeEnd)
            if rec[1]=="kg":
                mat.weight=rec[0]*(self.volume/self.recette.volumeEnd)
        self.density=self.weight/self.volume





