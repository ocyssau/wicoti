import RbhPackage

class Material(RbhPackage.Ou):
    packagings=[]
    material=""
    density=0
    thChaleurspe=0
    thLambda=0
    def newUsed(self):
        umat=UsedMaterial(self.name,self)
        umat.material=self
        return umat

class Lineic(Material):
    section=0
    def newUsed(self):
        umat=UsedLineicMaterial(self.name,self)
        umat.material=self
        return umat

class Surfacic(Material):
    thickness=0
    def newUsed(self):
        umat=UsedSurfacicMaterial(self.name,self)
        umat.material=self
        return umat

class Bulk(Material):
    def newUsed(self):
        umat=UsedBulkMaterial(self.name,self)
        umat.material=self
        return umat

