#! /usr/bin/env python
import rbh

projet = rbh.Ou("projet")

pmciment=rbh.BulkMaterial("ciment",projet)
pmciment.density=1250
pmsable04=rbh.BulkMaterial("sable0-4",projet)
pmsable04.density=1400
pmsable410=rbh.BulkMaterial("sable4-10",projet)
pmsable410.density=1300
pmgravier1040=rbh.BulkMaterial("gravier10-40",projet)
pmgravier1040.density=1400

dalle = rbh.Ou("dalle", projet)

recette=rbh.Recette("Beton250", projet)
recette.setForVolume(1)
recette.addIngredient("ciment",250,"kg",pmciment)
recette.addIngredient("sable0-4",0.5,"m3",pmsable04)
recette.addIngredient("gravier10-40",0.5,"m3",pmgravier1040)

beton = rbh.Beton("beton",dalle)
beton.setVolume(5)
beton.setRecette(recette)
beton.setVolume(15)
beton.prepare()

print beton.density

"Bilan"
def childFilter(ou, className):
    ret=[]
    for child in ou.children:
        if isinstance(child, className):
            ret.append(child)
        if isinstance(child, rbh.Ou):
            ret.extend(childFilter(child,className))
    return ret

for mat in childFilter(projet, rbh.UsedMaterial):
    if isinstance(mat, rbh.UsedMaterial):
        print mat.name, mat.weight, mat.volume
    else:
        print mat.name

projet = rbh.Ou("projet")
fondation = rbh.BuildSurface("dalle", projet)
fondation.setOrientation(180,0)
fondation.surface=25
herrisson=fondation.newLayer("herrisson")
herrisson.setMaterial(rbh.BulkMaterial("gravier",herrisson))
dalle=fondation.newLayer("dalle")
dalle.setMaterial(beton)
chappe=fondation.newLayer("chappe")

recette=rbh.Recette("Beton200", projet)
recette.setForVolume(1)
recette.addIngredient("ciment",200,"kg",pmciment)
recette.addIngredient("sable0-4",0.5,"m3",pmsable04)
recette.addIngredient("gravier10-40",0.5,"m3",pmgravier1040)

beton2=rbh.Beton("beton200",herrisson)
beton2.setRecette(recette)
beton.setVolume(4.8)
beton.prepare()

chappe.setMaterial(rbh.Beton("beton200",herrisson))

for mat in childFilter(projet, rbh.UsedMaterial):
    if isinstance(mat, rbh.UsedMaterial):
        print mat.name, mat.weight, mat.volume
    else:
        print mat.name







