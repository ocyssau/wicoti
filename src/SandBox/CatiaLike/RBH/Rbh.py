#! /usr/bin/env python
class Ou:
    children =[]
    parents=[]
    name=""
    def __init__(self, name, parent=""):
        self.name = name
        self.children = []
        self.parents.append(parent)
        if isinstance(parent,Ou):
            parent.children.append(self)
    def addChild(self, child):
        self.children.append(child)
class Section(Ou):
    surface=0
    IGx=0
    IGy=0
    IGz=0
    Perimeter=0
class Material(Ou):
    packagings=[]
    material=""
    density=0
    thChaleurspe=0
    thLambda=0
    def newUsed(self):
        umat=UsedMaterial(self.name,self)
        umat.material=self
        return umat
class LineicMaterial(Material):
    section=0
    def newUsed(self):
        umat=UsedLineicMaterial(self.name,self)
        umat.material=self
        return umat
class SurfacicMaterial(Material):
    thickness=0
    def newUsed(self):
        umat=UsedSurfacicMaterial(self.name,self)
        umat.material=self
        return umat
class BulkMaterial(Material):
    def newUsed(self):
        umat=UsedBulkMaterial(self.name,self)
        umat.material=self
        return umat
class Packaging(Ou):
    salesUnit="m"
    weight=0
    volume=0
    surface=0
    section=0
    length=0
    width=0
    thickness=0
class UsedMaterial(Ou):
    material=""
    packaging=""
    weight=0
    volume=0
    density=0
class UsedLineicMaterial(UsedMaterial):
    length=0
    section=0
class UsedSurfacicMaterial(UsedMaterial):
    length=0
    width=0
    surface=0
    section=0
    thickness=0
class UsedBulkMaterial(UsedMaterial):
    pass
class Beton(UsedBulkMaterial):
    materials=[]
    recette=0
    qty=[]
    def setRecette(self,recette):
        self.recette=recette
    def getMaterial(self,name):
        for mat in self.materials:
            if mat.name == name:
                return mat
    def setVolume(self,vol):
        self.volume=vol
    def prepare(self):
        self.weight=0
        self.density=0
        self.qty=[]
        i=0
        for matname in self.recette.ingredient:
            mat=self.recette.materials[i]
            self.materials.append(mat.newUsed())
            i=i+1
        for mat in self.materials:
            self.weight=self.weight + mat.weight
            rec=self.recette.getIngredient(mat.name)
            if rec[1] == "m3":
                mat.volume=rec[0]*(self.volume/self.recette.volumeEnd)
            if rec[1]=="kg":
                mat.weight=rec[0]*(self.volume/self.recette.volumeEnd)
        self.density=self.weight/self.volume
class Recette(Ou):
    ingredient=[]
    qty=[]
    unit=[]
    materials=[]
    volumeEnd=0
    def setForVolume(self,vol):
        self.volumeEnd = vol
    def addIngredient(self,name,qty,unit,material):
        if isinstance(material,Material):
            self.ingredient.append(name)
            self.qty.append(qty)
            self.unit.append(unit)
            self.materials.append(material)
        else:
            print material , "is not authorized"
    def getIngredient(self,name):
        i=self.ingredient.index(name)
        ret=[]
        ret.append(self.qty[i])
        ret.append(self.unit[i])
        ret.append(self.materials[i])
        return ret
class BuildSystem(Ou):
    Materials=[]
    RefSurface=1
class BuildSurface(Ou):
    surface=0
    thickness=0
    volume=0
    azimut=0
    hauteur=0
    layers=[]
    material=0
    def setOrientation(self,azimut,hauteur):
        self.azimut=azimut
        self.hauteur=hauteur
    def newLayer(self,name,pos=-1):
        if pos < 0:
            pos=len(self.layers)
        self.layers.insert(pos,BuildSurface(name,self))
        return self.layers[pos]
    def addLayer(self,layer,pos=-1):
        if pos >= 0:
            pos=len(self.layers)
        self.layers.insert(pos,layer)
    def setMaterial(self,mat):
        self.children.append(mat)
        self.material=mat

class function(Ou):
    strFun=""
    def setFunc(fun):
        self.fun=fun
class functionProcessor(Ou):
    @staticmethod
    def run(function):
        print function


