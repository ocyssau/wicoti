# -*- coding: utf-8 -*-
import main

parameters=main.Parameters()
ep=parameters.CreateLength("ep",10)
width=parameters.CreateLength("width",1000)
length=parameters.CreateLength("length",350)
density=parameters.CreateReal("density",1250)
mass=parameters.CreateReal("masse",0)

print(ep.Parent)

relations=main.Relations()
f=relations.CreateFormula("comutmass", "Calcul masse columique", mass,"(ep*height*depth)*density")
print (f)
f.resolve()

import compiler
#eq="(sin(x)*x**2)*8"
eq="(ep*height*depth)*density*cos(5)*sin(8)"
#ast= compiler.parse( eq )
#print ast.getChildren()

import re
regex = re.compile('([\*\+\-\/]{1})|((cos|sin|tan|acos|asin|atan){1})|([0-9]+)|([a-z0-9]+)|([\(\)]{1})')
res=re.match(regex, eq)
print (res.groupdict())

res=re.findall(regex, eq)
for grp in res:
    for w in grp:
        if w != "":
            print (w)

#from Tkinter import *
#fenetre = Tk()
#champ_label = Label(fenetre, text="Salut les Zér0s !")
#champ_label.pack()
#fenetre.mainloop()

def parse(text):
    chunks = ['']
    for character in text:
        if character.isdigit():
            if chunks[-1].isdigit():   # If the last chunk is already a number
                chunks[-1] += character  # Add onto that number
            else:
                chunks.append(character) # Start a new number chunk
        elif character in '+-/*':
            chunks.append(character)  # This doesn't account for `1 ++ 2`.
    return chunks[1:]


