import requests
import re

"""
Get filename from content-disposition
"""
def getFilenameFromHeader(headers):
    cd = headers.get('content-disposition')
    if not cd:
        return None
    fname = re.findall('filename=(.+)', cd)
    if len(fname) == 0:
        return None
    return fname[0]


url = 'http://daguerre3.sierbla.int/ranchbe/shared/2e958c6211f2baaba9caf6686d408c54d4f3f781'
r = requests.get(url, allow_redirects=True)
filename = getFilenameFromHeader(r.headers)
open(filename, 'wb').write(r.content)

