
dict = {0:{0:"id", 1:5, 2:"SUP"}, "toto": "test"}

print(dict.get(0, "default"))
print(dict.get("hola", "default"))
exit()


##### BOUCLE FOR LISTE LES CLEFS
for key in dict:
    print(dict[key])

print(dict)

for key in dict:
    print(key)

print(dict)


array=['val1','val2','val3','val3']
print(array)
print(array[0])
print(array[2])

assocArray={'item1':'value1', 'item2':'value2'}
print(assocArray['item1'])
print(assocArray)

multiDimArray=[[0,1,2],[5,6,7],[5],[5,6,7]]
print(multiDimArray)
print(multiDimArray[0])
print(multiDimArray[0][0])
print(multiDimArray[1])
print(multiDimArray[1][2])

multiDimArray.append('toto')

multiDimArray[0][1]='bibi'
print(multiDimArray[0])

multiDimAssocArray={'item1':{'i11':'v11','i12':'v12'}, 'item2':'value2'}
print(multiDimAssocArray['item1']['i11'])

#array.pop()
print(array.index('val3'))
print(array.count('val2'))





