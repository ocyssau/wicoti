import sys
from PyQt5 import QtGui
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
import sip # Python extension module generator for C and C++ libraries @see https://pypi.org/project/SIP/

class RbServerTree(QMainWindow):
    
    # # 
    #
    def __init__(self, parent=None):
        super (RbServerTree, self).__init__(parent)
        
        l1 = QTreeWidgetItem(["String A", "String B", "String C"])
        l2 = QTreeWidgetItem(["String AA", "String BB", "String CC"])
        
        for i in range(3):
            l1_child = QTreeWidgetItem(["Child A" + str(i), "Child B" + str(i), "Child C" + str(i)])
            l1.addChild(l1_child)
    
        for j in range(2):
            l2_child = QTreeWidgetItem(["Child AA" + str(j), "Child BB" + str(j), "Child CC" + str(j)])
            l2.addChild(l2_child)
    
        w = QWidget(self)
        w.resize(510, 210)
    
        treeWidget = QTreeWidget(w)
        treeWidget.itemExpanded.connect(self.handle)    
        treeWidget.itemCollapsed.connect(self.handleCollapse)    
        treeWidget.resize(500, 200)
        treeWidget.setColumnCount(3)
        treeWidget.setHeaderLabels(["Column 1", "Column 2", "Column 3"])
        treeWidget.addTopLevelItem(l1)
        treeWidget.addTopLevelItem(l2)
        
        treeWidget.setContextMenuPolicy(Qt.CustomContextMenu)
        treeWidget.customContextMenuRequested.connect(self.openMenu)
        
        self.treeWidget = treeWidget

    def handle (self, item): 
        print('emitted!', item.text(1))
    
    def handleCollapse (self, item): 
        print('collapse emitted!', item.text(1))
    
    def openMenu(self):
        print("cm")
        print(QtGui.QCursor.pos())
        
        items = self.treeWidget.selectedItems()
        name = items[0].text(0)  # The text of the node.
        
        menu = QMenu(self)
        deleteAction = menu.addAction("delete")
        menu.addSeparator()
        action_1 = menu.addAction("Choix 1")
        action_2 = menu.addAction("Choix 2")
        action_3 = menu.addAction("Choix 3")
        action = menu.exec_(QtGui.QCursor.pos())
        if action == deleteAction:
            self.deleteAction(items)

    def deleteAction(self,items):
        item = items[0]
        children = item.takeChildren()
        for item in children:
            sip.delete(item)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    myapp = RbServerTree()
    myapp.show()

    sys.exit(app.exec_())
    
    