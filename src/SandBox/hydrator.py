class Entity():
    
    # #
    #
    def __init__(self, properties=None):
        if properties != None:
            self.hydrate(properties)
        else:
            self.properties = {}

    ##
    #
    def hydrate(self, properties):
            self.properties=properties
            return self

    def getProp(self, name):
        if name in self.properties:
            return self.properties[name]
        
    def setProp(self, name, value):
        if name in self.properties:
            self.properties[name] = value
        return self


class EntityAlt():
    
    # #
    #
    def __init__(self, properties=None):
        if properties != None:
            self.hydrate(properties)

    ##
    #
    def hydrate(self, properties):
        for key in properties:
            setattr(self, key, properties[key])
        return self


props = {"id":4, "name":"toto", "uid":"azezrezrtfd"}
entity = Entity(props)
print(entity.getProp("id"))
print(entity.getProp("name"))
entity.setProp("name", "marcel")
print(entity.getProp("name"))


props = {"id":4, "name":"toto", "uid":"azezrezrtfd"}
entity2 = EntityAlt(props)
print(entity2.id)
print(entity2.name)



