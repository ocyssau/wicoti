#!/usr/bin/python

import sys
import re
import operator
import os

from PyQt5 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineWidgets import  *

'''
os.startfile(path[, operation])�
lance un fichier avec son application associ�e.
Quand operation n�est pas sp�cifi� ou vaut 'open', l�effet est le m�me qu�un double clic sur le fichier dans Windows Explorer, ou que de passer le nom du fichier en argument du programme start depuis l�invite de commande interactif : le fichier est ouvert avec l�application associ�e � l�extension (s�il y en a une).
Quand une autre operation est donn�e, ce doit �tre une � commande-verbe � qui sp�cifie ce qui devrait �tre fait avec le fichier. Les verbes habituels document�s par Microsoft sotn 'print' et 'edit' (qui doivent �tre utilis�s sur des fichiers) ainsi que 'explore' et 'find' (qui doivent �tre utilis�s sur des r�pertoires).
startfile() retourne d�s que l�application associ�e est lanc�e.Il n�y a aucune option permettant d�attendre que l�application ne se ferme et aucun moyen de r�cup�rer le statu de sortie de l�application. Le param�tre path est relatif au r�pertoire actuel. Si vous voulez utiliser un chemin absolu, assurez-vous que le premier caract�re ne soit pas un slash ('/'). La fonction Win32 ShellExecute() sous-jacente ne fonctionne pas sinon. Utilisez la fonction os.path.normpath() pour vous assurer que le chemin est encod� correctement pour Win32.
Pour r�duire Pour r�duire le temps syst�me de d�marrage de l�interpr�teur, la fonction Win32 ShellExecute() ne se finit pas tant que cette fonction na pas �t� appel�e. Si la fonction ne peut �tre interpr�t�e, une NotImplementedError est lev�e.
Disponibilit� : Windows.
'''


class Test():
    
    def run(self):
        qurl = QUrl("file:///C:/home/o_cyssau/parts")
        ret = QDesktopServices.openUrl(qurl)
        
        os.system("start "+"C:/home/o_cyssau/parts/Test.docx")
        #os.system("start "+"C:/home/o_cyssau/parts/Test un peu plus.docx") == FAILED
        path = "C:/home/o_cyssau/parts/Test un peu plus.docx"
        os.startfile(path, 'open')

        print(ret)

    def staticVars(self):
        print(QStandardPaths.locate(QStandardPaths.DesktopLocation, "filename"))
        print(QStandardPaths.standardLocations(QStandardPaths.DocumentsLocation))
        print(QStandardPaths.standardLocations(QStandardPaths.ApplicationsLocation))
        print(QStandardPaths.standardLocations(QStandardPaths.TempLocation))
        print(QStandardPaths.standardLocations(QStandardPaths.HomeLocation))
        print(QStandardPaths.standardLocations(QStandardPaths.DataLocation))
        print(QStandardPaths.standardLocations(QStandardPaths.CacheLocation))

test = Test()
test.run()
test.staticVars()
