
def ConnectorFactory():
    class Catia:
        def __init__(self):
            self.name = "CATIA"
        def getName(self):
            return self.name

# create the class definition

import sys
import types

def str_to_class(field):
    try:
        identifier = getattr(sys.modules[__name__], field)
    except AttributeError:
        raise NameError("%s doesn't exist." % field)
    if isinstance(identifier, (types.ClassType, types.Type)):
        return identifier
    raise TypeError("%s is not a class." % field)


class ConnectorFactory2():
    def get(self,name):
        return eval(name+"()")

factory = eval("ConnectorFactory")
print(factory)
