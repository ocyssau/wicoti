# -*- coding: iso-8859-1 -*-

import sys, os.path
from PySide import QtCore, QtGui, QtSql
import sqlite3

h = 300
l = 400

PROJECT_PATH = os.path.dirname(sys.argv[0])
PATH_DB = PROJECT_PATH+ "\mybd_qsql.db"

class CreateTABLE():
    def TableContact(self):
        self.db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
	self.db.setDatabaseName(PATH_DB)
	self.db.open()
	query = QtSql.QSqlQuery()
	query.exec_('''create table Contact (
	id INTEGER PRIMARY KEY,
	nom TEXT, 
	prenom TEXT, 
	tel TEXT,
	fax TEXT, 
	mail TEXT, 
	adres TEXT,
	cp TEXT, 
	ville TEXT)''')
	self.db.commit()
	self.db.close()

class WriteTABLE():
    def TableContact(self, liste):        
	self.db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
	self.db.setDatabaseName(PATH_DB)
	self.db.open()
	self.model = QtSql.QSqlTableModel()
	self.model.setTable("Contact")
        self.model.select()
	self.model.insertRows(0, 1)
	a = 0
	while a <= 7:
	    self.model.setData(self.model.index(0, a+1), liste[a])
	    a+=1
	self.model.submitAll()
        self.db.close()
	
class ReadTABLE():
    def AppelContact(self, nom, prenom):
        liste = []
        self.db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
	self.db.setDatabaseName(PATH_DB)
	self.db.open()
	self.model = QtSql.QSqlTableModel()
	self.model.setTable("Contact")
        self.model.setFilter("""nom = '%s' and prenom = '%s'""" %(nom,prenom))
        self.model.select()
        record = self.model.record(0)
        a = 0
	while a <= 7:
            contact = record.value(a+1)
            liste.append(contact)
            a +=1
        self.db.close()
        return liste

    def TriNom(self):
        liste = []
        self.db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
	self.db.setDatabaseName(PATH_DB)
	self.db.open()
	self.model = QtSql.QSqlTableModel()
	self.model.setTable("Contact")
        self.model.select()
        nb_row = self.model.rowCount()

        a = 0
        while a < nb_row:
            record = self.model.record(a)
            contact = [record.value(1), record.value(2)]
            liste.append(contact)
            a +=1
        self.db.close()
        return liste
        
    def SupprContact(self, nom, prenom):
        self.db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
	self.db.setDatabaseName(PATH_DB)
	self.db.open()
	self.model = QtSql.QSqlTableModel()
	self.model.setTable("Contact")
        self.model.setFilter("""nom = '%s' and prenom = '%s'""" %(nom,prenom))
        self.model.select()
        self.model.removeRow(0)
        self.db.close()

    def ModifContact(self, nom, prenom, liste):
        self.db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
	self.db.setDatabaseName(PATH_DB)
	self.db.open()
	self.model = QtSql.QSqlTableModel()
	self.model.setTable("Contact")
        self.model.setFilter("""nom = '%s' and prenom = '%s'""" %(nom,prenom))
        self.model.select()
        record = self.model.record(0)
        a = 3
        while a <= 8:
            record.setValue(a, liste[a-3])
            a += 1
        self.model.setRecord(0, record)
        self.model.submitAll()
        self.db.close()

class Frame(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.resize(l,h)
        self.setFont(QtGui.QFont("Verdana"))
        self.setWindowTitle("Apprentissage PySide : R�alisation d'un carnet d'adresses")
        try:
            self.setWindowIcon(QtGui.Icon("icon.jpg"))
        except:pass
	
	CreateTABLE().TableContact()
        self.liste_cb = []

        fen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((fen.width()-size.width())/2, (fen.height()-size.height())/2)
        
        self.statusBar().showMessage("Utilisation de QMainWindow")
        
        menubar = self.menuBar()
        file_ = menubar.addMenu("&Fichier")
        
        self.menuquit = QtGui.QAction("&Fermer", self,
                                    shortcut=QtGui.QKeySequence.Close,
                                    statusTip="Quitter l'application", triggered=self.close)
        file_.addAction(self.menuquit)
        
        self.tabWidget = QtGui.QTabWidget(self)
        self.tabWidget.setGeometry(0,20,l,h-40)
        self.pnl_1 = QtGui.QWidget(self.tabWidget)
        self.pnl_2 = QtGui.QWidget(self.tabWidget)
        
        self.tabWidget.addTab(self.pnl_1, "Entr�e")
        self.tabWidget.addTab(self.pnl_2, "Lecture")
        
        self.pnl_1.setPalette(QtGui.QPalette(QtGui.QColor("white")))
        self.pnl_1.setAutoFillBackground(True)
        self.pnl_2.setPalette(QtGui.QPalette(QtGui.QColor("white")))
        self.pnl_2.setAutoFillBackground(True)
        
        self.widget_1(self.pnl_1)
        self.widget_1(self.pnl_2)

        self.btn_save = QtGui.QPushButton("Enregistrer", self.pnl_1)
        self.btn_save.move(315,210)
	self.btn_save.clicked.connect(self.write_bdd)
	
	self.btn_read = QtGui.QPushButton("Chercher", self.pnl_2)
        self.btn_read.move(315,210)
	self.btn_read.clicked.connect(self.read_bdd)

        self.btn_suppr = QtGui.QPushButton("Supprimer", self.pnl_2)
        self.btn_suppr.move(240,210)
	self.btn_suppr.clicked.connect(self.suppr)

        self.btn_read = QtGui.QPushButton("Modifier", self.pnl_2)
        self.btn_read.move(165,210)
	self.btn_read.clicked.connect(self.modif_bdd)
        
        self.item_cb()
        globals()[str(self.pnl_2)+"edit_nom"].addItems(self.liste_cb)
        
    def item_cb(self):
        self.liste_cb = []
        contact_bdd = ReadTABLE().TriNom()
        if contact_bdd!= []:
            for i in contact_bdd:
                contact = i[0]+" "+i[1]
                self.liste_cb.append(contact)

    def widget_1(self, parent):
        if parent == self.pnl_1:
            globals()[str(parent)+"nom"] = QtGui.QLabel("Nom", parent)
            globals()[str(parent)+"nom"].move(10,10)
            globals()[str(parent)+"edit_nom"] = QtGui.QLineEdit("", parent)
            globals()[str(parent)+"edit_nom"].setGeometry(75,7,315,20)
        
            globals()[str(parent)+"prenom"] = QtGui.QLabel("Pr�nom", parent)
            globals()[str(parent)+"prenom"].move(10,40)
            globals()[str(parent)+"edit_prenom"] = QtGui.QLineEdit("", parent)
            globals()[str(parent)+"edit_prenom"].setGeometry(75,37,315,20)
        else:
            globals()[str(parent)+"nom"] = QtGui.QLabel("Contact :", parent)
            globals()[str(parent)+"nom"].move(100,10)
            globals()[str(parent)+"edit_nom"] = QtGui.QComboBox(parent)
            globals()[str(parent)+"edit_nom"].setGeometry(10,37,380,20)
     
        globals()[str(parent)+"tel"] = QtGui.QLabel("T�l", parent)
        globals()[str(parent)+"tel"].move(10,70)
        globals()[str(parent)+"edit_tel"] = QtGui.QLineEdit("", parent)
        globals()[str(parent)+"edit_tel"].setGeometry(75,67,315,20)
        
        globals()[str(parent)+"fax"] = QtGui.QLabel("Fax", parent)
        globals()[str(parent)+"fax"].move(10,100)
        globals()[str(parent)+"edit_fax"] = QtGui.QLineEdit("", parent)
        globals()[str(parent)+"edit_fax"].setGeometry(75,97,315,20)
        
        globals()[str(parent)+"mail"] = QtGui.QLabel("Mail", parent)
        globals()[str(parent)+"mail"].move(10,130)
        globals()[str(parent)+"edit_mail"] = QtGui.QLineEdit("", parent)
        globals()[str(parent)+"edit_mail"].setGeometry(75,127,315,20)
        
        globals()[str(parent)+"adres"] = QtGui.QLabel("Adresse", parent)
        globals()[str(parent)+"adres"].move(10,160)
        globals()[str(parent)+"edit_adres"] = QtGui.QLineEdit("", parent)
        globals()[str(parent)+"edit_adres"].setGeometry(75,157,315,20)
        
        globals()[str(parent)+"cp"] = QtGui.QLabel("CP", parent)
        globals()[str(parent)+"cp"].move(10,190)
        globals()[str(parent)+"edit_cp"] = QtGui.QLineEdit("", parent)
        globals()[str(parent)+"edit_cp"].setGeometry(75,187,50,20)
        
        globals()[str(parent)+"ville"] = QtGui.QLabel("Ville", parent)
        globals()[str(parent)+"ville"].move(150,190)
        globals()[str(parent)+"edit_ville"] = QtGui.QLineEdit("", parent)
        globals()[str(parent)+"edit_ville"].setGeometry(190,187,200,20)

    def efface_widget(self, parent):
        if parent == self.pnl_1:
            globals()[str(parent)+"edit_nom"].clear()
            globals()[str(parent)+"edit_prenom"].clear()
        else:pass
	globals()[str(parent)+"edit_tel"].clear()
	globals()[str(parent)+"edit_fax"].clear()
	globals()[str(parent)+"edit_mail"].clear()
	globals()[str(parent)+"edit_adres"].clear()
	globals()[str(parent)+"edit_cp"].clear()
	globals()[str(parent)+"edit_ville"].clear()
	   	
    def write_bdd(self):
	parent = self.pnl_1
	liste = [str(globals()[str(parent)+"edit_nom"].text()),
	         str(globals()[str(parent)+"edit_prenom"].text()),
	         str(globals()[str(parent)+"edit_tel"].text()),
	         str(globals()[str(parent)+"edit_fax"].text()),
	         str(globals()[str(parent)+"edit_mail"].text()),
	         str(globals()[str(parent)+"edit_adres"].text()),
	         str(globals()[str(parent)+"edit_cp"].text()),
	         str(globals()[str(parent)+"edit_ville"].text())]

        contact = str(globals()[str(parent)+"edit_nom"].text()) + ' ' + str(globals()[str(parent)+"edit_prenom"].text())
        find_contact = globals()[str(self.pnl_2)+"edit_nom"].findText(contact)

	if find_contact == -1:
            WriteTABLE().TableContact(liste)
            globals()[str(self.pnl_2)+"edit_nom"].clear()
            self.efface_widget(parent)
            self.item_cb()
            globals()[str(self.pnl_2)+"edit_nom"].addItems(self.liste_cb)
        else:
            print "Erreur: Contact d�j� existant"

    def read_bdd(self):
	parent = self.pnl_2
	idx = str(globals()[str(parent)+"edit_nom"].currentIndex())
        sel = ReadTABLE().TriNom()
        nom = sel[int(idx)]
	row = ReadTABLE().AppelContact(nom[0], nom[1])
	globals()[str(parent)+"edit_tel"].setText(row[2])
        globals()[str(parent)+"edit_fax"].setText(row[3])
	globals()[str(parent)+"edit_mail"].setText(row[4])
	globals()[str(parent)+"edit_adres"].setText(row[5])
	globals()[str(parent)+"edit_cp"].setText(row[6])
	globals()[str(parent)+"edit_ville"].setText(row[7])

    def suppr(self):
        parent = self.pnl_2
	idx = str(globals()[str(parent)+"edit_nom"].currentIndex())
        sel = ReadTABLE().TriNom()
	ReadTABLE().SupprContact(sel[int(idx)][0], sel[int(idx)][1])

        globals()[str(parent)+"edit_nom"].clear()
        xx = ReadTABLE().TriNom()
        new_liste_cb = []
        for i in xx:
            contact = i[0]+' '+i[1]
            new_liste_cb.append(contact)

        globals()[str(self.pnl_2)+"edit_nom"].addItems(new_liste_cb)
        self.efface_widget(parent)

    def modif_bdd(self):
	parent = self.pnl_2
	idx = str(globals()[str(parent)+"edit_nom"].currentIndex())
        sel = ReadTABLE().TriNom()
	
	liste = [str(globals()[str(parent)+"edit_tel"].text()),
	         str(globals()[str(parent)+"edit_fax"].text()),
	         str(globals()[str(parent)+"edit_mail"].text()),
	         str(globals()[str(parent)+"edit_adres"].text()),
	         str(globals()[str(parent)+"edit_cp"].text()),
	         str(globals()[str(parent)+"edit_ville"].text())]
	
	ReadTABLE().ModifContact(sel[int(idx)][0], sel[int(idx)][1], liste)

                     
    def closeEvent(self, event):
        msg = QtGui.QMessageBox.question(self, "Info", "Voulez vous vraiment quitter l'application?",
                                         QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if msg == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

app = QtGui.QApplication(sys.argv)
frame = Frame()
frame.show()
sys.exit(app.exec_())
