#!/usr/bin/python

import sys
import re
import os
import tempfile

"""
from PyQt5 import QtCore, QtGui, QtWidgets, QtWebEngineWidgets
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
"""
from PyQt5.QtWidgets import  QTableWidgetItem

class ClassA():
    var1 = "var1"
    var2 = "var2"
    myList = []
    
    def __init__(self):
        pass


class ClassB(ClassA):
    
    def __init__(self):
        pass


def test20():
    instance1 = ClassA()
    instance2 = ClassB()
    instance1.var1 = "test"
    instance1.myList.append("test")
    print(instance1.var1)
    print(instance2.var1)
    print(instance2.myList)
    instance3 = ClassA()
    print(instance3)
    instance1.myList = []
    instance1.myList.append("test2")
    print(instance2.myList)

#test20()
#exit()


##
#
class TempFileSb():
    print(tempfile.gettempdir())
    tmpFile = tempfile.SpooledTemporaryFile(mode='w+', max_size=200000)
    tmpFile.write("Hello!")
    tmpFile.write("Hello!")
    tmpFile.write("Hello!")
    tmpFile.write("Hello!")
    tmpFile.write("Hello!")
    tmpFile.write("Hello!")
    tmpFile.write("Hello!")
    print(tmpFile.name)

    tmpFile = tempfile.TemporaryFile(mode='w+')
    tmpFile.write("Hello!")
    print(tmpFile.name)

    ##tmp = os.path.join(tempfile.gettempdir(), filename)
    ##print(os.path.join(tempfile.gettempdir(), filename))


#tempFileSb = TempFileSb()
#exit()


class StringMiscSb():
    
    def test1(self):
        string = "text/html;charset=UTF-8,<html><body><h1>Disconnected</h1></body></html>"
        bool = string.startswith("text/html")
        print(bool)
        exit()


class TableWidgetSb():
    item = QTableWidgetItem("Item1")            
    item.rbdata = {}
    t = hasattr(item, 'rbdata')
    print(t)
    
    item = QTableWidgetItem("Item1")            
    t = hasattr(item, 'zouzou')
    print(item.data)
    print(t)

class Test():
    name = "FOR TEST"

#item = Test()
#t = hasattr(item, 'data')
#print(t)



class ExReg():
    @staticmethod
    def test1():
        exp = "/ged/document/manager/[a-z]+/index/[0-9]+"
        
        url = "/ged/document/manager/workitem/index/1"
        r = re.match(exp, url)
        assert(r.__class__.__name__ == "SRE_Match")
        
        url = "/ged/docfile/manager/workitem/index/1"
        r = re.match(exp, url)
        assert(r == None)

        url = "/ged/document/manager/workitem/edit/1"
        r = re.match(exp, url)
        assert(r == None)

        url = "/ged/document/manager/cadlib/index/40"
        r = re.match(exp, url)
        assert(r != None)


ExReg.test1()


def miscs():
    print(os.environ)
    print(os.environ.get('HOME', '/home/username/'))
    print(sys.getprofile())
    print(sys.platform) ##linux or ...
    exit()
    
    serviceName = "woRkitem"
    serviceName = serviceName.title()
    print(serviceName)
    
    
    path = "ranchbe/a//rb/to/controller"
    print(path)
    
    path = path.replace("//", "/")
    print(path)

