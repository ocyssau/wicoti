import os
import sys
import sqlite3
from Connector.Ranchbe import Ranchbe, RbServiceResponse
from Main import RbMain
from PyQt5.QtWidgets import QApplication
from Ranchbe.Entity import Doctype

##
#
class DoctypeLocalCache():
    
    ##
    #
    def __init__(self, mainApp):
        self.conn = sqlite3.connect(os.path.realpath("../cache.db"))
        self.mainApp = mainApp
    
    ##
    #
    def initDb(self):
        try:
            c = self.conn.cursor()
            c.execute('CREATE TABLE doctypes (id integer, name text, number text, icon text)')
            self.conn.commit()
        except Exception as e: #OperationalError
            print(str(e))
        
        # We can also close the connection if we are done with it.
        # Just be sure any changes have been committed or they will be lost.
        ##conn.close()

    ##
    #
    def connectToRanchbe(self):
        #config = Config(os.path.realpath("../config.ini"))
        config = self.mainApp.config
        self.ranchbe = Ranchbe()
        self.ranchbe.setConfig(config)
        
        username = "admin"
        password = "admin00"
        
        qurl = self.ranchbe.getConnectQurl(username, password)
        self.ranchbe.restService().connect(qurl)
        qurl = self.ranchbe.getQurl('/service/application/ping/get')
        #ping = self.ranchbe.restService().get(qurl)
        #print(ping.text)
    
    ##
    #
    def loadServerDoctypes(self):
        qurl = self.ranchbe.getQurl('/service/doctype/list/search')
        httpRespons = self.ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        data = serviceRespons.data
        doctypes = []
        for properties in data:
            doctype = Doctype(properties)
            doctypes.append(doctype)
        return doctypes

    ##
    #
    def clear(self):
        try:
            c = self.conn.cursor()
            c.execute('DELETE FROM doctypes WHERE 1=1')
        except Exception as e:
            print(str(e))

    ##
    #
    def add(self, doctypes):
        try:
            c = self.conn.cursor()
            for doctype in doctypes:
                #id, name, number, icon
                entry = [doctype.id, doctype.name, doctype.number, doctype.icon]
                c.execute('INSERT INTO doctypes VALUES (?, ?, ?, ?)', entry)
            self.conn.commit()
        except Exception as e:
            print(str(e))

    ##
    #
    def getFromId(self, doctypeId):
        c = self.conn.cursor()
        cursor = c.execute('SELECT * FROM doctypes WHERE id=?', [doctypeId])
        row =  cursor.fetchone()
        return row

    ##
    #
    def getContent(self):
        c = self.conn.cursor()
        c.execute('SELECT * FROM doctypes')
        datas =  c.fetchall()
        return datas


RbMain.CONFIG_INIPATH = "../../config.ini"
app = QApplication(sys.argv)
mainApp = RbMain()

cache = DoctypeLocalCache(mainApp)
cache.initDb()
cache.connectToRanchbe()
doctypes = cache.loadServerDoctypes()
cache.clear()
cache.add(doctypes)

c = cache.getContent()
o = cache.getFromId(101)
