from Application.Catia import CatiaApplication, Products


# # 
#
class SampleCatiaData():

    def __init__(self):
        self.catia = CatiaApplication.get()
        self.catia.connect()
        self.partPrefix = "part_"
        self.productPrefix = "product_"
        None

    def generateHugeProduct(self, count):
        activeDocument = self.catia.getActiveDocument()
        products = Products(activeDocument)
        
        for num in range(0, count):
            try:
                pn = self.productPrefix + '{:04d}'.format(num)
                product = products.new("product", pn)
                subproducts = Products(product)
                self.addChildren(subproducts, count, str(num)+"_")
            except Exception as e:
                print(pn + " not generated", str(e))
                continue
    
    # #
    # @param products Products
    # @param count Integer
    # 
    def addChildren(self, products, count, prefix=""):
        for num in range(0, count):
            try:
                pn = prefix + self.partPrefix + '{:04d}'.format(num)
                products.new("part", pn)
            except Exception as e:
                print(pn + " not generated", str(e))
                continue

        return products


sampleGenerator = SampleCatiaData()
sampleGenerator.generateHugeProduct(30)
