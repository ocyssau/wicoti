from Application.Catia import FileLink, Document as CatDocument

##
#
class Entity():


    # #
    #
    def __init__(self, properties=None):
        self.id = None
        self.uid = None
        self.name = ""
        self.number = ""
        self.description = ""
        self.label = ""
        if properties != None:
            self.hydrate(properties)

    ##
    #
    def hydrate(self, properties):
        for key in properties:
            setattr(self, key, properties[key])
        return self

##
#
class User(Entity):
    pass

##
#
class Document(Entity):
    """
    A Document is recorded metadata about a vaulted document
    """

    # #
    #
    def __init__(self, properties=None):
        super().__init__(properties)
        self.docfiles = []
        self.catdoc = None

    ##
    # @param CatDocument catdoc
    # @return Document
    def setCatDocument(self, catdoc):
        if not(isinstance(catdoc, CatDocument)):
            raise(Exception("filelink parameter must be CatDocument type"))
        catdoc.rbDocument = self
        self.catDocument = catdoc
        return self

##
#
class Docfile(Entity):
    """
    A docfile is recorded metadata about a vaulted file. It make link between Document and Vault data 
    """

    # #
    #
    def __init__(self, properties=None):
        super().__init__(properties)
        self.catFilelink = None
    
    ##
    # @param FileLink filelink
    # @return Docfile
    def setFilelink(self, filelink):
        if not(isinstance(filelink, FileLink)):
            raise(Exception("filelink parameter must be FileLink type"))
        
        filelink.docfile = self
        self.catFilelink = filelink
        return self


##
#
class Project(Entity):
    pass

##
#
class Doctype(Entity):
    pass

##
#
class Container(Entity):
    pass

##
#
class Space(Entity):
    pass

##
#
class Category(Entity):
    pass

##
#
class AccessCode():
    """
    Enum to traduct access code to string readable for human
    use static :
    label = accessCode.getName(1)
    """

    lockCode = {
        0:'Free',
        1:'Checkout',
        5:'InWorkflow',
        6:'Canceled',
        7:'LockedLevel2',
        8:'LockedLevel3',
        9:'LockedLevel4',
        10:'Validate',
        11:'Locked',
        12:'MarkedToSuppress',
        13:'Iteration',
        14:'Previous Version',
        15:'Previous Version',
        20:'Archived',
        100: 'Archived 100',
        110: 'Archived 110',
        115: 'Archived 115',
        200: 'Archived 200'
    }

    @staticmethod
    def getName(code):
        """
        static method
        @param integer $code
        @return string
        """
        return AccessCode.lockCode.get(int(code), code)
