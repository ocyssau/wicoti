import sys
if sys.platform == "win32":
    import win32com.client

# # Super class for applications that been acceded through DCOM service
#    
class ComApplication():

    ##
    #
    def __init__(self, mainApp):
        self.comApplication = None
        self.connected = False
        self.name = ""
        self.mainApp = mainApp
    
    ##
    #
    def getName(self):
        return self.name

    ##
    #
    def connect(self):
        if not self.comApplication:
            try:
                self.comApplication = win32com.client.Dispatch(self.name + ".application")
                if self.comApplication.Visible == False:
                    self.comApplication.Visible = True
                self.connected = True
            except:
                print("unable to init com application for system "+sys.platform)

    ##
    #
    def isConnected(self):
        return self.connected
