from Application.Com import ComApplication


class Powerpoint(ComApplication):
    def __init__(self, mainApp):
        self.name = "powerpoint"
        self.mainApp = mainApp
        
    def openFile(self,filename):
        if self.application==None:
            self.Start()
        self.application.Presentations.Open(filename)
