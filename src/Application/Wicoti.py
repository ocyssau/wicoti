import os
import configparser
import shutil
import requests
import zipfile
from distutils.version import StrictVersion
import logging
from Connector.Ranchbe import DeployService

class Wicoti():
    
    VERSION = "1.2.0"
    APPLICATION_PATH = ""    
    SRC_PATH = ""
    DATA_PATH = ""
    CONFIG_INIPATH = ""
    CONFIG_DIST_INIPATH = ""
    USER_HOME = ""
    WEB_SITE = "https://bitbucket.org/ocyssau/wicoti/wiki/Home"

    def __init__(self, localInifile, distInifile):
        
        """ @var configparser.Config """
        self.config = None
        
        """ @var boolean """
        self.isInit = False
        
        """ @var Connector.Ranchbe """
        self.ranchbeConnector = None
        
        # init config of current application
        self.boot(localInifile, distInifile)
    
    def boot(self, localInifile, distInifile):
        """
        init configurations
        @param string localInifile Path to local configuration file
        @param string distInifile Path to distribution configuration file
        @return Ranchbe
        """
        # build config
        inifile = os.path.realpath(localInifile)
        if not os.path.isfile(inifile):
            ''' config must be set by user '''
            try:
                distConfigfile = os.path.realpath(distInifile)
                shutil.copy(distConfigfile, inifile)
                config = Config(inifile)
                self.setConfig(config)
            except Exception as e:
                raise Exception("inifile " + inifile + " cant be create for reason " + str(e))
        else:
            self.isInit = True
            config = Config(inifile)
            self.setConfig(config)
        
        return self
    
    def setMainApp(self, main):
        """
        @param Main.RbMain main
        @return Ranchbe
        """
        
        self.mainApp = main
        return self
        
    def getMainApp(self):
        """
        @return Main.RbMain mainApp
        """
    
        return self.mainApp

    def setConfig(self, config):
        """
        @param configparser.Config config
        @return Ranchbe
        """
        self.config = config
        return self
    
    def getConfig(self):
        """
        @return Ranchbe
        """
        return self.config

    def setRanchbeConnector(self, ranchbe):
        """
        @param Connector.Ranchbe ranchbe
        @return Ranchbe
        """
        self.ranchbeConnector = ranchbe
        return self
    
    def getRanchbeConnector(self):
        """
        @return Connector.Ranchbe
        """
        return self.ranchbeConnector


# #
#
class Config():

    # #
    #
    def __init__(self, inifile):
            self.inifile = inifile
            self.configParser = configparser.ConfigParser()
            self.configParser.read(self.inifile)

    # #
    #
    def getboolean(self, section, name):
        return self.configParser.getboolean(section, name)
    
    # #
    #
    def get(self, section, name):
        return self.configParser.get(section, name)

    def set(self, section, name, value):
        return self.configParser.set(section, name, value)

    # #
    #
    def save(self):
        with open(self.inifile, "w") as configfile:
            self.configParser.write(configfile)


# #
#
class Updater():
    
    # #
    #
    def __init__(self, ranchbe):
        self.ranchbe = ranchbe
    
    # #
    #
    def checkForUpdate(self, applicationPath):
        # Get last version from ranchbe server
        
        # # init rest service with none session handler
        restService = self.ranchbe.restService()
        restService.restRequest = requests
        
        deployService = DeployService(self.ranchbe)
        
        try:
            published = deployService.getCurrentDatas()
            publishedVersion = published.label.strip().lstrip('v').lstrip('V')
        except Exception as e:
            logging.error("updater error", str(e))
            return False

        logging.info("Current deployed iteration : %s, published deployed package iteration: %s", Wicoti.VERSION, publishedVersion)
        
        if(StrictVersion(Wicoti.VERSION) < StrictVersion(publishedVersion)):
            try:
                logging.info("Download update zip package iteration %s", publishedVersion)
                #Download last version of app
                path = Wicoti.DATA_PATH
                filename = "updateapp."+publishedVersion+".zip"
                filepath = deployService.download(path, filename)
            except Exception as e:
                logging.error("updater error", str(e))
                return False
        
            #unzip package
            try:
                logging.info("Unzip package in %s", applicationPath)
                with zipfile.ZipFile(filepath, "r") as zipRef:
                    zipRef.extractall(applicationPath)
            except Exception as e:
                logging.error("updater error", str(e))
                return False
        
        logging.info("Application is up to date")
        
        '''
        respons = requests.post(url, allow_redirects=True)
        if respons.status_code == 200:
            path = applicationPath
            filename = "updatedata.zip"
            filepath = os.path.join(path, filename)
            open(filepath, "wb").write(respons.content)
        else:
            raise Exception("Connexion is refused with code " + respons.status_code)
        with zipfile.ZipFile(filepath, "r") as zipRef:
            zipRef.extractall(applicationPath)
        '''
            
        return publishedVersion
    