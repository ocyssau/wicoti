from Application.Com import ComApplication


class Word(ComApplication):
    def __init__(self, mainApp):
        self.name = "word"
        self.mainApp = mainApp
    
    def openFile(self,filename):
        if self.application==None:
            self.Start()
        self.application.Documents.Open(filename)
