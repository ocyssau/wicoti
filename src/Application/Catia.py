#! /usr/bin/python
import os, sys
import logging, hashlib
from Application.Com import ComApplication

# #
#
class CatiaApplication(ComApplication):
    
    CATIA_COMMAND_LOAD = "Load"
    CATIA_COMMAND_UNLOAD = "Unload"

    """ Singleton instance """    
    singleton = None
    
    # #
    #
    def __init__(self):
        super().__init__(None)
        
        """ @var COMVariant Catia document thru COM """
        self.activeDocument = None
        
        """ Com named application """
        self.name = "Catia"
        
        """ @var string """
        self.lang = "en"
        
        """ @var Factory """
        self.factory = Factory(self)

        """ @var Documents """
        self.documents = None

        """ @var Products """
        self.products = None

        """ @var FileLinks """
        self.filelinks = None
        
        """ @var CATIA COM StiEngine """
        self.caiEngine = None
        
        CatiaApplication.singleton = self
    
    @staticmethod
    def get():
        if CatiaApplication.singleton == None:
            CatiaApplication.singleton = CatiaApplication()
        return CatiaApplication.singleton
    
    ''' ReInit active document to be ready to open a new document '''
    def clearAndInit(self):
        """ @var Documents """
        self.documents = Documents(self)

        """ @var Products """
        self.products = Products(self)

        """ @var FileLinks """
        self.filelinks = FileLinks(self)

        """ init active document (after init of collections) """
        self.activeDocument = None
        self.getActiveDocument()
        
    # #
    #
    def setLang(self, lang):
        self.lang = lang
        if self.lang == "en":
            CatiaApplication.CATIA_COMMAND_UNLOAD = "Unload"
            CatiaApplication.CATIA_COMMAND_LOAD = "Load"
        elif self.lang == "fr":
            CatiaApplication.CATIA_COMMAND_UNLOAD = "Decharger"
            CatiaApplication.CATIA_COMMAND_LOAD = "Charger"
        return self

    # #
    #   @see \Com\Connector::connect()
    def connect(self, renew=False):
        if renew == True:
            self.comApplication = None

        super().connect()
        self.clearAndInit()
        
        self.comApplication.Caption = "WICOTI CONNECT"
        #self.comApplication.Visible = True
        #self.comApplication.RefreshDisplay = False
        #self.comApplication.Height = 1
        #self.comApplication.Width = 300
        #self.comApplication.Top = 0
        #self.comApplication.Left = 0
        #self.comApplication.Interactive = False
        self.comApplication.DisplayFileAlerts = False
        return self.comApplication

    # #
    #   @see \Com\Connector::connect()
    def getFactory(self):
        if self.factory == None:
            self.factory = Factory(self)
        return self.factory

    # #
    # Reads a document stored in a file.
    # This method has to be used only for Browse purpose, for instance to retrieve Product properties.
    # Be careful, it doesn't open any editor (no visualization, no undo/redo capabilities...)
    #
    # @param string file
    # @return Catia
    #
    def readDocument(self, file):
        documents = self.comApplication.Documents
        documentName = os.path.basename(file)
        try :
            self.activeDocument = documents.Item(documentName)
        except:
            self.activeDocument = documents.Read(file)
        
        return self

    # #
    #
    def newfromDocument(self, file):
        documentName = os.path.basename(file)
        documents = self.comApplication.Documents
        try:
            self.activeDocument = documents.Item(documentName)
        except:
            documents.NewFrom(file)
            self.activeDocument = self.comApplication.ActiveDocument
            # self.activeDocument = documents.Open(file)
            # self.error =  sys.exc_info()[0]
        return self.activeDocument
       
    # #
    #
    def normalizeView(self, myViewer):
        myViewer.PutBackgroundColor(list(1, 1, 1))
        myViewer.RenderingMode = CatRenderingMode.catRenderShadingWithEdges
        self.comApplication.ActiveWindow.Layout = CatSpecsAndGeomWindowLayout.catWindowGeomOnly
        
        # Put in iso view
        myViewpoint = myViewer.Viewpoint3D
        myViewpoint.PutOrigin(list(0, 0, 0))
        myViewpoint.PutSightDirection(list(-0.577, -0.577, -0.577))
        myViewpoint.PutUpDirection(list(0, 0, 1))
        myViewer.Reframe
        
        return self

    # #
    #
    def getVersion(self):
        ver = {}
        try:
            system = self.comApplication.SystemConfiguration
            ver["version"] = system.Version
            ver["release"] = system.Release
            ver["servicePack"] = system.ServicePack
            ver["productCount"] = system.ProductCount
        except:
            ver["version"] = "unknow"
            ver["release"] = "unknow"
            ver["servicePack"] = "unknow"
            ver["productCount"] = "unknow"
        return ver

    # #
    # @return ProductDocument|PartDocument|DrawingDocument
    #
    def getActiveDocument(self):
        if self.activeDocument == None:
            comDocument = self.comApplication.ActiveDocument
            self.activeDocument = self.factory.getDocument(comDocument)
        return self.activeDocument
    
    # #
    #
    def newSelection(self, clear=True):
        selection = self.comApplication.ActiveDocument.Selection
        if clear:
            selection.clear()
        return selection
    
    # #
    #
    def closeAllDocuments(self):
        for openDoc in self.comApplication.Documents:
            openDoc.Close()
        return self
    
    # #
    #
    def getCAIEngine(self):
        if self.caiEngine == None:
            self.caiEngine = self.comApplication.GetItem("CAIEngine")
        return self.caiEngine
    
    # #
    # @return Application
    #
    def quit(self):
        self.comApplication.quit
        return self

##
#
class Collection():
    
    # 
    #
    def __init__(self):
        self.dictionnary = {}
    
    # #
    #
    def add(self, myObj):
        if myObj.name not in self.dictionnary:
            self.dictionnary[myObj.name] = myObj
        return self

    # #
    #
    def get(self, name):
        if name in self.dictionnary:
            return self.dictionnary[name]
        else:
            raise(Exception("Object not found"))

    # #
    #
    def toDict(self):
        return self.dictionnary.values()

    # #
    #
    def count(self):
        return len(self.dictionnary)

    # #
    #
    def reset(self):
        self.dictionnary = {}
        return self

##
#
class Products(Collection):
    
    # #
    # 
    # @param parent ProductDocument
    #
    def __init__(self, catia):
        self.dictionnary = {}
        self.catia = catia

    # #
    #
    # @param  myObj Product
    #
    def add(self, myObj):
        if myObj.__class__ == Product :
            self.dictionnary[myObj.comObject.name] = myObj
        else:
            raise(Exception("myObj must be Product"))


##
#
class Documents(Collection):
    
    # 
    # @param CDispatch(Catia Documents) comObject
    #
    def __init__(self, catia):
        self.catia = catia
        self.comObject = catia.comApplication.documents
        self.dictionnary = {}

    # #
    #
    def add(self, document):
        if isinstance(document, Document):
            self.dictionnary[document.comObject.name] = document
        else:
            raise(Exception("Documents.add document parameter must be Document"))

    # #
    #
    def new(self, docType):
        if(docType == "part"):
            comObject = self.comObject.Add("Part")
            document = PartDocument(comObject)
        elif(docType == "product"):
            comObject = self.comObject.Add("Product")
            document = ProductDocument(comObject)
        elif(docType == "drawing"):
            comObject = self.comObject.Add("Drawing")
            document = DrawingDocument(comObject)
            
        document.populate()
        self.add(document)
        return document

    # #
    #
    def item(self, name):
        try:
            return self.get(self, name)
        except:
            comObject = self.comObject.item(name)
            document = Factory.initDocumentFromCom(comObject)
            self.add(document)
        return document


    # #
    #
    def read(self, filepath):
        try:
            document = self.comObject.Read(filepath)
        except:
            raise(Exception("Read of file %s failed" % filepath))
            
        return document


##
#
class Instances(Collection):
    
    # # 
    # @param Product parent
    #
    def __init__(self, parent):
        self.dictionnary = {}
        self.isLoad = False
        if(isinstance(parent, Product)):
            self.parent = parent
            self.comObject = self.parent.comObject.Products
        else:
            raise(Exception("parent must be Product"))

    # # 
    # @param ProductInstance instance
    # @return self
    #
    def add(self, instance):
        if(isinstance(instance, ProductInstance)):
            super().add(instance)
        else:
            raise(Exception("instance must be ProductInstance"))
        return self

    # #
    #
    # @param  Product product
    # @param  String docType
    # @param  String partNumber
    #
    def new(self, product, docType, partNumber):
        if(docType == "part"):
            comObject = self.comObject.AddNewComponent("Part", partNumber)
        elif(docType == "product"):
            comObject = product.comObject.AddNewComponent("Product", partNumber)

        product = ProductInstance(comObject)
        product.populate()
        self.add(product)
        return product

    # # 
    # @param CDispatch(Catia Products) comProducts
    # @return self
    #
    def load(self):
        if(self.isLoad == False):
            try:
                c = self.comObject.Count
                for i in range(1, c+1):
                    product = ProductInstance(self.comObject.item(i))
                    product.setParent(self.parent)
                    self.add(product)
            except Exception as e:
                logging.debug(str(e))
        
        self.isLoad = True
        return self

##
#
class FileLinks(Collection):
    
    # 
    # @param CatiaApplication catia
    #
    def __init__(self, catia):
        if not(isinstance(catia, CatiaApplication)):
            raise(Exception("catia parameter must be CatiaApplication type"))
        self.catia = catia
        self.dictionnary = {}
        self.parent = None

    # #
    # @param CDispatch(Catia StiDBChildren) stiDBChildren
    # @param FileLink parent
    #
    def loadDocuments(self, oStiDBItem):
        oStiDBChildren = oStiDBItem.GetChildren()
        c = len(oStiDBChildren)

        for i in range(1, c+1):
            oStiDBItem2 = oStiDBChildren.Item(i)
            try:
                oStiDBItem2.GetDocument()
            except:
                fullname = oStiDBItem2.GetDocumentFullPath()
                self.catia.documents.read(fullname)
        # End for

    # #
    # @param CDispatch(Catia StiDBChildren) stiDBChildren
    # @param FileLink parent
    #
    def extractFromStiDBItem (self, oStiDBItem, parent):
        if not(isinstance(parent, FileLink)):
            raise(Exception("parent parameter must be FileLink type"))
        self.parent = parent
        self.loadDocuments(oStiDBItem)
        
        oStiDBChildren = oStiDBItem.GetChildren()
        c = len(oStiDBChildren)

        oStiDBChildren = oStiDBItem.GetChildren()
        for i in range(1, c+1):
            oStiDBItem2 = oStiDBChildren.Item(i)
            fullname = oStiDBItem2.GetDocumentFullPath()
            
            try:
                link = self.get(fullname)
            except:
                try:
                    link = FileLink()
                    link.setStiDbItem(oStiDBItem2)
                    link.linkType = oStiDBChildren.LinkType(i)
                except Exception as e:
                    logging.error(str(e))
                    continue

                if parent != None:
                    if parent.fullName == link.fullName:
                        link.isComponent = True
                    else:
                        link.isComponent = False
                
                if(link.isComponent):
                    self.extractFromStiDBItem(oStiDBItem2, parent)
                else:
                    self.add(link)

            logging.debug(link.name)
        # End for
        
        return self.dictionnary

    # #
    #
    def add(self, filelink):
        if isinstance(filelink, FileLink):
            key = hashlib.md5(filelink.fullName.encode('utf-8')).hexdigest()
            if key not in self.dictionnary:
                self.dictionnary[key] = filelink
        else:
            raise(Exception("FileLinks.add filelink parameter must be FileLink"))
        return self

    # #
    #
    def get(self, fullname):
        key = hashlib.md5(fullname.encode('utf-8')).hexdigest()
        if key in self.dictionnary:
            return self.dictionnary[key]
        else:
            raise(Exception("filelink key: %s fullname: %s not found" % (key, fullname)))

# #
#
class Factory():
    
    # #
    #  
    # @param parent Product
    #
    def __init__(self, catia):
        if(catia.__class__ != CatiaApplication):
            raise(Exception("Factory catia parameter must be CatiaApplication type"))
        self.catia = catia

    # #
    # Get product from collection or create it if necessary
    #
    def getProduct(self, comObject):
        try:
            product = self.catia.products.get(comObject.Name)
        except:
            product = Product(comObject)
            product.populate()
            self.catia.products.add(product)
        return product

    # #
    # Builf a document from the comObject and populate the catia.documents collection
    #
    def getDocument(self, comObject):
        try:
            document = self.catia.documents.get(comObject.Name)
        except:
            document = self.initDocumentFromCom(comObject)
            self.catia.documents.add(document)
        return document

    # #
    # Builf a filelink from the comObject and populate the catia.filelinks collection
    #
    def getFilelink(self, comObject):
        try:
            filelink = self.catia.filelinks.get(comObject.Name)
        except:
            filelink = FileLink(comObject)
            self.catia.filelinks.add(filelink)
        return filelink

    # #
    # Builf a document from the comObject and populate the catia.documents collection
    @staticmethod
    def initDocumentFromCom(comObject):
        extension = os.path.splitext(comObject.Name)
        if extension[1] == ".CATPart":
            document = PartDocument(comObject)
        elif extension[1] == ".CATProduct":
            document = ProductDocument(comObject)
        elif extension[1] == ".CATDrawing":
            document = DrawingDocument(comObject)
        document.populate()
        return document

# #
# Bind of CATIA Product
#
class Product():
    
    # # 
    #
    # @param COM CATIA OBJECT comInstanceProduct
    #
    def __init__(self, comProduct) :
        self.comObject = comProduct
        '''  @var CatiaApplication '''
        self.catia = CatiaApplication.get()
        try:
            self.catia.products.add(self)
        except:
            pass

        ''' @var dict '''
        self.userRefProperties = []
        
        ''' @var Product '''
        self.parent = None
        
        ''' @var FileLink '''
        self.fileLink = None
        
        ''' @var string '''
        self.catiaPath = ""
    
        ''' @var int '''
        self.catiaDepth = 1

        ''' @var string '''
        self.partNumber = ""

        ''' True if shape is activated '''
        self.shapeIsActivated = False
        
        ''' True if document is loaded '''
        self.dataIsLoaded = False
        
        ''' True if product is a component '''
        self._isComponent = None
        
        '''  @var string '''
        self.type = ""
        
        ''' @var integer '''
        self.id = ""
    
        '''  @var string '''
        self.uid = ""
        
        '''  @var ProductDocument|PartDocument '''
        self.document = None
        
        '''  @var Instances '''
        self.children = None
        
        '''  @var ProductWalker '''
        self.productWalker = None
        
        ''' Populate some properties from COM natif object '''
        self.populate()
 
    # # populate properties from comObjects when all datas are loaded
    # @return self
    #
    def populate(self):
        try:
            self.name = self.comObject.Name
            self.description = ""
            self.DescriptionInst = self.comObject.DescriptionInst
            self.descriptionRef = self.comObject.DescriptionRef
            self.definition = self.comObject.Definition
            self.revision = self.comObject.Revision
            self.nomenclature = self.comObject.Nomenclature
        except Exception as e:
            logging.debug("product is not fully loaded:" + str(e))
        
        try:
            if self.fileLink == None:
                self.fileLink = FileLink(self.comObject.Parent.FullName)
            self.type = self.fileLink.type
        except:
            pass
        
        return self

    # #
    #
    # @return self
    #
    def loadUserRefProperties(self):
        self.userRefProperties = []
        for i in range(1, self.comObject.UserRefProperties.count) :
            prop = self.comObject.UserRefProperties.item(i)
            self.userRefProperties.append({"name": prop.name, "value":prop.value})
        return self
        
    # #
    #
    # @return FileLink
    #
    def getFileLink(self):
        if self.fileLink == None:
            if self.comObject.HasAMasterShapeRepresentation() :
                filepath = self.comObject.GetMasterShapeRepresentationPathName()
                self.fileLink = FileLink(filepath)
                return self.fileLink
            else:
                raise(Exception("None shape found"))
        else:
            return self.fileLink

    # #
    # @return Boolean
    #
    def isActive(self):
        if (self.product) :
            params = self.comObject.Parameters
            b = self.referenceProduct.PartNumber + "\\" + self.name + "\Component Activation State"
            acti = params.GetItem(b).Value
            return acti
        else :
            raise(Exception('The reference product is not loaded'))

    # #
    # @return Boolean
    #
    def isComponent(self):
        """ detection of component """
        """ tree = Parent(Products).Parent(Product).Parent(Document) """
        """ :return : Boolean """
        if(self._isComponent == None):
            if(self.comObject.Parent.Parent.Parent.Name == self.comObject.ReferenceProduct.Parent.Name):
                self._isComponent = True
            else:
                self._isComponent = False
        
        return self._isComponent

    # #
    # @return List
    #
    def getArrayCopy(self):
        return {
            'name' : self.name,
            'partNumber' : self.partNumber,
            'description' : self.description,
            'descriptionInst' : self.descriptionInst,
            'descriptionRef' : self.descriptionRef,
            'nomenclature' : self.nomenclature,
            'revision' : self.revision,
            'userRefProperties': self.userRefProperties
        }

    # #
    # @return self
    #
    def activateDefaultShape(self):
        self.comObject.ActivateDefaultShape()
        self.shapeIsActivated = True
        return self

    # #
    # @return Document
    #
    def getDocument(self):
        if self.document == None :
            try:
                docComObj = self.comObject.Parent
            except Exception as e:
                logging.debug("getDocument error:", str(e))
                raise(e)
            
            try:
                self.document = self.catia.documents.get(docComObj.name)
            except:
                if self.fileLink.type == ".CATPart":
                    self.document = PartDocument(docComObj)
                else:
                    self.document = ProductDocument(docComObj)
                    
            self.document.setProduct(self)
        
        return self.document

    ##
    # @return Product
    #
    def setDocument(self, document):
        if(isinstance(document, Document)):
            self.document = document
            self.document.product = self
        else:
            raise(Exception("document parameter must be Document type"))
        return self

    ##
    # @return Product
    #
    def getParent(self):
        return self.parent

    ##
    # @return Product
    #
    def setParent(self, parent):
        if(isinstance(parent, Product)):
            self.parent = parent
        else:
            raise(Exception("parent parameter must be Product type"))
        return self

    # # 
    # Load children instances products of this product
    # @return Instances
    #
    def getChildren(self):
        if(self.children == None):
            try:
                self.children = Instances(self)
                self.children.load()
            except Exception as e:
                logging.debug(str(e))
        return self.children

# #
# Bind of CATIA Product
class ProductInstance(Product):
    """
    init : ProductInstance(productComObject) where productComObject is the com object refer to CATIA application. This ref is accessible in productInstance.comObject property
    after init, productInstance.comDocument is set with CatiaComObject of the document refer to instance
    """

    # # 
    # @param COM CATIA OBJECT comInstanceProduct
    #
    def __init__(self, comInstanceProduct) :
        super().__init__(comInstanceProduct)
        self.catiaPath = ""
        self.catiaDepth = 1
        self.nomenclature = ""
        self.quantity = ""
        self.descriptionInst = ""
        self.position = {}
        self.referenceProduct = None
        self.ofProductId = None
        self.ofProductUid = None
        self.ofProductName = None
        self.ofDocumentName = None

    # # populate properties from comObjects when all datas are loaded
    #
    def populate(self):
        try:
            self.name = self.comObject.Name
            self.description = ""
            self.descriptionRef = self.comObject.DescriptionRef
            self.descriptionInst = self.comObject.DescriptionInst
            self.definition = self.comObject.Definition
            self.revision = self.comObject.Revision
            self.nomenclature = self.comObject.Nomenclature
        except Exception as e:
            logging.debug("product instance is not fully loaded:" + str(e))

    ##
    #
    #
    def getReferenceProduct(self):
        if self.referenceProduct == None:
            self.loadReferenceProduct()
        return self.referenceProduct

    ##
    # Return the Products collection as CATIA COM Object CATBaseDispatch\Products
    #
    def getParent(self):
        return self.comObject.parent

    ##
    #
    def replace(self, filepath):
        catiaProducts = self.getParent()
        catiaProducts.ReplaceComponent(self.comObject, filepath, False)
        logging.debug("replace by ", filepath)
        return self

    # #
    #
    # @raises ReferenceProductLoadingException
    #
    def loadReferenceProduct(self):
        try :
            comReferenceProduct = self.comObject.ReferenceProduct
            self.referenceProduct = self.catia.factory.getProduct(comReferenceProduct)
            self.partNumber = self.comObject.ReferenceProduct.PartNumber
            self.ofProductName = self.comObject.ReferenceProduct.Name
            self.ofProductNumber = self.comObject.ReferenceProduct.PartNumber
            self.ofDocumentName = self.comObject.ReferenceProduct.Parent.Name
            
            if self.fileLink == None:
                self.fileLink = FileLink(self.comObject.ReferenceProduct.Parent.FullName)
            self.type = self.fileLink.type
            
            #if self.comObject.ReferenceProduct.name != self.name :
                #self._isComponent = True
            
        except Exception as e:
            logging.debug(str(e))
            fileLink = self.getFileLink()
            self.partNumber = fileLink.partNumber
            self.type = self.fileLink.type
            raise(ReferenceProductLoadingException("Load of catia document link failed, but FileLink is populated"))
        
        self.dataIsLoaded = True
        return self

    # #
    #
    def getArrayCopy(self):
        return {
            'name' : self.name,
            'ofProductId' : self.ofProductId,
            'ofProductUid' : self.ofProductUid,
            'ofProductName' : self.ofProductName,
            'ofProductNumber' : self.ofProductName,
            'ofDocumentName' : self.ofDocumentName,
            'nomenclature' : self.nomenclature,
            'quantity' : self.quantity,
            'description' : self.description,
            'position' : self.getPosition(),
            'children' : []
        }

    # #
    # Get the position matrix of the product item
    # to get the product in the activeProduct
    # If itemId is array, construct path to object like this :
    # Set Product = ActiveProduct.Products.item(1).Products.item(1) .
    #
    # ..etc for each value in itemPath
    #            
    #
    def getPosition(self):
        if self.position :
            return self.position
        self.position = Position.prototype.copy()
        logging.debug("getPosition of %s", self.name)
        
        """
        BE CAREFUL TO CASE OF function name case, problems if camel notation !!!! rb_get_position is ok, rbGetPosition is not.
        """
        script = '''
            Public Function rb_get_position(instance)
                Dim positionMatrix(11)
                instance.Position.GetComponents positionMatrix
                rb_get_position = positionMatrix
            End Function
            '''

        try:
            scriptLanguage = CatScriptLanguage.CATVBALanguage
            funcName = 'rb_get_position'
            parameters = [self.comObject]
            res = self.catia.comApplication.SystemService.Evaluate(script, scriptLanguage, funcName, parameters)
            self.position['ux'] = res[0]
            self.position['uy'] = res[1]
            self.position['uz'] = res[2]
            self.position['vx'] = res[3]
            self.position['vy'] = res[4]
            self.position['vz'] = res[5]
            self.position['wx'] = res[6]
            self.position['wy'] = res[7]
            self.position['wz'] = res[8]
            self.position['x'] = res[9]
            self.position['y'] = res[10]
            self.position['z'] = res[11]
        except:
            logging.debug(sys.exc_info())
        return self.position

# #
#
class Document():
    
    # #
    #
    # @param \variant comDocument
    #
    def __init__(self, comDocument):
        '''  @var Application.Catia.CatiaApplication '''
        self.catia = CatiaApplication.get()
        ''' @var COM '''
        self.comObject = comDocument
        ''' @var string '''
        self.fullName = ''
        ''' @var string '''
        self.name = ''
        ''' @var string '''
        self.path = ''
        ''' @var boolean '''
        self.readOnly = None
        ''' @var boolean '''
        self.saved = None

        ''' @var Application.Catia.Product '''
        self.product = None
        
        ''' @var Application.Catia.FileLink '''
        self.fileLink = None


    # # populate properties from comObjects when all datas are loaded
    # @return self
    #
    def populate(self):
        try:
            self.name = self.comObject.Name
            self.fullName = self.comObject.FullName
            self.path = self.comObject.Path
            self.saved = self.comObject.Saved
            self.readOnly = self.comObject.ReadOnly
        except Exception as e:
            logging.debug("product document is not fully loaded:" + str(e))
        return self

    ##
    # @param filelink FileLink
    # @return Document
    def setFileLink(self, filelink):
        if not(isinstance(filelink, FileLink)):
            raise(Exception("filelink parameter must be a FileLink type"))
        self.fileLink = filelink

    ##
    # @return FileLink
    def getFilelink(self):
        return self.fileLink

    # #
    #
    #
    def setProduct(self, product):
        if(isinstance(product, Product)):
            self.product = product
            self.product.document = self
        else:
            raise(Exception("product parameter must be Product type"))
        return self
    
    # #
    #
    #
    def getProduct(self):
        if self.product == None :
            try:
                comProduct = self.comObject.Product
            except Exception as e:
                logging.debug("getProduct error:", str(e))
                raise(e)
            
            try:
                self.product = self.catia.products.get(comProduct.name)
            except:
                self.product = Product(comProduct)
                    
            self.product.setDocument(self)
            
        return self.product
    
    # #
    #
    #
    def saveAs(self, filename):
        try:
            self.comObject.saveAs(filename)
            self.populate()
            if(self.fileLink):
                self.fileLink.setFullName(self.fullName)
        except Exception as e:
            logging.debug(str(e))
            raise(e)
        return self

    # #
    #
    #
    def save(self):
        try:
            self.comObject.save()
        except Exception as e:
            logging.debug(str(e))
            raise(e)
        
        return self

# #
#
class ProductDocument(Document):
    """
    init : ProductDocument(documentComObject) where documentComObject is the com object refer to CATIA application. This ref is accessible in productDocument.comObject property
    after init, productDocument.comProduct is set with CatiaComObject of the product of this document
    productDocument.catia is reference to the top CatiaApplication object
    
    Example:
        activeComDocument = comApplication.ActiveDocument
        productDocument = ProductDocument(activeComDocument)
        ret = productDocument.getArrayCopy()
        logging.debug(ret)
    """

    # #
    #
    # @param \variant comDocument
    #
    def __init__(self, comDocument):
        super().__init__(comDocument)
        ''' @var Application.Catia.Products '''
        self.products = None
        ''' @var list '''
        self.links = None
        
        self.populate()

    # #
    #
    # @return array
    #
    def getArrayCopy(self):
        try:
            comProduct = self.getProduct().comObject
            #comProduct = self.comObject.Product
        except Exception as e:
            logging.error("product of document %s can not be load" % self.name)
            raise(e)
        output = {
            'fullName' : self.fullName,
            'name' : self.name,
            'number' : self.name,
            'path' : self.path,
            'readonly' : self.readOnly,
            'saved' : self.saved,
            'product' : {
                'name' : comProduct.Name,
                'number' :comProduct.PartNumber,
                'nomenclature' : comProduct.Nomenclature,
                'version' : comProduct.Revision,
                'definition' :comProduct.Definition,
                'description' : comProduct.DescriptionRef
            }
        }
        return output

    # #
    # 
    # @param string file
    #
    def toStep(self, file):
        settingControllers1 = self.catia.comApplication.SettingControllers
        stepSettingAtt1 = settingControllers1.Item("CATSdeStepSettingCtrl")
        
        stepSettingAtt1.AttHeaderAuthor = 'Wicoti'
        stepSettingAtt1.AttHeaderOrganisation = 'SIER'
        # AP214 ISO#
        stepSettingAtt1.AttAP = 2
        # Assemblage with links to catia#
        stepSettingAtt1.AttASM = 3
        # unit mm#
        stepSettingAtt1.AttUnits = 0
        # export no show#
        stepSettingAtt1.AttShow = 1
        # export not visibles layers#
        stepSettingAtt1.AttLayersFilters = 1
        
        # Export to step file#
        self.comObject.ExportData(file, "stp")


    # # Get loaded file from catia app
    # @return FileLinks
    #
    def getLinks(self, oStiDBItem=None):
        
        if self.links != None:
            return self.links
        
        if(oStiDBItem is None):
            CAIEngine = self.catia.comApplication.GetItem("CAIEngine")
            oStiDBItem = CAIEngine.GetStiDBItemFromAnyObject(self.comObject)

        links = FileLinks(self.catia)
        links.extractFromStiDBItem(oStiDBItem, self.fileLink)
        
        self.links = links
        return self.links
    

# #
# 
#
#
class PartDocument(ProductDocument):

    # #
    #
    # @param \variant catDocument            
    #
    def __init__(self, comDocument):
        super().__init__(comDocument)

        self.shape = None

        ''' @var array '''
        self.physicalProperties = {}
        
        ''' Volume mm3 '''
        ''' @var decimal '''
        self.volume = None
    
        ''' Surface M2 '''
        ''' @var decimal '''
        self.wetArea = None
    
        ''' Density Kg/m3 '''
        ''' @var decimal '''
        self.density = None
    
        ''' Mass in grammes '''
        ''' @var decimal '''
        self.weight = None
    
        ''' @var array '''
        self.materials = {}
    
        ''' @var array '''
        self.gravityCenter = {}
    
        ''' @var array '''
        self.inertiaMatrix = {}
        
    # #
    #
    def getArrayCopy(self):
        output = super().getArrayCopy()
        output['physicalProperties'] = {
            'weight' : self.weight,  # #Mass in grammes#
            'density' : self.density,  # #Density Kg/m3#
            'wetArea' : self.wetArea,  # #Surface M2#
            'volume' : self.volume,  # #Volume mm3#
        }
        output['gravityCenter'] = self.gravityCenter
        output['inertiaMatrix'] = self.inertiaMatrix
        output['materials'] = self.materials
        return output

    # #
    #
    # @return PartDocument
    #
    def loadMaterials(self):
        part = self.document.Part
        materials = []
        parameters = part.Parameters
        
        for i in range(0, part.Bodies.Count) :
            # # protection against long time #
            if  i > 10 :
                break
            try:
                body = part.Bodies.item(i)
                strParam = parameters.item(part.Name + '\\' + body.Name + '\Material')
                materialName = strParam.Value
                materials.append({
                    'name' : materialName,
                    'density' : None
                })
            except Exception as e:
                logging.debug(str(e))
                continue

        self.materials = materials
        return self
        
    # #
    #
    # @return array
    #
    def getWeight(self):
        if self.weight == None:
            self.loadPhysicalProperties()
        return self.weight

    # #
    #
    # @return array
    #
    def getWetArea(self):
        if self.wetArea == None:
            self.loadPhysicalProperties()
        return self.wetArea

    # #
    #
    # @return array
    #
    def getVolume(self):
        if self.volume == None:
            self.loadPhysicalProperties()
        return self.volume

    # #
    #
    # @return array
    #
    def loadPhysicalProperties(self):
        product = self.product
        
        inertia = product.GetTechnologicalObject('Inertia')
        analyze = product.Analyze
        
        try :
            self.volume = analyze.Volume  # # Volume mm3 #
            self.wetArea = analyze.WetArea  # # Surface M2 #
            self.density = inertia.Density  # # Density Kg/m3 #
            self.weight = inertia.Mass  # 1000) ## Mass in grammes #
        except Exception as e :
            raise(PhysicalPropertyException(str(e)))

        return self

    # #
    # Get the gravity center matrix of the product item in the product context
    # or in the local datum
    # to get the product in the activeProduct
    # If itemId is array, construct path to object like this :
    # Set Product = ActiveProduct.Products.item(1).Products.item(1) ...etc for each value in itemPath
    #
    # @param \stdClass itemId
    #            is id to use with method VBA CATIA Automation Products.item()
    #
    def loadGravityCenter(self):

        if self.gravityCenter != None :
            return self.gravityCenter
    
        script = ("def getGravityCenter()"
            "Dim document As document"
            "Dim product As product"
            "Dim Inertia As Varian"
            "Dim cgCoordinates(2) As Variant"
            "Set document = CATIA.ActiveDocument"
            "Set product = CATIA.ActiveDocument.product"
            "Set Inertia = product.GetTechnologicalObject(\"Inertia\")"
            "Inertia.GetCOGPosition cgCoordinates"
            "getGravityCenter = cgCoordinates"
        "End Function")
        
        try :
            scriptLanguage = 0  # CATVBScriptLanguage
            funcName = 'getGravityCenter'
            parameters = list()
            catia = self.document.Application
            res = catia.SystemService.Evaluate(script, scriptLanguage, funcName, parameters)
        except :
            err = sys.exc_info()[0]
            raise(PhysicalPropertyException(err))
            return self
        
        # # Convert variant to array #
        keys = ['x', 'y', 'z']
        gravityCenter = {}
        k = 0
        for v in res :
            gravityCenter[keys[k]] = v
            k = k + 1
        
        self.gravityCenter = gravityCenter
        return self.gravityCenter

    # #
    #
    # @param string itemPath            
    #
    def loadInertiaCenter(self):

        if self.inertiaCenter != None:
            return self.inertiaCenter
    
        script = ("def getInertiaMatrix()"
            "Dim document As document"
            "Dim product As product"
            "Dim Inertia As Variant"
            "Dim inertiaMatrix(8) As Variant"
            "Dim analyze"
            "Set document = CATIA.ActiveDocument"
            "Set product = CATIA.ActiveDocument.product"
            "Set analyze = product.Analyze()"
            "analyze.GetInertia inertiaMatrix"
            "getInertiaMatrix = inertiaMatrix"
        "End Function")
        
        try :
            scriptLanguage = 0  # CATVBScriptLanguage
            funcName = 'getInertiaMatrix'
            parameters = {}
            catia = self.document.Application
            res = catia.SystemService.Evaluate(script, scriptLanguage, funcName, parameters)
        except:
            err = sys.exc_info()[0]
            raise(PhysicalPropertyException(err))
            return self

        # # Convert variant to array #
        keys = [
            'Ixx',
            'Ixy',
            'Ixz',
            'Iyx',
            'Iyy',
            'Iyz',
            'Izx',
            'Izy',
            'Izz'
        ]
        inertia = {}
        k = 0
        for v in res:
            inertia[keys[k]] = v
            k = k + 1

        self.inertiaMatrix = inertia
        return inertia

    # #
    #
    def loadShape(self, activate=True):
        if self.fileLink == None:
            self.fileLink = {
                'filePath' : "",
                'name' : "",
                'partNumber' : ""
            }
            product = self.document.product
            
            if product.HasAMasterShapeRepresentation() :
                self.fileLink['filePath'] = product.GetMasterShapeRepresentationPathName(activate)
                self.fileLink['name'] = os.path.basename(self.fileLink['filePath'])
                self.fileLink['partNumber'] = os.path.splitext(self.fileLink['name'])[0]
        return self.fileLink

    # #
    #
    def normalizeView(self):
        catApplication = self.catia.getApplication()
        window = catApplication.ActiveWindow
        myViewer = window.ActiveViewer
        
        myViewer.PutBackgroundColor({
            1,
            1,
            1
        })
        myViewer.RenderingMode = CatRenderingMode.catRenderShadingWithEdges
        window.Layout = CatSpecsAndGeomWindowLayout.catWindowGeomOnly
        
        # # Put in iso view 
        #
        myViewpoint = myViewer.Viewpoint3D
        myViewpoint.PutOrigin({
            0,
            0,
            0
        })
        myViewpoint.PutSightDirection({
            -0.577,
            -0.577,
            -0.577
        })
        myViewpoint.PutUpDirection({
            0,
            0,
            1
        })
        
        myViewer.Reframe()
        self.viewer = myViewer
        self.window = window
        return self

    # #
    #
    # @return \Rbcs\Converter\CatpartToRanchbe
    #
    def toJpg(self, file):
        # # Prepare view #
        self.normalizeView()
        
        # # Format unix path to windows path # require by com interface
        file = file.replace('/', '\\')
        
        # # See enum type CatCaptureFormat #
        self.viewer.CaptureToFile(CatCaptureFormat.catCaptureFormatJPEG, file)
        return self

    # #
    #
    def toWrl(self, file):
        catiaDocument = self.document
        
        settingControllers1 = self.catia.getApplication().SettingControllers
        vrmlSettingAtt1 = settingControllers1.Item("CATVisVrmlSettingCtrl")
        vrmlSettingAtt1.ExportVersion = 2  # VRML 97
        vrmlSettingAtt1.SetExportBackgroundColor(255, 255, 255)  # Background color
        
        # # Export to vrml file #
        file = file.replace('/', '\\')
        catiaDocument.ExportData(file, "wrl")
        return self

    # #
    #
    def toStl(self, file):
        catiaDocument = self.document
        
        # # Format unix path to windows path #
        file = file.replace('/', '\\')
        
        catiaDocument.ExportData(file, "stl")
        return self

    # #
    #
    def toIgs(self, file):

        catiaDocument = self.document
        
        settingControllers1 = self.catia.getApplication().SettingControllers
        settingAtt1 = settingControllers1.Item("CATIdeIgesSettingCtrl")
        settingAtt1.ApplyJoin = 1
        settingAtt1.ImportGroupAsSelSet = 1
        settingAtt1.ExportMSBO = 1
        settingAtt1.SaveRepository()
        
        # # Format unix path to windows path #
        file = file.replace('/', '\\')
        
        catiaDocument.ExportData(file, "igs")
        return self

# #
#
#
class DrawingDocument(Document):

    # #
    #
    # @param \COM comDocument
    #
    def __init__(self, comDocument):
        super().__init__(comDocument)
        ''' @var list '''
        self.sheets = []

    # #
    #
    # @return DrawingDocument
    #
    def loadSheets(self):
        if self.sheets == None:
            sheets = self.comObject.Sheets
            for i in range(1, sheets.Count):
                self.sheets.append(DrawingSheet(sheets.item(i)))
        return self

    # #
    #
    # @return array
    #
    def getArrayCopy(self):
        output = {
            "fullName": self.fullName,
            "name": self.name,
            "number": self.name,
            "path": self.path,
            "readonly": self.readOnly,
            "saved": self.saved,
            "sheets": {}
        }

        for sheet in self.sheets:
            output["sheets"][sheet.name] = sheet.getArrayCopy()

        return output

    # #
    # @return DrawingDocument
    #
    def toPdf(self, file):
        self.comObject.ExportData(file, "pdf")
        return self


# #
#
class DrawingSheet():

    # #
    #
    #
    # @param \stdClass catSheet
    #
    def __init__(self, comObject):
        self.comObject = comObject
        self.format = comObject.PaperName
        self.name = comObject.Name
        self.orientation = comObject.Orientation

    # #
    #
    def getArrayCopy(self):
        output = {
            'name': self.name,
            'format': self.format,
            'orientation': self.orientation
        }
        return output


# #
#
class FileLink():
    
    # #
    #
    def __init__(self, fullName='') :
        self.catia = CatiaApplication.get()
        self.isComponent = False
        self.isRoot = False
        self.product = None
        self.document = None
        self.stiDBItem = None
        self.fullName = ""
        self.name = ""
        self.partNumber = ""
        self.type = ""
        self.linkType = ""
        if fullName != "":
            self.setFullName(fullName)

    # #
    #
    def setFullName(self, fullName):
        caiEngine = self.catia.getCAIEngine()
        stiDBItem = caiEngine.GetStiDBItemFromCATBSTR(fullName)
        self.setStiDbItem(stiDBItem)
        return self
        
    # #
    # @param CatiaCom stiDBItem stiDBItem
    #
    def setStiDbItem(self, stiDBItem):
        self.stiDBItem = stiDBItem
        fullName = stiDBItem.GetDocumentFullPath()
        
        """ @var string """
        self.fullName = fullName
        
        """ @var string """
        self.name = os.path.split(fullName)[1]
        
        split = os.path.splitext(self.name)
        """ @var string """
        self.partNumber = split[0]
        """ @var string """
        self.type = split[1]
        
        self.isComponent = stiDBItem.IsCFOType()
        self.isRoot = stiDBItem.isRoot()

        try:
            comObject = stiDBItem.GetDocument()
        except Exception as e:
            self.document = None
            raise(Exception("error in FileLink.setStiDbItem() of %s. Document must be load: %s" % (self.fullName, str(e))))
            
        myDocument = self.catia.getFactory().getDocument(comObject)
        # Omni relation
        self.document = myDocument
        self.document.setFileLink(self)
        
        if(self.isComponent):
            self.name = stiDBItem.Parent.Name
            
        return self
    
    # #
    # @param CatiaCom stiDBItem stiDBItem
    #
    def getDocument(self):
        if not(isinstance(self.document, Document)):
            try:
                comObject = self.stiDBItem.GetDocument()
                self.document = self.catia.getFactory().getDocument(comObject)
            except Exception as e:
                logging.error("error in FileLink.getDocument() of %s: %s" % (self.fullName, str(e)))
                self.document = None
        return self.document
        
# #
#
class ProductWalker():

    # #
    # @param Application.Catia.Product
    # @param Connector.Catia.Catia
    def __init__(self, productDocument=None):
        self.rootDocument = productDocument
    
    # #
    # @param Application.Catia.Product parent
    # @param string path
    # @param integer depth
    def loadChildren(self, parent:ProductInstance, depth=1, basePath="/"):
        if parent.__class__ != ProductInstance:
            raise(Exception("parent must be a ProductInstance"))
            
        comProducts = parent.comObject.Products
        for itemCounter in range(1, len(comProducts)+1):
            try:
                catiaPath = basePath + '/' + str(itemCounter)
                instance = self.factoryFromPath(catiaPath, self.rootDocument)
                instance.catiaPath = catiaPath
                instance.catiaDepth = depth + 1
                parent.getReferenceProduct().children.append(instance) ## before loadReferenceProduct
                instance.parent = parent
                instance.loadReferenceProduct().populate() # populate() after loadReferenceProduct
                instance.isComponent() # detection of component
                instance.productWalker = self
            except Exception as e:
                logging.debug(str(e))
                continue
        
        return parent.getReferenceProduct().children


    # #
    # @param Application.Catia.Product parent
    # @param string path
    # @param integer depth
    def loadChildrenLinks(self, parent:Product, depth=1, basePath="/"):
        if parent.__class__ != Product:
            raise(Exception("parent must be a Product"))
        
        comProducts = parent.comObject.Products
        for itemCounter in range(1, len(comProducts)+1):
            try:
                catiaPath = basePath + '/' + str(itemCounter)
                instance = self.factoryFromPath(catiaPath, self.rootDocument)
                instance.catiaPath = catiaPath
                instance.catiaDepth = depth + 1
                parent.getReferenceProduct().children.append(instance) ## before loadReferenceProduct
                instance.parent = parent
                instance.loadReferenceProduct().populate() # populate() after loadReferenceProduct
                instance.isComponent() # detection of component
                instance.productWalker = self
            except Exception as e:
                logging.debug(str(e))
                continue
        
        return parent.getReferenceProduct().children

    # # Factory for build instance
    #
    # Get instance from root documument to leaf to be able to use instance in select collection
    #
    # @param string path String as id/id/id wheres id are Item index of the parents in Products relations
    # @param Application.Catia. ProductDocument rootComDocument
    #
    @staticmethod
    def factoryFromPath(path, rootDocument) :
        logging.debug("factoryFromPath:", path)
        pathAsArray = path.strip('/').strip().split('/')
        comRootProducts = rootDocument.comObject.Product.Products
        
        comProducts = comRootProducts
        for itemId in pathAsArray :
            if (itemId == ''): 
                continue
            comInstance = comProducts.Item(int(itemId))
            logging.debug(comInstance.Name)
            comProducts = comInstance.Products
            
            instance = ProductInstance(comInstance)
            instance.catiaPath = path

        return instance
    

class PhysicalPropertyException(Exception):
    pass


class ReferenceProductLoadingException(Exception):
    pass

# #
#
class ProductWalkerException(Exception):
    pass


# enum CatRenderingMode
class CatRenderingMode():
    catRenderShading = 0
    catRenderShadingWithEdges = 1
    catRenderWireFrame = 2
    catRenderHiddenLinesRemoval = 3
    catRenderQuickHiddenLinesRemoval = 4
    catRenderMaterial = 5
    catRenderMaterialWithEdges = 6
    catRenderShadingWithEdgesAndHiddenEdges = 7
    catRenderShadingWithEdgesWithoutSmoothEdges = 8

    
# enum CatSpecsAndGeomWindowLayout
class CatSpecsAndGeomWindowLayout():
    catWindowSpecsOnly = 0
    catWindowGeomOnly = 1
    catWindowSpecsAndGeom = 2

    
# enum CatCaptureFormat
class CatCaptureFormat():
    catCaptureFormatCGM = 0
    catCaptureFormatEMF = 1
    catCaptureFormatTIFF = 2
    catCaptureFormatTIFFGreyScale = 3
    catCaptureFormatBMP = 4
    catCaptureFormatJPEG = 5

# enum CatScriptLanguage
class CatScriptLanguage():
    CATVBScriptLanguage = 0
    CATVBALanguage = 1
    CATBasicScriptLanguage = 2
    CATJavaLanguage = 3
    CATJScriptLanguage = 4


# #
# Bind of CATIA Product
class Position():
    prototype = {
        'ux' : "",
        'uy' : "",
        'uz' : "",
        'vx' : "",
        'vy' : "",
        'vz' : "",
        'wx' : "",
        'wy' : "",
        'wz' : "",
        'x' : "",
        'y' : "",
        'z' : ""
    }
