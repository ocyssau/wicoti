from Application.Com import ComApplication


class Solidwork(ComApplication):
    def __init__(self, mainApp):
        self.name = "sldworks"
        self.mainApp = mainApp
        
    def openFile(self,filename):
        PART=1        
        ASSEMBLY = 2
        if self.application==None:
            self.Start()
        if "sldprt" in filename.lower():
            ctype = PART
        elif "sldasm" in filename.lower():
            ctype = ASSEMBLY
        self.application.OpenDoc(filename,ctype)
