#!/usr/bin/python
from Main import RbMain
import sys
import os
from PyQt5.QtWidgets import QApplication
import logging
from Connector.Ranchbe import Ranchbe
from Application.Wicoti import Wicoti, Updater as WicotiUpdater

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG, format='%(pathname)s:%(lineno)s:%(message)s') #https://docs.python.org/2/library/logging.html
logging.info("Wicoti version : %s", Wicoti.VERSION)
    
if __name__ == "__main__":
    logging.info("File : %s", __file__)
    logging.info("Sys version : %s", sys.version)
    logging.info("Args : %r" % sys.argv)
    logging.info("Plateform : %s", sys.platform)
    
    # determine if application is a script file or frozen exe
    if getattr(sys, 'frozen', False):
        srcPath = os.path.dirname(sys.executable)
        dataPath = os.path.join(os.path.dirname(srcPath), "data")
    elif __file__:
        srcPath = os.path.dirname(__file__)
        dataPath = os.path.join(os.path.dirname(srcPath), "data")
        
    applicationPath = os.path.dirname(srcPath)
    
    logging.info("Application path : %s", applicationPath)
    os.chdir(applicationPath)
    logging.info("Current dir: %s", os.getcwd())
    logging.info("Data path : %s", dataPath)
    
    home = os.path.expanduser("~")
    logging.info(home)
    
    if sys.platform == "win32":
        Wicoti.CONFIG_INIPATH = os.path.join(os.getenv("APPDATA"), "wicoti.ini")
    else:
        Wicoti.CONFIG_INIPATH = os.path.join(home, "wicoti.ini")
    
    Wicoti.USER_HOME = home
    Wicoti.CONFIG_DIST_INIPATH = os.path.join(dataPath, "config.dist.ini")
    Wicoti.APPLICATION_PATH = applicationPath
    Wicoti.SRC_PATH = srcPath
    Wicoti.DATA_PATH = dataPath
    RbMain.IMG_PATH = os.path.join(dataPath, "img")
    
    logging.info("ini file %s", Wicoti.CONFIG_INIPATH)
    
    ranchbe = Ranchbe()
    wicoti = Wicoti(Wicoti.CONFIG_INIPATH, Wicoti.CONFIG_DIST_INIPATH)
    wicoti.setRanchbeConnector(ranchbe)
    ranchbe.setConfig(wicoti.getConfig())
    
    if (hasattr(sys, "frozen") == True):
        updater = WicotiUpdater(ranchbe)
        updated = updater.checkForUpdate(srcPath)
        if(updated != False):
            # restart application
            pass
    
    ''' set user model '''
    app = QApplication(sys.argv)
    myapp = RbMain(wicoti)
    myapp.setAppuserModel('Sier.RanchbeConnector.'+Wicoti.VERSION)
    myapp.show()
    
    sys.exit(app.exec_())
