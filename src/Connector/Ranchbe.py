import requests
# from pprint import pprint
import json
import os
import re
import sqlite3
# import sip
from urllib.parse import parse_qs
from requests.utils import default_headers

from Connector.Abstract import AbstractConnector
from Ranchbe.Entity import Document, Docfile, Container, User, Doctype
from PyQt5.QtCore import QUrlQuery, QUrl
# from http.client import HTTPResponse


# #
#
#
class HttpClient():
    """
    BIND class for requests.Session
    """
    
    def __init__(self, config):
        """
        @param configparser.Config config
        """ 
        
        """ @var configparser.Config """
        self.config = config
    
        """ @var requests.Session """
        self.restRequest = None
    
        """ @var string """
        self.serverUrl = ""
    
        """ @var integer """
        self.serverPort = 80
        
        """ @var Ui_WebViewQt5 """
        self.webview = None
        
        """ @var boolean True is is connected """
        self.connected = False
        
        """ @var list """
        self.sessionCookie = None

    def cleanupCookie(self):
        """
        delete all recorded cookies of the session
        @return HttpClient fluent interface
        """
    
        """ @var RequestCokkieJar cookiesJar """
        cookiesJar = self.restRequest.cookies
        cookiesJar.clear()
        self.sessionCookie = None
        return self

    def connect(self, qurl):
        """
        @param QUrl qurl
        @return HTTPResponse
        """ 
        
        try:
            print("Try to connect to rest serveur")
            self.restRequest = requests.Session()
            
            respons = self.post(qurl, {})
            if respons.status_code == 200:
                self.connected = True
            elif respons.status_code == 401:
                self.connected = False
                raise ConnException("Connexion is refused with code 401")
            
            self.sessionCookies = self.restRequest.cookies.items()
            print("REST Cookie name: ", self.sessionCookies[0][0])
            print("REST Cookie value: ", self.sessionCookies[0][1])
            
        except Exception as e:
            raise Exception("Ranchbe Server is not available with reason :", str(e))

        return respons

    def connectWithoutSession(self):
        """
        @return HTTPResponse
        """ 
        
        try:
            print("Try to connect to rest serveur")
            self.restRequest = requests
        except Exception as e:
            raise Exception("Ranchbe Server is not available with reason :", str(e))

    def getCookies(self):
        return self.sessionCookies

    def disconnect(self, qurl):
        """
        @param QUrl qurl
        @return void
        """ 
    
        try:
            print("disconnect rest serveur")
            self.post(qurl, {})
            self.connected = False
            self.cleanupCookie()
            
        except Exception as e:
            print("Error during diconnexion :", e)

    def post(self, qurl:QUrl, datas:list={}, options:list={}):
        """
        HTTP POST REQUEST
        @param url QUrl
        @param list params http query to put in POST request, as can not be passed in QUrl query
        @return HTTPResponse
        """
        
        print(qurl.toString())
        query = qurl.query()
        parsedQuery = parse_qs(query)
        qurl.setQuery(QUrlQuery())
        headers = default_headers()
        
        if len(parsedQuery) > 0:
            datas = {}
            for value in parsedQuery :
                datas[value] = parsedQuery[value][0]
                
        if "dataType" in options:
            if options["dataType"] == "json":
                # headers["Accept"] = "text/plain"
                headers["Accept"] = "application/json, text/javascript, */*; q=0.01"
                headers["Content-Type"] = "application/json"
        
        r = self.restRequest.request("post", qurl.toString(), data=datas, headers=headers)
        print("REST STATUS CODE", r.status_code)
        return r

    def get(self, qurl:QUrl, datas:list={}, options:list={}):
        """
        HTTP GET REQUEST
        @param QUrl
        @return HTTPResponse
        """
        headers = default_headers()
        
        if "dataType" in options:
            if options["dataType"] == "json":
                # headers["Accept"] = "text/plain"
                headers["Accept"] = "application/json, text/javascript, */*; q=0.01"
                headers["Content-Type"] = "application/json"
        
        r = self.restRequest.request("get", qurl.toString(), data=datas, headers=headers)
        print("REST STATUS CODE", r.status_code)
        return r
    
    def upload(self, qurl:QUrl, filepath, params:list={}):
        """
        HTTP UPLOAD ON POST
        @param QUrl qurl
        @param string filepath of file to upload
        @return HTTPResponse
        @param list params http query to put in POST request
        """
    
        filehandle = open(filepath, "rb")
        print(filepath)
        multifiles = [
            ("file", (os.path.basename(filepath), filehandle, "multipart/form-data "))
        ]
        
        r = self.restRequest.post(qurl.toString(), files=multifiles, data=params)
        return r

    def getFilenameFromHeader(self, headers):
        """
        Get filename from content-disposition
        
        @param Headers headers as return by HttpRespons
        @return string
        """
    
        cd = headers.get("content-disposition")
        if not cd:
            return None
        fname = re.findall("filename=(.+)", cd)
        if len(fname) == 0:
            return None
        
        fnameStr = fname[0]
        fnameStr = fnameStr.strip('"')
        fnameStr = fnameStr.strip("'")
        return fnameStr
    
    def download(self, qurl:QUrl, params:list={"path":"/", "filename":""}):
        """
        DOWNLOAD OVER HTTP
        
        @param QUrl qurl
        @param list params must have key "path" to define directory where to put the downloaded file
        @return string filepath of the new file
        """
    
        r = self.restRequest.get(qurl.toString(), allow_redirects=True)
        
        if(params['filename'] == ""):
            filename = self.getFilenameFromHeader(r.headers)
        else:
            filename = params['filename']
        
        if r.status_code != 200:
            raise Exception("download failed from: " + r.url)
            
        try:
            path = params['path']
            filepath = os.path.join(path, filename)
            open(filepath, "wb").write(r.content)
        except Exception as e:
            raise Exception("file cant be write to " + filepath + " -- " + str(e))
        
        return filepath


# #
# Exception raise by HttpClient
#
class ConnException(Exception):
    title = "Connexion failed"
    pass


# #
# Exception raise by HttpClient
#
class ServiceNotFoundException(Exception):
    pass


class RbServiceResponse():
    """
    class of object return by Services methods
    """
    
    def __init__(self, httpRespons=None):
        self.feedbacks = {}
        self.errors = {}
        self.data = None
        self.exception = None
        self.json = "{}"
        self.url = ""
        self.code = 0
        self.headers = ""
        self.encoding = ""
        
        """
        @param HTTPRespons httpRespons as return by request
        @return void
        """
        if httpRespons != None:
            self.httpRespons = httpRespons
            t = httpRespons.text
            self.url = httpRespons.url
            self.code = httpRespons.status_code
            self.headers = httpRespons.headers
            self.encoding = httpRespons.encoding
            try:
                self.json = json.loads(t)
                self.errors = self.json["errors"]
                self.feedbacks = self.json["feedbacks"]
                self.exception = self.json["exception"]
                self.data = self.json["data"]
            except:
                self.json = {}
        
            if self.hasErrors():
                raise Exception(", ".join(self.getErrors()))
            if self.hasException():
                raise Exception(self.getException())
        
    def debug(self):
        print("REST URL", self.httpRespons.url)
        print("REST STATUS CODE", self.httpRespons.status_code)
        print("REST HEADER", self.httpRespons.headers)
        print("REST ENCODING", self.httpRespons.encoding)
        print("REST RESPONS", self.httpRespons.text)
        
    def getHttpRespons(self):
        """
        @return HTTPRespons
        """
        return self.httpRespons
    
    def hasFeedbacks(self):
        """
        @return boolean
        """
        return (len(self.feedbacks) > 0)

    def getFeedbacks(self):
    
        """ 
        @return list
        """ 
        return self.feedbacks

    def hasErrors(self):
        """
        @return boolean;
        """
        return (len(self.errors) > 0)

    def getErrors(self):
        """ 
        @return list
        """ 
        return self.errors
    
    def hasData(self):
        """
        @return boolean;
        """
        if (self.data != None):
            if (len(self.data) == 0):
                return False
            else:
                return True
        else:
            return False
    
    def getData(self):
        """ 
        @return list
        """ 
        return self.data
    
    def hasException(self):
        """
        @return boolean;
        """
        return (self.exception != None)
    
    def getException(self):
        """ 
        @return list
        """ 
        return self.exception


class Ranchbe(AbstractConnector):
    """
    Factory and registry to instanciate Services
    A service is instanciate only one time, call twice the factory return same instance
    Ranchbe is a singleton but as contructor need mainApp, it must construct before rb = Ranchbe(mainApp) and setConfig :rb.setConfig(config)
    Now to get the instance : Ranchbe.get()
    """
    
    """ Singleton instance """
    """ @var Ranchbe """
    instance = None
    
    @staticmethod
    def get():
        """ 
        Singleton accessor
        """
    
        if Ranchbe.instance is None:
            raise Exception("Ranchbe instance is not set. The ranchbe class must be instanciate before")
        return Ranchbe.instance
    
    def __init__(self, mainApp=None):
        """
        @param Main.RbMain mainApp
        """
        
        """ @var HttpClient """
        self.rest = None
        
        """ @var DocumentService """
        self.documentService = None
        
        """ @var DocfileService """
        self.docfileService = None
    
        """ @var ContainerService """
        self.containerService = None
        
        """ @var WildspaceService """
        self.wildspaceService = None
        
        """ @var CheckinService """
        self.checkinService = None
        
        """ @var CheckinService """
        self.checkoutService = None
    
        """ @var ProjectService """
        self.projectService = None
        
        """ @var SpaceService """
        self.spaceService = None
        
        """ @var CategoryService """
        self.categoryService = None
        
        """ @var WorkflowService """
        self.workflowService = None
        
        """ @var UserService """
        self.userService = None
        
        """ @var configparser.Config """
        self.config = None
        
        """ @var boolean """
        self.isInit = False
        
        self.name = "Ranchbe"
        self.mainApp = mainApp
        Ranchbe.instance = self
        
    def setMainApp(self, main):
        """
        @param Main.RbMain main
        @return Ranchbe
        """
        
        self.mainApp = main
        return self
        
    def getMainApp(self):
        """
        @return Main.RbMain mainApp
        """
    
        return self.mainApp

    def setConfig(self, config):
        """
        @param configparser.Config config
        @return Ranchbe
        """
        self.config = config
        return self
    
    def getConfig(self):
        """
        @return Ranchbe
        """
        return self.config

    def restService(self):
        """
        Get the REST service connexion
        @return HttpClient
        """
    
        if not self.rest:
            if self.config == None:
                raise Exception("Ranchbe.config is not set")
            self.rest = HttpClient(self.config)
        return self.rest
        
    def getConnectQurl(self, username, password):
        """
        Return Qurl to connect to ranchbe
        @return QUrl
        """
        
        qurl = self.getQurl("/service/application/auth/authenticate")
        query = QUrlQuery()
        query.addQueryItem("username", username)
        query.addQueryItem("password", password)
        query.addQueryItem("rememberme", "0")
        query.addQueryItem("applayout", "popup")
        query.addQueryItem("submit", "Signin")
        qurl.setQuery(query)
        print(str(qurl.toString()))
        return qurl
    
    def getQurl(self, path, params:list={}):
        """
        Return Qurl from string and params
        @param string path part of url after ranchbe server base url (without http://server.domain.com/base)
        @param list one dimension array of parameters of the query key is parameter name : value of parameter
        @return QUrl
        """
    
        qurl = QUrl()
        qurl.setHost(self.config.get("rbserver", "host"))
        qurl.setPort(int(self.config.get("rbserver", "port")))
        qurl.setScheme(self.config.get("rbserver", "scheme"))
        fullpath = self.config.get("rbserver", "basepath") + path
        fullpath = fullpath.replace("//", "/")
        qurl.setPath(fullpath)
        
        if len(params) > 0:
            query = QUrlQuery()
            for key, name in params.items():
                query.addQueryItem(key, str(name))
            qurl.setQuery(query)
        
        print(str(qurl.toString()))
        return qurl
    
    def getService(self, serviceName):
        """
        @return Service for type serviceName
        """
        try:
            serviceName = serviceName.title()
            serviceName = "get" + serviceName + "Service"
            func = getattr(self, serviceName)
            return func()
        except ServiceNotFoundException as e:
            raise Exception("Service " + serviceName + " is not available : " + str(e))
    
    def getDocumentService(self):
        """
        @return DocumentService
        """
    
        if self.documentService is None:
            self.documentService = DocumentService(self)
        return self.documentService

    def getDocfileService(self):
        """
        @return DocfileService
        """
    
        if self.docfileService is None:
            self.docfileService = DocfileService(self)
        return self.docfileService

    def getContainerService(self):
        """
        @return ContainerService
        """
    
        if self.containerService is None:
            self.containerService = ContainerService(self)
        return self.containerService

    def getProjectService(self):
        """
        @return ProjectService
        """
    
        if self.projectService is None:
            self.projectService = ProjectService(self)
        return self.projectService

    def getWildspaceService(self):
        """
        @return WildspaceService
        """
    
        if self.wildspaceService is None:
            self.wildspaceService = WildspaceService(self)
        return self.wildspaceService

    def getCheckinService(self):
        """
        @return CheckinService
        """
        if self.checkinService is None:
            self.checkinService = CheckinService(self)
        return self.checkinService

    def getCheckoutService(self):
        """
        @return CheckoutService
        """
    
        if self.checkoutService is None:
            self.checkoutService = CheckoutService(self)
        return self.checkoutService

    def getSpaceService(self):
        """
        @return SpaceService
        """
    
        if self.spaceService is None:
            self.spaceService = SpaceService(self)
        return self.spaceService

    def getCategoryService(self):
        """
        @return CategoryService
        """
        
        if self.categoryService is None:
            self.categoryService = CategoryService(self)
        return self.categoryService

    def getWorkflowService(self):
        """
        @return WorkflowService
        """
        
        if self.workflowService is None:
            self.workflowService = WorkflowService(self)
        return self.workflowService

    def getUserService(self):
        """
        @return UserService
        """
    
        if self.userService is None:
            self.userService = UserService(self)
        return self.userService

    def webSplashScreen(self, webEngineView):
        """
        @param QtWebEngineView webEngineView
        @return RbServiceResponse
        """
        params = {"layout":"popup"}
        qurl = self.getQurl("/home/splash", params)
        httpRespons = self.restService().get(qurl)
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        return serviceRespons

    def isConnected(self):
        if self.rest == None:
            return False
        else:
            if(self.rest.connected):
                return True
            else:
                return False


# #
#
#
class Service():
    """
    Super class for Services
    """
    
    def __init__(self, ranchbe):
        """ 
        @param Ranchbe ranchbe
        """
        
        """ @var Ranchbe """
        self.ranchbe = ranchbe
        
        """@var string"""
        self.name = self.__class__.__name__

    def getName(self):
        """ 
        @return string
        """
        return self.name

    def searchFromUrl(self, url, what="", filters="", params={}):
        """ 
        @param string what     is a term or a list of term to search
        @param dict filter     is dict of dict where key 0 is name of property, 1 value to be matched, 2 Operator as define in Rbplm\Dao\Filter\Op:
            CONTAINS
            NOTCONTAINS
            BEGIN
            NOTBEGIN
            END
            NOTEND
            LIKE
            MATCH
            EQUAL
            NOTEQUAL
            IN
            NOTIN
            SUP
            EQUALSUP
            INF
            EQUALIN
            NOTBETWEEN
            REGEX
            ISNULL
            NOTNULL
            FINDINSET
    
        @return RbServiceResponse
        """

        ranchbe = self.ranchbe
        
        if ranchbe.restService().connected == False:
            raise ConnException("Ranchbe service is not connected")
        
        if what:
            what = what.replace("  ", " ")
            whats = what.split(" ")
            for word in whats:
                params["search[]"] = word

        if filters:
            i = 0
            for k in filters:
                sqlfilter = filters[k]
                prop = sqlfilter[0]
                term = sqlfilter[1]
                op = sqlfilter[2]
                params["filter[" + str(i) + "][0]"] = str(prop)
                params["filter[" + str(i) + "][1]"] = str(term)
                params["filter[" + str(i) + "][2]"] = str(op)
                i = i + 1
        
        try:
            qurl = ranchbe.getQurl(url, params)
            httpRespons = ranchbe.restService().get(qurl)
            serviceRespons = RbServiceResponse(httpRespons)
        except Exception as e:
            print(e)
            raise e
            
        return serviceRespons


# #
#
class DocumentService(Service):

    def search(self, what="", sqlfilter="", params={}):
        """ 
        @see Service.searchFromUrl()
        @return RbServiceResponse
        """
        return self.searchFromUrl("/service/document/list/search", what, sqlfilter, params)

    def isExisting(self, name, spacename):
        """
        @param string name
        @param string spacename
        @return RbServiceResponse    with serviceRespons.data empty if not found else populate with dict with id and iteration of founded objects
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/document/entity/isexisting", {"name":name, "spacename":spacename})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        if serviceRespons.hasData():
            raise Exception("document not found")
        else:
            return serviceRespons

    def getEntity(self, sysid, spacename):
        """
        @param int sysid
        @param string spacename
        @return Document
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/document/entity/getfromid", {"id":sysid, "spacename":spacename})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        
        if serviceRespons.hasErrors():
            err = serviceRespons.getErrors()
            raise Exception(err[0])
        if serviceRespons.hasException():
            err = serviceRespons.getException()
            raise Exception(err[0])
        
        document = Document(serviceRespons.getData())
        print(document.id, document.uid, document.name)
        return document

    def getFromFilename(self, filename, spacename):
        """
        @param string filename
        @param string spacename
        @return Document
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/object/search/forfilename", {"name":filename, "spacename":spacename, "lastversiononly":"true"})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        if serviceRespons.hasErrors():
            err = serviceRespons.getErrors()
            raise Exception(err[0])
        elif serviceRespons.hasException():
            err = serviceRespons.getException()
            raise Exception(err[0])
        elif serviceRespons.hasData() == False:
            raise Exception("document for file " + filename + " in spacename " + spacename + " is not found")
        
        document = Document(serviceRespons.getData())
        return document

    def getDetails(self, documentId, spacename, webEngineView):
        """
        @param int documentId
        @param QtWebEngineView webEngineView
        @return RbServiceResponse
        """
        
        params = {"layout":"popup"}
        qurl = self.ranchbe.getQurl("/ged/document/detail/" + spacename + "/" + documentId, params)
        httpRespons = self.ranchbe.restService().get(qurl)
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        serviceRespons = RbServiceResponse()
        return serviceRespons

    def getEditPage(self, documentId, spacename, webEngineView):
        """
        @param Document document
        @param QtWebEngineView webEngineView
        @return RbServiceResponse
        """
        
        params = {"layout":"popup"}
        url = "/ged/document/manager/%spacename%/edit/%id%"
        url = url.replace("%spacename%", spacename).replace("%id%", documentId)        
        qurl = self.ranchbe.getQurl(url, params)
        httpRespons = self.ranchbe.restService().get(qurl)
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        serviceRespons = RbServiceResponse()
        return serviceRespons

    def setProperty(self, documentId, spacename, pName, pValue):
        """
        @param documentId
        @param spacename
        @param pName
        @param pValue
        @return
        """
        
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/document/entity/edit", {'id':documentId, 'spacename':spacename, 'pnames[0]':pName, 'pvalues[0]':pValue})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        
        if serviceRespons.hasErrors():
            err = serviceRespons.getErrors()
            raise Exception(err[0])
        if serviceRespons.hasException():
            err = serviceRespons.getException()
            raise Exception(err[0])


# #
#
class DocfileService(Service):

    def search(self, what="", sqlfilter="", params={"spacename":"workitem"}):
        """ 
        @see Service.searchFromUrl()
        @return RbServiceResponse
        """
        return self.searchFromUrl("/service/docfile/list/search", what, sqlfilter, params)

    def getEntity(self, sysid, spacename):
        """
        @param int sysid
        @param string spacename
        @return Document
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/docfile/entity/getfromid", {"id":sysid, "spacename":spacename})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        
        if serviceRespons.hasErrors():
            err = serviceRespons.getErrors()
            raise Exception(err[0])
        if serviceRespons.hasException():
            err = serviceRespons.getException()
            raise Exception(err[0])
        
        docfile = Docfile(serviceRespons.getData())
        return docfile

    def isExisting(self, filename, spacename):
        """
        @param string filename
        @param string spacename
        @return RbServiceResponse    with serviceRespons.data empty if not found else populate with dict with id and iteration of founded objects
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/docfile/entity/isexisting", {"name":filename, "spacename":spacename})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        if serviceRespons.hasData():
            return serviceRespons
        else:
            raise Exception("file not found")

    def getfromname(self, filename, spacename, version=""):
        """
        @param string filename
        @param string spacename
        @param string version [OPTIONAL] if not set return the last
        @return Docfile
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/docfile/entity/getfromname", {"name":filename, "spacename":spacename, "version":version})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        if serviceRespons.hasData():
            data = serviceRespons.getData()
            docfile = Docfile(data)
            return docfile
        else:
            raise Exception("file not found")

    def getOfDocument(self, documentId, spacename):
        """
        @param int documentId
        @param string spacename
        @return array of Docfile object
        """

        sqlfilter = {0:{0:"parentId", 1:documentId, 2:"EQUAL"}}
        params = {"spacename":spacename}
        docfiles = []
        serviceRespons = self.search("", sqlfilter, params)
        if serviceRespons.hasData():
            for entity in serviceRespons.getData():
                docfile = Docfile(entity)
                docfiles.append(docfile)
        return docfiles

    def download(self, docfileId, spacename, toPath):
        """
        @param int docfileId
        @param string spacename
        @param string toPath
        @return string filepath of the new file @see HttpClient.download()
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/docfile/viewer/download", {"spacename":spacename, "id":docfileId})
        filepath = ranchbe.restService().download(qurl, {"path":toPath})
        return filepath

# #
#
class DeployService(Service):

    def getCurrentDatas(self):
        """
        @return Docfile
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/wicoti/deploy/getcurrentdatas", {})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        
        if serviceRespons.hasErrors():
            err = serviceRespons.getErrors()
            raise Exception(err[0])
        if serviceRespons.hasException():
            err = serviceRespons.getException()
            raise Exception(err[0])
        
        docfile = Docfile(serviceRespons.getData())
        return docfile

    def download(self, path, filename):
        """
        @param string path
        @param string filename
        @return string filepath of the new file @see HttpClient.download()
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/wicoti/deploy/download")
        filepath = ranchbe.restService().download(qurl, {"path": path, "filename": filename})
        return filepath

# #
#
class WildspaceService(Service):

    def upload(self, filepath, replace=1):
        """
        @param int docfileId
        @param string spacename
        @param string toPath
        @return RbServiceResponse
        """

        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/wildspace/file/upload")
        httpRespons = ranchbe.restService().upload(qurl, filepath, {"replace":replace})
        serviceRespons = RbServiceResponse(httpRespons)
        if serviceRespons.hasErrors():
            serviceRespons.debug()
            raise Exception("upload of file " + os.path.basename(filepath) + " failed")
        elif serviceRespons.hasException():
            serviceRespons.debug()
            raise Exception("upload of file " + os.path.basename(filepath) + " failed")
        
        return serviceRespons


# #
#
class CheckinService(Service):
    
    def checkinDocuments(self, documents:list, checkinandkeep:bool, webEngineView):
        """
        checkin document and associated files
        @param list documents where values are list[0] = document id, list[1] = spacename
        @param QtWebEngineView webEngineView
        @return RbServiceResponse
        """
        if len(documents) == 0:
            webEngineView.setHtml("<html><body><h1>None selected files or upload failed</h1></body></html>")
            return
        
        if checkinandkeep:
            checkinandkeep = "1"
        else:
            checkinandkeep = "0"
        
        params = {"checkinandkeep":checkinandkeep, "layout":"popup", "basecamp":"/home/splash"}
        for document in documents:
            i = str(document.id)
            params["documents[" + i + "][id]"] = str(document.id)
            params["documents[" + i + "][spacename]"] = document.spacename
            
            if hasattr(document, "description") and document.description != "":
                params["documents[" + i + "][description]"] = document.description

            if hasattr(document, "categoryId") and document.categoryId != "":
                params["documents[" + i + "][categoryId]"] = document.categoryId
                
        qurl = self.ranchbe.getQurl("/ged/document/lockmanager/checkin", params)
        httpRespons = self.ranchbe.restService().post(qurl)
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        
        serviceRespons = RbServiceResponse(httpRespons)
        return serviceRespons

    def checkinfiles(self, filelist:dict, checkinandkeep:bool, webEngineView):
        """
        @param list filelist
        @param boolean checkinandkeep If True, document is keep as checkouted
        @param QtWebEngineView webEngineView
        @return httpRespons
        """
        
        if len(filelist) == 0:
            webEngineView.setHtml("<html><body><h1>None selected files or upload failed</h1></body></html>")
            return
        
        spacename = "workitem"
        params = {"checkinandkeep":str(checkinandkeep), "layout":"popup", "spacename":spacename, "basecamp":"/home/splash"}
        documentService = self.ranchbe.getDocumentService()
        for filename in filelist:
            document = documentService.getFromFilename(filename, spacename)
            documentId = document.id
            params["checked[" + str(documentId) + "]"] = str(documentId)
            params["spacenames[" + str(documentId) + "]"] = spacename

        qurl = self.ranchbe.getQurl("/ged/document/lockmanager/checkin", params)
        httpRespons = self.ranchbe.restService().get(qurl)
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        # if httpRespons.status_code != 200:
        return serviceRespons

    def storefiles(self, docfiles:list, context, webEngineView):
        """
        @param list products
        @param Main.Context context
        @param QtWebEngineView webEngineView
        @return RbServiceResponse
        
        From ranchbe class :
         * ["containerId"]=>integer
         * ["spacename"]=>string
         * ["docfiles"][index] => array(
         *         "uid" => string
         *         "name" => string /file name/
         *         "data"=>array(
         *             "data"=>"",
         *             "md5"=>"",
         *             "base64"=>"",
         *         ),
         *         "action" => string
         *         "parent" => array( //document data
         *             "description" => int
         *             ...
         *         ),
         *         "container"=>array(
         *             "id"=>integer,
         *             "lockContainerSelector"=>boolean, //If true, disable container selector
         *         )
         * )
        """
        
        if len(docfiles) == 0:
            webEngineView.setHtml("<html><body><h1>None selected files or upload failed</h1></body></html>")
            return
        
        containerId = context.containerId
        spacename = context.spacename
        
        datas = {"layout":"popup", "basecamp":"/home/splash", "docfiles":{}}
        datas["containerId"] = containerId
        datas["spacename"] = spacename
        
        i = 1
        for docfile in docfiles:
            try:
                props = docfile.pdmData.getDatas()['properties']['product']
                description = props['description']
                tags = props['definition']
            except:
                description = ""
                tags = ""

            filename = docfile.name
            # params["checked[" + str(i) + "]"] = filename
            # params["description[" + str(i) + "]"] = product.description
            # params["container[" + str(i) + "]"] = containerId
            datas["docfiles"]["docfile" + str(i)] = {
                "name": filename,
                "parent":{
                    "description": description,
                    "tags": tags
                },
                "container":{
                    "id":containerId,
                    "spacename":spacename
                }
            }
            i = i + 1
        
        qurl = self.ranchbe.getQurl("/ged/document/smartstore/store")
        httpRespons = self.ranchbe.restService().post(qurl, json.dumps(datas), {"dataType":"json"})
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        serviceRespons = RbServiceResponse()
        return serviceRespons


# #
#
class CheckoutService(Service):
    
    def checkoutDocument(self, documentId, spacename):
        """
        @param int documentId
        @param string spacename
        @return array of checkouted docfiles
        """
        
        params = {"id":str(documentId), "spacename":spacename, "replace":"true", "getfile":"false"}

        qurl = self.ranchbe.getQurl("/service/document/lockmanager/checkout", params)
        httpRespons = self.ranchbe.restService().post(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        
        if serviceRespons.hasErrors():
            raise Exception(", ".join(serviceRespons.getErrors()))
        if serviceRespons.hasException():
            raise Exception(serviceRespons.getException())
        
        docfiles = []
        for dfentity in serviceRespons.getData():
            docfile = Docfile(dfentity)
            docfiles.append(docfile)
        
        return docfiles

    def cancelcheckoutDocument(self, documentId, spacename):
        """
        @param int documentId
        @param string spacename
        @return RbServiceResponse
        """
        
        params = {"id":str(documentId), "spacename":spacename}

        qurl = self.ranchbe.getQurl("/service/document/lockmanager/reset", params)
        httpRespons = self.ranchbe.restService().post(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        
        return serviceRespons


# #
#
#
class ProjectService(Service):

    def search(self, what="", sqlfilter=""):
        """ 
        @see Service.searchFromUrl()
        @return RbServiceResponse
        """
        return self.searchFromUrl("/service/project/list/search", what, sqlfilter)

    def getDetails(self, projectId, webEngineView):
        """
        @param int projectId
        @param QtWebEngineView webEngineView
        @return RbServiceResponse
        """
        
        params = {"layout":"popup"}
        qurl = self.ranchbe.getQurl("/ged/project/detail/" + projectId, params)
        httpRespons = self.ranchbe.restService().get(qurl)
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        serviceRespons = RbServiceResponse()
        return serviceRespons


# #
#
#
class SpaceService(Service):

    def search(self, what="", sqlfilter=""):
        """ 
        @see Service.searchFromUrl()
        @return RbServiceResponse
        """
        return self.searchFromUrl("/service/space/list/search", what, sqlfilter)

    
# #
#
class ContainerService(Service):
    
    def getEntity(self, sysid, spacename):
        """
        @param int sysid
        @param string spacename
        @return Document
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/container/entity/getfromid", {"id":sysid, "spacename":spacename})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        
        if serviceRespons.hasException():
            err = serviceRespons.getErrors()
            raise Exception(err[0])
        if serviceRespons.hasException():
            err = serviceRespons.getException()
            raise Exception(err[0])
        
        container = Container(serviceRespons.getData())
        print(container.id, container.uid, container.name)
        return container

    def search(self, what="", sqlfilter="", params={}):
        """ 
        @see Service.searchFromUrl()
        @return RbServiceResponse
        """
        return self.searchFromUrl("/service/container/list/search", what, sqlfilter, params)

    def getDetails(self, containerId, spacename, webEngineView):
        """
        @param int id
        @param QtWebEngineView webEngineView
        @return RbServiceResponse
        """
        
        params = {"layout":"popup"}
        qurl = self.ranchbe.getQurl("/ged/container/detail/" + spacename + "/" + containerId, params)
        httpRespons = self.ranchbe.restService().get(qurl)
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        serviceRespons = RbServiceResponse()
        return serviceRespons


# #
#
class CategoryService(Service):

    def search(self, what="", sqlfilter="", params={}):
        """ 
        @see Service.searchFromUrl()
        @return RbServiceResponse
        """
        return self.searchFromUrl("/service/category/list/search", what, sqlfilter, params)

    def getusedbycontainer(self, params={}):
        """ 
        @param dict params
                params["spacename"]
                params["containerId"]
                params["parentId"]
        @see Service.searchFromUrl()
        @return RbServiceResponse
        """
        return self.searchFromUrl("/service/category/list/getusedbycontainer", "", "", params)


# #
#
class UserService(Service):
    
    def getCurrent(self):
        """ 
        @return User
        """
        ranchbe = self.ranchbe
        qurl = ranchbe.getQurl("/service/application/auth/getme", {})
        httpRespons = ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        
        if serviceRespons.hasException():
            err = serviceRespons.getErrors()
            raise Exception(err[0])
        if serviceRespons.hasException():
            err = serviceRespons.getException()
            raise Exception(err[0])
        
        user = User(serviceRespons.getData())
        print(user.id, user.uid, user.login)
        return user


# #
#
class WorkflowService(Service):

    def start(self, docfiles:list, context, webEngineView):
        """
        @param list docfiles
        @param QtWebEngineView webEngineView
        @return RbServiceResponse
        """
        
        if len(docfiles) == 0:
            webEngineView.setHtml("<html><body><h1>None selected files or upload failed</h1></body></html>")
            return
        
        spacename = context.spacename
        
        datas = {"layout":"popup", "basecamp":"/home/splash", "docfiles":{}}
        datas["spacename"] = spacename
        
        params = {}
        i = 1
        for docfile in docfiles:
            documentId = docfile.parentId
            params["checked[" + str(i) + "]"] = documentId
            params["spacename"] = spacename
            """
            datas["documents"]["document"+str(i)] = {
                "id": documentId,
                "spacename":spacename
            }
            """
            i = i + 1
        
        qurl = self.ranchbe.getQurl("/docflow/index/index", params)
        httpRespons = self.ranchbe.restService().post(qurl)
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        serviceRespons = RbServiceResponse()
        return serviceRespons

    def startFromDocument(self, documents:list, webEngineView):
        """
        @param list documents
        @param QtWebEngineView webEngineView
        @return RbServiceResponse
        """
        
        if len(documents) == 0:
            webEngineView.setHtml("<html><body><h1>None selected document</h1></body></html>")
            return
        
        params = {"layout":"popup", "basecamp":"/home/splash"}
        params["spacename"] = documents[0].spacename
        i = 1
        for document in documents:
            if document.spacename != params["spacename"]:
                print("Document " + document.name + " is out out of selected space")
                continue
            params["checked[" + str(i) + "]"] = document.id
            """
            datas["documents"]["document"+str(i)] = {
                "id": documentId,
                "spacename":spacename
            }
            """
            i = i + 1
        
        qurl = self.ranchbe.getQurl("/docflow/index/index", params)
        httpRespons = self.ranchbe.restService().post(qurl)
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        serviceRespons = RbServiceResponse()
        return serviceRespons


# #
#
class DoctypeLocalCache():
    
    """ singleton instance """
    """ @var DoctypeLocalCache """
    instance = None
    
    COL_ID = 0
    COL_NAME = 1
    COL_NUMBER = 2
    COL_ICON = 3
    
    # #
    #
    def __init__(self, dbPath="data/cache.db"):
        self.conn = sqlite3.connect(os.path.realpath(dbPath))
        self.dbpath = dbPath
        DoctypeLocalCache.instance = self
    
    # # Singleton getter
    # 
    @staticmethod
    def get():
        if DoctypeLocalCache.singleton == None:
            DoctypeLocalCache.singleton = DoctypeLocalCache()
        return DoctypeLocalCache.instance
    
    # #
    #
    def initDb(self):
        try:
            c = self.conn.cursor()
            c.execute('CREATE TABLE doctypes (id integer, name text, number text, icon text)')
            self.conn.commit()
        except Exception as e:  # OperationalError
            print(str(e))
        
        """
        We can also close the connection if we are done with it.
        Just be sure any changes have been committed or they will be lost.
        conn.close()
        """

    # #
    #
    def connectToRanchbe(self, mainApp):
        config = mainApp.config
        self.ranchbe = Ranchbe()
        self.ranchbe.setConfig(config)
        
        username = "admin"
        password = "admin00"
        
        qurl = self.ranchbe.getConnectQurl(username, password)
        self.ranchbe.restService().connect(qurl)
        qurl = self.ranchbe.getQurl('/service/application/ping/get')
    
    # #
    #
    def loadServerDoctypes(self):
        qurl = self.ranchbe.getQurl('/service/doctype/list/search')
        httpRespons = self.ranchbe.restService().get(qurl)
        serviceRespons = RbServiceResponse(httpRespons)
        data = serviceRespons.data
        doctypes = []
        for properties in data:
            doctype = Doctype(properties)
            doctypes.append(doctype)
        return doctypes

    # #
    #
    def clear(self):
        try:
            c = self.conn.cursor()
            c.execute('DELETE FROM doctypes WHERE 1=1')
        except Exception as e:
            print(str(e))

    # #
    #
    def add(self, doctypes):
        try:
            c = self.conn.cursor()
            for doctype in doctypes:
                # id, name, number, icon
                entry = [doctype.id, doctype.name, doctype.number, doctype.icon]
                c.execute('INSERT INTO doctypes VALUES (?, ?, ?, ?)', entry)
            self.conn.commit()
        except Exception as e:
            print(str(e))

    # #
    #
    def getFromId(self, doctypeId):
        c = self.conn
        cursor = c.execute('SELECT * FROM doctypes WHERE id=?', [doctypeId])
        row = cursor.fetchone()
        return row

    # #
    #
    def getContent(self):
        c = self.conn.cursor()
        c.execute('SELECT * FROM doctypes')
        datas = c.fetchall()
        return datas
