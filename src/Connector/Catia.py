# -*- coding: iso-8859-1 -*-
#! /usr/bin/python

from Application.Catia import CatiaApplication, ProductInstance, Product, ProductWalker as SuperProductWalker, FileLink, FileLinks, Document as CatDocument
from PyQt5.QtWidgets import QTreeWidget, QTreeWidgetItem, QAbstractItemView, QMenu, QMessageBox, QHeaderView, QAction, QGridLayout
from PyQt5.QtGui import QIcon, QPixmap, QCursor, QColor, QBrush
from PyQt5.QtCore import Qt
from Connector.Ranchbe import ConnException, Docfile, ServiceNotFoundException
from Connector.Abstract import AbstractConnector
from Ranchbe.Entity import AccessCode, Document as RbDocument
import sip, os
from Converter import Catia as CatiaConverter
import logging

# #
#
class Catia(AbstractConnector):

    # #
    # @param Main.RbMain mainApp
    #
    def __init__(self, mainWin):
        self.name = "Catia"
        self.mainWin = mainWin
        
        """ @var Application.Catia.Catia """
        self.catia = CatiaApplication.get()

        """ @var Connector.Catia.ProductTreeItem """
        self.activeProductItem = None
        
        self.gridLayout = QGridLayout(mainWin.ui.appTab)
        self.gridLayout.setObjectName("gridLayoutApp")
        
        """ Catia product tree """
        self.productTreeWidget = ProductTreeWidget(mainWin.ui.appTab, self)
        self.gridLayout.addWidget(self.productTreeWidget, 2, 0, 1, 4)
        
        """ Catia documents links tree """
        self.linksTreeWidget = LinksTreeWidget(mainWin.ui.appTab, self)
        self.gridLayout.addWidget(self.linksTreeWidget, 1, 0, 1, 4)

        """ set ranchbe ref """
        self.ranchbe = mainWin.ranchbe

# #
#
class CatiaActions():

    # #
    # @param Main.RbMain mainApp
    #
    def __init__(self, treeWidget):
        self.treeWidget = treeWidget
        self.catiaConnector = treeWidget.catiaConnector

    # #
    #
    def getFilteredSelection(self):
        selected = self.treeWidget.selectedItems()
        return selected
            
    # #
    #
    def checkoutAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector
        ranchbe = catiaConnector.mainWin.ranchbe
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            catiaConnector.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return

        # Init helpers services
        workspace = catiaConnector.mainWin.config.get("workspace", "path")
        docfileService = ranchbe.getDocfileService()
        checkoutService = ranchbe.getCheckoutService()
        spacename = catiaConnector.mainWin.context.spacename
        toCheckout = []
        toDownload = []
        
        ''' Get docfile from filename '''
        ## @var treeItem QTreeWidgetItem
        unicity = []
        for treeItem in selected:
            filename = treeItem.fileLink.name
            
            if filename not in unicity :
                unicity.append(filename)
                try:
                    docfile = docfileService.getfromname(filename, spacename)
                    toCheckout.append(docfile)
                except Exception as e:
                    catiaConnector.mainWin.warningBox("checkout error", filename + " is not checkouted" + str(e))
                    continue
        
        unicity = []
        for docfile in toCheckout:
            documentId = docfile.parentId
            if documentId not in unicity :
                unicity.append(documentId)
                try:
                    dfs = checkoutService.checkoutDocument(documentId, spacename)
                    for df in dfs:
                        toDownload.append(df)
                except Exception as e:
                    catiaConnector.mainWin.warningBox("checkout error", str(e))
                    logging.debug(str(e))
        
        for docfile in toDownload:
            filename = docfile.name
            filepath = os.path.join(workspace, filename)
            replaceFile = True
            if os.path.isfile(filepath):
                buttonReply = catiaConnector.mainWin.questionBox("File is existing in Workspace", "The file "+filename+" is existing in workspace. \n Do you want to replace it? \n Click Yes to replace, No to Keep it")
                if buttonReply == QMessageBox.Yes:
                    replaceFile = True
                else:
                    replaceFile = False
                    
            if replaceFile:
                try:
                    filepath = docfileService.download(docfile.id, spacename, workspace)
                except Exception as e:
                    catiaConnector.mainWin.warningBox("Download failed", "File "+filename+" cant be download from server. \n BE CAREFUL, the document is checkouted but file in your wildspace is not updated|created. \n You must cancel the checkout and retry")
                    logging.debug(str(e))
                    continue

    # #
    #
    def workflowAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector
        ranchbe = catiaConnector.mainWin.ranchbe
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            catiaConnector.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return

        # Init helpers services
        docfileService = ranchbe.getDocfileService()
        workflowService = ranchbe.getWorkflowService()
        spacename = catiaConnector.mainWin.context.spacename
        docfiles = []
        
        ''' Get docfile from filename '''
        ## @var treeItem QTreeWidgetItem
        unicity = []
        for treeItem in selected:
            productInstance = treeItem.product
            filename = productInstance.fileLink.name
            
            if filename not in unicity :
                unicity.append(filename)
                try:
                    docfile = docfileService.getfromname(filename, spacename)
                    accessCode = int(docfile.accessCode)
                    if accessCode == 0 or accessCode == 5:
                        docfiles.append(docfile)
                    else:
                        raise(Exception("File is not free"))
                except Exception as e:
                    catiaConnector.mainWin.warningBox("workflowAction error", filename +": "+ str(e))
                    continue
        
        webEngineView = catiaConnector.mainWin.serverPanel.webView.getWebEngineView()
        workflowService.start(docfiles, catiaConnector.mainWin.context, webEngineView)
        catiaConnector.mainWin.setCurrentTab("webview")

    # #
    #
    def saveInWildspaceAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector
        
        # Init helpers services
        workspace = catiaConnector.mainWin.config.get("workspace", "path")
        
        ''' Get docfile from filename '''
        ## @var treeItem QTreeWidgetItem
        unicity = []
        for treeItem in selected:
            filelink = treeItem.getFileLink()
            filename = filelink.name
            
            if filename not in unicity :
                unicity.append(filename)
                try:
                    fullName = os.path.join(workspace, filename)
                    catiaDocument = filelink.getDocument()
                    catiaDocument.saveAs(fullName)
                    treeItem.fromLink(filelink)
                except Exception as e:
                    catiaConnector.mainWin.warningBox("save error", str(e))
                    continue

    # #
    #
    def checkinAndKeepAction(self):
        return self.checkinAction(True)

    # #
    #
    def checkinAction(self, checkinandkeep=False):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector
        ranchbe = catiaConnector.mainWin.ranchbe
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            catiaConnector.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return

        # Init helpers services
        workspace = catiaConnector.mainWin.config.get("workspace", "path")
        docfileService = ranchbe.getDocfileService()
        checkinService = ranchbe.getCheckinService()
        wsService = ranchbe.getWildspaceService()
        spacename = catiaConnector.mainWin.context.spacename
        toUpload = []
        products = []
        
        ''' Get docfile from filename '''
        ## @var treeItem QTreeWidgetItem
        unicity = []
        for treeItem in selected:
            filelink = treeItem.getFileLink()
            filename = filelink.name
            catProduct = filelink.getDocument().getProduct()
            
            if filename not in unicity :
                unicity.append(filename)
                products.append(catProduct)
                
                try:
                    docfile = filelink.docfile
                except:
                    try:
                        docfile = docfileService.getfromname(filename, spacename)
                        docfile.setFilelink(filelink)
                    except Exception as e:
                        catiaConnector.mainWin.warningBox("checkin error", filename + " is not existing" + str(e))
                        continue
                toUpload.append(docfile)
                
                #get other docfile of document
                docfiles = docfileService.getOfDocument(docfile.parentId, spacename)
                for df in docfiles:
                    toUpload.append(df)
        
        ''' Upload to wildspace '''
        unicity = []
        rbDocuments = {}
        for docfile in toUpload:
            try:
                filename = docfile.name
                filepath = os.path.join(workspace, filename)
                wsService.upload(filepath, 1)
                if docfile.parentId not in unicity:
                    unicity.append(docfile.parentId)
                    rbDocument = RbDocument()
                    rbDocument.id = docfile.parentId
                    rbDocument.spacename = spacename
                    rbDocument.docfiles.append(docfile)
                    rbDocuments[docfile.parentId] = rbDocument
                else:
                    rbDocuments[docfile.parentId].docfiles.append(docfile)
            except Exception as e:
                catiaConnector.mainWin.warningBox("Upload failed", "File "+filename+" cant not be upload toserver.")
                logging.debug(str(e))
        
        for rbDocument in rbDocuments:
            ''' get document '''
            mainDocfile = rbDocument.docfiles[0]
            catFilelink = mainDocfile.catFilelink
            catDocument = catFilelink.getDocument()
            catProduct = catDocument.getProduct()
            rbDocument.setCatDocument(catDocument)

            ''' save document '''
            try:
                catDocument.save()
            except Exception as e:
                raise(Exception("Can not be saved : ", str(e)))

            ''' convert data '''
            converter = ConverterFactory.getConverter(catFilelink.type, "Ranchbe")

            ''' convert document, prepare datas for ranchbe '''
            try:
                pdmData = converter.convert(catDocument)
                pdmData.jsonFilename = catDocument.name + ".pdmdataset.json"
                pdmdataSetFile = pdmData.saveTo(workspace)
                wsService.upload(pdmdataSetFile, 1)
            except Exception as e:
                catiaConnector.mainWin.warningBox("Converter Failed", "Document "+rbDocument.name+" cant not be converted.")
                logging.debug(str(e))

            ''' set some attributes of ranchbe document from catia document '''
            try:
                rbDocument.description = pdmData.datas['properties']['product']['description']
            except:
                pass
        
        ''' Checkin '''
        if len(rbDocuments) > 0:
            try:
                serverPanel = catiaConnector.mainWin.serverPanel
                webEngineView = serverPanel.webView.getWebEngineView()
                checkinService.checkinDocuments(rbDocuments, checkinandkeep, webEngineView)
                catiaConnector.mainWin.setCurrentTab("webview")
            except Exception as e:
                catiaConnector.mainWin.warningBox("checkin error", str(e))

    ##
    #
    def cancelCheckoutAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector
        ranchbe = catiaConnector.mainWin.ranchbe
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            catiaConnector.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return
        
        # Init helpers services
        checkoutService = ranchbe.getCheckoutService()
        docfileService = ranchbe.getDocfileService()
        spacename = catiaConnector.mainWin.context.spacename

        ## @var treeItem QTreeWidgetItem
        unicity = []
        for treeItem in selected:
            filename = treeItem.fileLink.name
            
            if filename in unicity :
                continue
            unicity.append(filename)
            
            try:
                docfile = docfileService.getfromname(filename, spacename)
                documentId = docfile.parentId
                if documentId in unicity :
                    continue
                unicity.append(documentId)
                checkoutService.cancelcheckoutDocument(documentId, spacename)
            except Exception as e:
                catiaConnector.mainWin.warningBox("cancel checkout error", str(e))
                continue
        
    # #
    #
    def storeAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector
        ranchbe = catiaConnector.mainWin.ranchbe
        
        try:
            catiaConnector.mainWin.testConnexion()
        except ConnException as e:
            catiaConnector.mainWin.warningBox(e.title, str(e))
            return
        
        ''' Init helpers services '''
        wsService = ranchbe.getWildspaceService()
        workspace = catiaConnector.mainWin.config.get("workspace", "path")
        docfiles = []
        unicity = []
        
        if catiaConnector.mainWin.context.container == None:
            catiaConnector.mainWin.warningBox("store error", "You must select a container before.")
            return

        ## @var treeItem QTreeWidgetItem
        for treeItem in selected:
            fileLink = treeItem.getFileLink()
            filepath = fileLink.fullName
            filename = fileLink.name
            
            if filename not in unicity:
                unicity.append(filename)
                try:
                    catDocument = fileLink.getDocument()
                    converter = ConverterFactory.getConverter(fileLink.type, "Ranchbe")
                    pdmData = converter.convert(catDocument)
                except Exception as e:
                    pdmData = {}
                    logging.debug(str(e))
                
                try:
                    wsService.upload(filepath, 1)
                    docfile = Docfile({
                        "name": filename,
                        "containerId": catiaConnector.mainWin.context.container.id,
                        "spacename": catiaConnector.mainWin.context.spacename,
                        })
                    docfile.pdmData = pdmData
                    docfiles.append(docfile)
                    logging.debug("File is upload to wildspace: " + filename)
                    pdmData.jsonFilename = catDocument.name + ".pdmdataset.json"
                    pdmdataSetFile = pdmData.saveTo(workspace)
                    wsService.upload(pdmdataSetFile, 1)
                    os.remove(pdmdataSetFile)
                except Exception as e:
                    catiaConnector.mainWin.warningBox("store error", "Upload of file "+filename+" failed with reason : " + str(e) + " it will be ignored and NOT STORED")
                    continue
        
        if len(docfiles) > 0:
            try:
                ''' Call ranchbe store on toStore list '''
                serverPanel = catiaConnector.mainWin.serverPanel
                webEngineView = serverPanel.webView.getWebEngineView()
                checkinService = ranchbe.getCheckinService()
                context = catiaConnector.mainWin.context
                checkinService.storefiles(docfiles, context, webEngineView)
                catiaConnector.mainWin.setCurrentTab("webview")
            except Exception as e:
                catiaConnector.mainWin.warningBox("Store error", str(e))


    # # Convert object as Ranchbe PDM Data Set
    #
    def saveAsPdmDataSetAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector

        # Init helpers services
        workspace = catiaConnector.mainWin.config.get("workspace", "path")
        
        ''' Get docfile from filename '''
        ## @var treeItem QTreeWidgetItem
        filelinks = []
        unicity = []
        for treeItem in selected:
            # expand node and refresh it
            self.treeWidget.handleExpand(treeItem, True)
            
            filelink = treeItem.getFileLink()
            filename = filelink.name
            
            if filename not in unicity :
                unicity.append(filename)
                filelinks.append(filelink)
        
        ''' Upload to wildspace '''
        for filelink in filelinks:
            try:
                filename = filelink.name
                catProduct = filelink.getDocument().getProduct()
                
                ''' get document '''
                if type(catProduct) is ProductInstance:
                    catDocument = catProduct.getReferenceProduct().getDocument()
                elif type(catProduct) is Product:
                    catDocument = catProduct.getDocument()
                
                ''' convert data '''
                converter = ConverterFactory.getConverter(filelink.type, "Ranchbe")
                
                ''' convert document, prepare datas for ranchbe '''
                try:
                    pdmData = converter.convert(catDocument)
                    try:
                        pdmData.jsonFilename = filename + ".pdmdataset.json"
                        pdmData.saveTo(workspace)
                    except Exception as e:
                        logging.debug(str(e))
                except Exception as e:
                    logging.debug(str(e))
                
            except Exception as e:
                catiaConnector.mainWin.warningBox("Conversion failed", "File "+filename+" - Exception: "+str(e))
                logging.debug(str(e))

    ##
    # @param item QTreeWidgetItem
    #
    def getObjectDetailAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector
        ranchbe = catiaConnector.mainWin.ranchbe
        
        ## @var treeItem QTreeWidgetItem
        treeItem = selected[0]
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            catiaConnector.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return
        
        try:
            documentService = ranchbe.getService("document")
        except ServiceNotFoundException as e:
            catiaConnector.mainWin.warningBox("No service for this", str(e))
        except:
            return
        
        try:
            docfile = treeItem.getProduct().docfile
            documentId = docfile.parentId
            
            if hasattr(docfile, "spacename"):
                spacename = docfile.spacename
            else:
                spacename = catiaConnector.mainWin.context.spacename
    
            webEngineView = catiaConnector.mainWin.ui.webEngineView
            documentService.getDetails(documentId, spacename, webEngineView)
        except Exception as e:
            catiaConnector.mainWin.warningBox(str(e))
            
        catiaConnector.mainWin.setCurrentTab("webview")


    # #
    #
    def selectInCatiaTreeAction(self):
        catiaConnector = self.catiaConnector
        selected = self.getFilteredSelection()
        selection = catiaConnector.catia.comApplication.ActiveDocument.Selection
        selection.Clear()
        
        ## @var treeItem QTreeWidgetItem
        for treeItem in selected:
            productInstance = treeItem.product
            selection.Add(productInstance.comObject)

    # #
    #
    def selectFromCatiaTreeAction(self):
        selection = self.catiaConnector.comApplication.ActiveDocument.Selection
        
        ## @var treeItem QTreeWidgetItem
        for i in range(1, selection.Count2):
            item = selection.Item2(i)
            if(item.Type == "Product"):
                productInstance = item.LeafProduct
                ''' get path from child to head '''
                parent = productInstance.parent.parent
                while parent.Type == "Product":
                    parent = productInstance.parent.parent
                

    # #
    # Try to load CATIA definition datas. Equiv to CATIA command Load on selection.
    # @param Application.Catia.Products products
    #
    def loadProductDatas(self, products):
        catiaConnector = self.catiaConnector
        selection = catiaConnector.catia.newSelection()
        for productInstance in products.toDict():
            try:
                productInstance.getReferenceProduct()
            except Exception as e:
                selection.Add(productInstance.comObject)
                logging.debug(str(e))
        try:
            count = selection.Count
            if count > 0:
                catiaConnector.catia.comApplication.StartCommand(catiaConnector.catia.CATIA_COMMAND_LOAD)
                selection.Clear()
        except Exception as e:
            logging.debug("Connector.catia.loadProductDatas error: "+str(e))

    # #
    #
    #
    def loadDatasAction(self):
        selected = self.getFilteredSelection()
        products = []
        for item in selected:
            products.append(item.product)
        self.loadProductDatas(products)

    ##
    #
    def replaceByVaulfileAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector
        ranchbe = catiaConnector.mainWin.ranchbe

        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            catiaConnector.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return

        # Init helpers services
        docfileService = ranchbe.getDocfileService()
        spacename = catiaConnector.mainWin.context.spacename
        products = []
        
        ''' Get docfile from filename '''
        ## @var treeItem QTreeWidgetItem
        unicity = []
        for treeItem in selected:
            productInstance = treeItem.product
            filename = productInstance.fileLink.name
            if filename not in unicity :
                unicity.append(filename)
                products.append(productInstance)
                try:
                    docfile = docfileService.getfromname(filename, spacename)
                    productInstance.docfile = docfile
                except Exception as e:
                    catiaConnector.mainWin.warningBox("error", filename, str(e))
                    continue
        
        ''' replace reference of wildspace document to server document '''
        try:
            os.path.join
            term1 = catiaConnector.mainWin.config.get("localaccess", "privatepath")
            term2 = catiaConnector.mainWin.config.get("localaccess", "useraccesspath")
        except Exception as e:
            logging.debug(str(e))
        
        for productInstance in products:
            try:
                docfile = productInstance.docfile
                vaultFilepath = docfile.path.replace(term1, term2)
                vaultFilepath = os.path.join(vaultFilepath, docfile.name)
                vaultFilepath = vaultFilepath.replace('/', '\\')
                productInstance.replace(vaultFilepath)
            except Exception as e:
                catiaConnector.mainWin.warningBox("replace error: ", str(e))

    ##
    #
    def replaceByWildspaceFileAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector

        for item in selected:
            try:
                productInstance = item.productInstance
                filename = productInstance.fileLink.name
                wildspace = catiaConnector.mainWin.config.get("workspace", "path")
                filepath = os.path.join(wildspace, filename)
                if os.path.isfile(filepath):
                    pass
            except Exception as e:
                logging.debug(str(e))
                
        '''
        Sub CATMain()
        Dim productDocument1 As ProductDocument
        Set productDocument1 = CATIA.ActiveDocument
        Dim product1 As Product
        Set product1 = productDocument1.Product
        Dim products1 As products
        Set products1 = product1.products
        Dim product2 As Product
        Set product2 = products1.Item("Rotor de queue.1")
        Set Nothing1 = products1.ReplaceComponent(product2, "Z:\WildSpace2\rotor_queue.CATPart", True)
        End Sub
        '''

    ##
    #
    def getRanchbeDatasAction(self):
        selected = self.getFilteredSelection()
        catiaConnector = self.catiaConnector

        try:
            catiaConnector.mainWin.testConnexion()
        except Exception as e:
            catiaConnector.mainWin.warningBox(e.title, str(e))
            return
        
        docfileService = catiaConnector.mainWin.ranchbe.getDocfileService()
        spacename = catiaConnector.mainWin.context.spacename
        
        ##
        for item in selected:
            item.getDataFromRanchbe(docfileService, spacename)
            
# #
#
#
class ProductTreeWidget(QTreeWidget):
    # # Catia product tree
    #
    
    # #
    # @param Main.RbMain mainApp
    # @param Connector.Catia catiaConnector
    #
    def __init__(self, parent, catiaConnector):
        QTreeWidget.__init__(self, parent)
        
        self.setLineWidth(0)
        self.setObjectName("catiaTreeView")
        self.catiaConnector = catiaConnector
        self.productWalker = None
        
        #Features
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.setSortingEnabled(True)
        
        #Connect
        self.itemExpanded.connect(self.handleExpand)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.openMenu)
        
        #Headers
        self.headerItem().setText(ProductTreeItem.INSTANCE_NAME_COL, "Instance")
        self.headerItem().setText(ProductTreeItem.PARTNUMBER_COL, "Part Number")
        self.headerItem().setText(ProductTreeItem.DESCRIPTION_COL, "Description")
        self.headerItem().setText(ProductTreeItem.RANCHBE_COL, "isInRanchbe")
        self.headerItem().setText(ProductTreeItem.RB_ACCESS_COL, "Access")
        self.headerItem().setText(ProductTreeItem.RB_LIFESTAGE__COL, "LifeStage")
        self.headerItem().setText(ProductTreeItem.VERSION_COL, "Version")
        self.headerItem().setText(ProductTreeItem.NAME_COL, "Document")
        self.headerItem().setText(ProductTreeItem.FULLNAME_COL, "File")
        
        # column size
        header = self.header()
        header.setDefaultSectionSize(200)
        header.resizeSections(QHeaderView.ResizeToContents)
        
        header.setSectionResizeMode(QHeaderView.Interactive)
        header.setStretchLastSection(False)
        
        rootItem = QTreeWidgetItem(["Catia"])
        rootItem.setIcon(0, Ressources.get().catiaIcon)
        rootItem.setDisabled(False)
        rootItem.isRoot = True
        rootItem.loaded = False
        #rootItem.setData(0, Qt.UserRole + 1, "catia type")
        #rootItem.setFlags(Qt.ItemIsEnabled) ##Not selectable item
        self.addTopLevelItem(rootItem)
        self.rootItem = rootItem
        
        self.menuActions = CatiaActions(self)


    # #
    #
    def getProductWalker(self):
        if self.productWalker == None:
            self.productWalker = ProductWalker(self.catiaConnector.catia.activeDocument)
        return self.productWalker

    # #
    #
    # @see https://www.qtcentre.org/threads/39342-how-to-use-custom-type-items-in-QTreeWidget
    # @param QTreeWidgetItem|list item
    # @param boolean refresh
    # @return void
    #
    def handleExpand(self, item, refresh=False):
        if isinstance(item, list):
            items = item
        else:
            items = [item]

        ''' is conected ? '''
        try:
            self.catiaConnector.mainWin.testConnexion()
            connected = True
            docfileService = self.catiaConnector.mainWin.ranchbe.getDocfileService()
            spacename = self.catiaConnector.mainWin.context.spacename
        except ConnException as e:
            connected = False
            docfileService = None
            spacename = ""

        for item in items:
            ''' Set loaded attr to indicate that item children are yet loaded '''
            if not hasattr(item,"loaded"):
                item.loaded = False
    
            ''' If item is loaded and refresh is not require, do nothing '''
            if item.loaded == True and refresh == False :
                logging.debug("Item "+item.text(0)+" is expand but not refresh")
                return
            
            ''' If refresh require, delete all actual children '''
            if refresh == True :
                logging.debug("refresh tree item " + item.text(0))
                c = item.takeChildren()
                for t in c:
                    sip.delete(t)
                c = None
            
            ''' if connected, get datas from ranchbe server '''
            if connected:
                try:
                    item.getDataFromRanchbe(docfileService, spacename)
                except Exception as e :
                    logging.debug(e)
            
            try:
                ''' load product instance and reference product '''
                if not hasattr(item, 'product'):
                    raise(Exception("item has none product"))
                
                if item.product.__class__ != ProductInstance:
                    raise(Exception("item.product must be a ProductInstance"))
                
                if item.product.referenceProduct == None:
                    item.product.loadReferenceProduct()
                referenceProduct = item.product.getReferenceProduct()
                depth = item.product.catiaDepth
                basePath = item.product.catiaPath
                    
            except Exception as e:
                logging.debug(str(e))
                continue

            try:
                '''
                load children
                populate children attr of productInstance 
                '''
                referenceProduct.children = []
                productWalker = self.getProductWalker()
                children = productWalker.loadChildren(item.product, depth, basePath)
                ''' load datas before load reference product '''
                self.catiaConnector.loadProductDatas(children)
                
                logging.debug("handleExpand:productInstance.catiaPath", item.catiaPath)
                
                ''' populate the view from productInstance.children '''
                productWalker.populateViewModel(children, item)

                item.loaded = True
            except Exception as e:
                logging.debug(str(e))
                continue
            
            ''' Load ranchbe data from children if connected '''
            if connected:
                try:
                    for i in range(0, item.childCount()):
                        child = item.child(i)
                        child.getDataFromRanchbe(docfileService, spacename)
                except Exception as e:
                    logging.debug(str(e))
            else:
                try:
                    self.catiaConnector.loadProductDatas(referenceProduct.children)
                except Exception as e:
                    logging.debug(str(e))
        # End for

        self.header().resizeSections(QHeaderView.ResizeToContents)


    # #
    #
    def refreshAction(self):
        selected = self.menuActions.getFilteredSelection()
        if selected != None:
            if isinstance(selected, list):
                for item in selected:
                    self.handleExpand(item, True)
            else:
                self.handleExpand(selected, True)

    # #
    #
    def openMenu(self):
        selected = self.selectedItems()
        if(len(selected)==0):
            return
        user = self.catiaConnector.mainWin.context.user
        menu = QMenu(self.catiaConnector.mainWin)
        
        if hasattr(selected[0], "isRoot"):
            connectAction = QAction("Connect/Refresh Head")
            connectAction.triggered.connect(self.connectAppAction)
            connectAction.setToolTip("Connect to CATIA application")
            menu.addAction(connectAction)
            return menu.exec_(QCursor.pos())

        docfile = None
        productInstance = None
        
        try:
            productInstance = selected[0].product
            if hasattr(productInstance, "docfile"):
                docfile = productInstance.docfile
        except Exception as e :
            logging.debug(str(e))
        
        menu.addSeparator()
        
        if docfile != None:
            
            detailAction = QAction("Get RbDocument Details")
            detailAction.triggered.connect(self.menuActions.getObjectDetailAction)
            detailAction.setToolTip("Get detail page about ranchbe document associated to this product")
            menu.addAction(detailAction)
            
            menu.addSeparator()
            
            if int(docfile.accessCode) == 1 and docfile.lockById == user.id:
                checkinAction = QAction("Checkin")
                checkinAction.triggered.connect(self.menuActions.checkinAction)
                checkinAction.setToolTip("Chekin this file in ranchbe server")
                menu.addAction(checkinAction)
                
                updateAction = QAction("Checkin And Keep")
                updateAction.triggered.connect(self.menuActions.checkinAndKeepAction)
                updateAction.setToolTip("Chekin document files in ranchbe server, but keep it as checkouted")
                menu.addAction(updateAction)
                
                cancelcoAction = QAction("Cancel Checkout")
                cancelcoAction.triggered.connect(self.menuActions.cancelCheckoutAction)
                cancelcoAction.setToolTip("Cancel checkout; files are let in wildspace")
                menu.addAction(cancelcoAction)
                
            elif int(docfile.accessCode) == 0:
                checkoutAction = QAction("Checkout")
                checkoutAction.triggered.connect(self.menuActions.checkoutAction)
                checkoutAction.setToolTip("Checkout this document and copy files into wildspace")
                menu.addAction(checkoutAction)
                
            menu.addSeparator()
                
            replaceByVaulfileAction = QAction("Replace By vault version")
            replaceByVaulfileAction.triggered.connect(self.menuActions.replaceByVaulfileAction)
            replaceByVaulfileAction.setToolTip("Replace current referenced file by the vaulted file version")
            menu.addAction(replaceByVaulfileAction)
                
            menu.addSeparator()
            
            workflowAction = QAction("Run a Workflow")
            workflowAction.triggered.connect(self.menuActions.workflowAction)
            workflowAction.setToolTip("Run a workflow")
            menu.addAction(workflowAction)

        else:
            storeAction = QAction("Store")
            storeAction.triggered.connect(self.menuActions.storeAction)
            storeAction.setToolTip("Store this file in ranchbe server")
            menu.addAction(storeAction)
        
        menu.addSeparator()
        
        saveinwildspaceAction = QAction("Save In Wildspace")
        saveinwildspaceAction.triggered.connect(self.menuActions.saveInWildspaceAction)
        saveinwildspaceAction.setToolTip("Save file in wildspace")
        menu.addAction(saveinwildspaceAction)
        
        loadDatasAction = QAction("Load")
        loadDatasAction.triggered.connect(self.menuActions.loadDatasAction)
        loadDatasAction.setToolTip("Load catia document to access to description")
        menu.addAction(loadDatasAction)
        
        getRanchbeDatasAction = QAction("Get Ranchbe Datas")
        getRanchbeDatasAction.triggered.connect(self.menuActions.getRanchbeDatasAction)
        getRanchbeDatasAction.setToolTip("Try to get ranchbe document of this catia file")
        menu.addAction(getRanchbeDatasAction)

        saveAsPdmDataSetAction = QAction("Save Pdm Data Set")
        saveAsPdmDataSetAction.triggered.connect(self.menuActions.saveAsPdmDataSetAction)
        saveAsPdmDataSetAction.setToolTip("Save Pdm Data Set To file")
        menu.addAction(saveAsPdmDataSetAction)

        menu.addSeparator()
        
        selectInCatiaTreeAction = QAction("Select in Catia Tree")
        selectInCatiaTreeAction.triggered.connect(self.menuActions.selectInCatiaTreeAction)
        selectInCatiaTreeAction.setToolTip("Select this parts in catia structure tree")
        menu.addAction(selectInCatiaTreeAction)

        selectFromCatiaTreeAction = QAction("Select From Catia Tree")
        selectFromCatiaTreeAction.triggered.connect(self.menuActions.selectFromCatiaTreeAction)
        selectFromCatiaTreeAction.setToolTip("Select item from catia selected items")
        menu.addAction(selectFromCatiaTreeAction)

        menu.addSeparator()
        
        refreshAction = QAction("Refresh")
        refreshAction.triggered.connect(self.refreshAction)
        refreshAction.setToolTip("Refresh this view")
        menu.addAction(refreshAction)
        
        menu.exec_(QCursor.pos())


    # #
    #
    def connectAppAction(self):
        
        catia = self.catiaConnector.catia
        
        try:
            catia.connect(True)
        except Exception as e:
            logging.debug(str(e))
            return

        try:
            activeDocument = catia.getActiveDocument()
            activeProduct = activeDocument.getProduct()
            activeProduct.populate()
            
        except Exception as e:
            logging.debug("can not connect to active document", str(e))
            ''' delete existing children items '''
            if self.rootItem != None:
                c = self.rootItem.takeChildren()
                for t in c:
                    sip.delete(t)
            return
        
        try:
            ''' delete existing children items '''
            if self.rootItem != None:
                c = self.rootItem.takeChildren()
                for t in c:
                    sip.delete(t)

            activeInstance = ProductInstance(activeProduct.comObject)
            self.activeProductItem = ProductTreeItem(self.rootItem) #QTreeWidgetItem
            self.activeProductItem.fromProductInstance(activeInstance)
            
        except Exception as e:
            logging.debug(str(e))
            return
        
        self.header().resizeSections(QHeaderView.ResizeToContents)


# #
#
class ProductTreeItem(QTreeWidgetItem):
    
    INSTANCE_NAME_COL = 0
    PARTNUMBER_COL = 1
    DESCRIPTION_COL = 2
    RANCHBE_COL = 3
    RB_ACCESS_COL = 4
    RB_LIFESTAGE__COL = 5
    NAME_COL = 6
    FULLNAME_COL = 7
    VERSION_COL = 8

    def __init__(self, *args, **kwargs):
        QTreeWidgetItem.__init__(self, *args, **kwargs)
        
        ''' @var boolean '''
        self.loaded = False
    
        ''' @var boolean '''
        self.readOnly = True
    
        ''' @var boolean '''
        self.saved = False
        
        ''' @var Application.Catia.Product or ProductInstance '''
        self.product = None
        
        ''' @var string used to get instance object from the root catia document. @see ProductInstance.factoryFromPath '''
        self.catiaPath = ""
        
        ''' Item depth in the catia tree '''
        self.catiaDepth = 1

    # #
    #
    def getProduct(self):
        if self.product.__class__ == ProductInstance :
            if self.product.referenceProduct == None:
                self.product.loadReferenceProduct()
            referenceProduct = self.product.getReferenceProduct()
        else:
            referenceProduct = self.product
        
        return referenceProduct

    # #
    # @var Application.Catia.ProductInstance productInstance
    #
    def fromProductInstance(self, productInstance):
        if productInstance.__class__ != ProductInstance:
            raise(Exception("parent must be a ProductInstance"))
        
        self.catiaPath = productInstance.catiaPath
        self.catiaDepth = productInstance.catiaDepth
        self.product = productInstance
        
        comProduct = productInstance.comObject
        definition = ""
        description = ""
        
        if hasattr(productInstance, "definition"):
            definition = productInstance.definition
            description = definition
        if hasattr(productInstance, "descriptionRef"):
            description = definition + productInstance.descriptionRef
            
        try:
            fileLink = productInstance.getFileLink()
            fullName = fileLink.fullName
            name = fileLink.name
            filetype = fileLink.type
        except:
            fullName = ""
            name = ""
            filetype = ""
        
        version = productInstance.revision
        instanceName = productInstance.name
        partNumber = productInstance.partNumber
        
        if len(comProduct.Products) > 0:
            self.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
        
        ressource = Ressources.get()
        if filetype == ".CATProduct":
            self.setIcon(0,ressource.assemblyIcon)
        else:
            self.setIcon(0,ressource.partIcon)
            
        self.setData(self.INSTANCE_NAME_COL, 0, instanceName)
        self.setData(self.NAME_COL, 0, name)
        self.setData(self.PARTNUMBER_COL, 0, partNumber)
        self.setData(self.FULLNAME_COL, 0, fullName)
        self.setData(self.DESCRIPTION_COL, 0, description)
        self.setData(self.VERSION_COL, 0, version)
        self.setData(self.RANCHBE_COL, 0, "Not Connected")
        self.setData(self.RB_ACCESS_COL, 0, "")
        self.setData(self.RB_LIFESTAGE__COL, 0, "")
        
        return self


    """
    get datas from ranchbe and attach to current item
    """
    def getDataFromRanchbe(self, docfileService, spacename):
        tooltip = ""
        try:
            filename = self.product.fileLink.name
            docfile = docfileService.getfromname(filename, spacename)
            self.product.docfile = docfile
            accessLabel = AccessCode.getName(docfile.accessCode)
            isInranchbe = "Yes"
            if int(docfile.accessCode) > 0:
                accessLabel = accessLabel+" By "+docfile.lockByUid + " Since "+docfile.locked
                tooltip = "This file is existing in Ranchbe, and it locked"
            elif int(docfile.accessCode) == 0:
                tooltip = "This file is existing in Ranchbe, you must checkout it to edit"
                
        except Exception as e:
            logging.debug(str(e))
            isInranchbe = "No"
            tooltip = "This file may be stored"
            accessLabel = "Not found"

        self.setData(self.RANCHBE_COL, 0, isInranchbe)
        self.setToolTip(self.RANCHBE_COL, tooltip)
        self.setData(self.RB_ACCESS_COL, 0, accessLabel)

# #
#
class ProductWalker(SuperProductWalker):
    
    # #
    # @param dict children dict of ProductInstance
    # @param QTreeWidgetItem parentItem
    def populateViewModel(self, children, parentItem):
        # @var ProductInstance child
        for child in children:
            myitem = ProductTreeItem(parentItem)
            myitem.fromProductInstance(child)


# #
#
class LinksTreeWidget(QTreeWidget):
    # # Catia documents links tree
    #
    
    # #
    # @param Main.RbMain mainApp
    # @param Connector.Catia catiaConnector
    #
    def __init__(self, parent, catiaConnector):
        QTreeWidget.__init__(self, parent)
        """ Catia documents links tree """
        self.setLineWidth(0)
        self.setObjectName("catiaLinksTreeView")
        self.catiaConnector = catiaConnector
        self.menuActions = CatiaActions(self)
        self.productWalker = None
        
        #Features
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.setSortingEnabled(True)
        
        #Connect
        self.itemExpanded.connect(self.handleExpand)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.openMenu)
        
        #Headers
        self.headerItem().setText(LinksTreeItem.PARTNUMBER_COL, "Part Number")
        self.headerItem().setText(LinksTreeItem.DESCRIPTION_COL, "Description")
        self.headerItem().setText(LinksTreeItem.RANCHBE_COL, "isInRanchbe")
        self.headerItem().setText(LinksTreeItem.RB_ACCESS_COL, "Access")
        self.headerItem().setText(LinksTreeItem.RB_LIFESTAGE__COL, "LifeStage")
        self.headerItem().setText(LinksTreeItem.NAME_COL, "Document")
        self.headerItem().setText(LinksTreeItem.FULLNAME_COL, "File")
        
        # column size
        header = self.header()
        header.setDefaultSectionSize(200)
        header.resizeSections(QHeaderView.ResizeToContents)
        
        header.setSectionResizeMode(QHeaderView.Interactive)
        header.setStretchLastSection(False)
        
        rootItem = QTreeWidgetItem(["Catia"])
        rootItem.setIcon(0, Ressources.get().catiaIcon)
        rootItem.setDisabled(False)
        rootItem.isRoot = True
        rootItem.loaded = False
        self.addTopLevelItem(rootItem)
        self.rootItem = rootItem

    # #
    #
    def getProductWalker(self):
        if self.productWalker == None:
            self.productWalker = LinksWalker(self.catiaConnector.catia.activeDocument)
        return self.productWalker

    # #
    #
    # @see https://www.qtcentre.org/threads/39342-how-to-use-custom-type-items-in-QTreeWidget
    # @param QTreeWidgetItem|list item
    # @param boolean refresh
    # @return void
    #
    def handleExpand(self, item, refresh=False):
        if isinstance(item, list):
            items = item
        else:
            items = [item]

        ''' is conected ? '''
        try:
            self.catiaConnector.mainWin.testConnexion()
            connected = True
            docfileService = self.catiaConnector.mainWin.ranchbe.getDocfileService()
            spacename = self.catiaConnector.mainWin.context.spacename
        except ConnException as e:
            connected = False
            docfileService = None
            spacename = ""

        for item in items:
            ''' Set loaded attr to indicate that item children are yet loaded '''
            if not hasattr(item, "loaded"):
                item.loaded = False
    
            ''' If item is loaded and refresh is not require, do nothing '''
            if item.loaded == True and refresh == False :
                logging.debug("Item "+item.text(0)+" is expand but not refresh")
                return
            
            ''' If refresh require, delete all actual children '''
            if refresh == True :
                logging.debug("refresh tree item " + item.text(0))
                c = item.takeChildren()
                for t in c:
                    sip.delete(t)
                c = None
                
            try:
                if item.__class__ == LinksTreeItem:
                    ''' if connected, get datas from ranchbe server '''
                    if connected:
                        try:
                            item.getDataFromRanchbe(docfileService, spacename)
                        except Exception as e :
                            logging.error(e)
                    
                    if item.fileLink == None:
                        raise(Exception("fileLink is not defined"))
                    
                    links = FileLinks(self.catiaConnector.catia)
                    links.extractFromStiDBItem(item.fileLink.stiDBItem, item.fileLink)
                    ''' populate the view from productInstance.children '''
                    LinksWalker.populateViewModelFromLinks(links.toDict(), item)
            except Exception as e:
                logging.error(str(e))
                continue
            
            item.loaded = True
            
            ''' Load ranchbe data from children if connected '''
            if connected:
                try:
                    for i in range(0, item.childCount()):
                        child = item.child(i)
                        child.getDataFromRanchbe(docfileService, spacename)
                except Exception as e:
                    logging.error(str(e))
            else:
                try:
                    children = item.getFileLink().getDocument().getProduct().getChildren()
                    if not(children == None):
                        try:
                            self.menuActions.loadProductDatas(children)
                        except Exception as e:
                            raise(Exception("error during menuActions.loadProductDatas %s" % str(e)))
                except Exception as e:
                    logging.error(str(e))
        # End for

        self.header().resizeSections(QHeaderView.ResizeToContents)

    # #
    #
    def refreshAction(self):
        selected = self.menuActions.getFilteredSelection()
        if selected != None:
            if isinstance(selected, list):
                for item in selected:
                    self.handleExpand(item, True)
            else:
                self.handleExpand(selected, True)

    # #
    #
    def openMenu(self):
        selected = self.selectedItems()
        if(len(selected)==0):
            return
        user = self.catiaConnector.mainWin.context.user
        menu = QMenu(self.catiaConnector.mainWin)
        
        if hasattr(selected[0], "isRoot"):
            connectAction = QAction("Connect/Refresh Head")
            connectAction.triggered.connect(self.connectAppAction)
            connectAction.setToolTip("Connect to CATIA application")
            menu.addAction(connectAction)
            return menu.exec_(QCursor.pos())

        docfile = None
        product = None
        
        try:
            product = selected[0].product
            if hasattr(product, "docfile"):
                docfile = product.docfile
        except Exception as e :
            logging.debug(str(e))
        
        menu.addSeparator()
        
        if docfile != None:
            
            detailAction = QAction("Get RbDocument Details")
            detailAction.triggered.connect(self.menuActions.getObjectDetailAction)
            detailAction.setToolTip("Get detail page about ranchbe document associated to this product")
            menu.addAction(detailAction)
            
            menu.addSeparator()
            
            if int(docfile.accessCode) == 1 and docfile.lockById == user.id:
                checkinAction = QAction("Checkin")
                checkinAction.triggered.connect(self.catiaConnector.checkinAction)
                checkinAction.setToolTip("Chekin this file in ranchbe server")
                menu.addAction(checkinAction)
                
                updateAction = QAction("Checkin And Keep")
                updateAction.triggered.connect(self.menuActions.checkinAndKeepAction)
                updateAction.setToolTip("Chekin document files in ranchbe server, but keep it as checkouted")
                menu.addAction(updateAction)
                
                cancelcoAction = QAction("Cancel Checkout")
                cancelcoAction.triggered.connect(self.menuActions.cancelCheckoutAction)
                cancelcoAction.setToolTip("Cancel checkout; files are let in wildspace")
                menu.addAction(cancelcoAction)
                
            elif int(docfile.accessCode) == 0:
                checkoutAction = QAction("Checkout")
                checkoutAction.triggered.connect(self.menuActions.checkoutAction)
                checkoutAction.setToolTip("Checkout this document and copy files into wildspace")
                menu.addAction(checkoutAction)

            menu.addSeparator()

            replaceByVaulfileAction = QAction("Replace By vault version")
            replaceByVaulfileAction.triggered.connect(self.menuActions.replaceByVaulfileAction)
            replaceByVaulfileAction.setToolTip("Replace current referenced file by the vaulted file version")
            menu.addAction(replaceByVaulfileAction)
                
            menu.addSeparator()
            
            workflowAction = QAction("Run a Workflow")
            workflowAction.triggered.connect(self.menuActions.workflowAction)
            workflowAction.setToolTip("Run a workflow")
            menu.addAction(workflowAction)

        else:
            if(self.catiaConnector.ranchbe.isConnected()):
                storeAction = QAction("Store")
                storeAction.triggered.connect(self.menuActions.storeAction)
                storeAction.setToolTip("Store this file in ranchbe server")
                menu.addAction(storeAction)
        
        menu.addSeparator()
        
        saveinwildspaceAction = QAction("Save In Wildspace")
        saveinwildspaceAction.triggered.connect(self.menuActions.saveInWildspaceAction)
        saveinwildspaceAction.setToolTip("Save file in wildspace")
        menu.addAction(saveinwildspaceAction)
        
        if(self.catiaConnector.ranchbe.isConnected()):
            getRanchbeDatasAction = QAction("Get Ranchbe Datas")
            getRanchbeDatasAction.triggered.connect(self.menuActions.getRanchbeDatasAction)
            getRanchbeDatasAction.setToolTip("Try to get ranchbe document of this catia file")
            menu.addAction(getRanchbeDatasAction)
        
        saveAsPdmDataSetAction = QAction("Save Pdm Data Set")
        saveAsPdmDataSetAction.triggered.connect(self.menuActions.saveAsPdmDataSetAction)
        saveAsPdmDataSetAction.setToolTip("Save Pdm Data Set To file")
        menu.addAction(saveAsPdmDataSetAction)

        menu.addSeparator()
        
        refreshAction = QAction("Refresh")
        refreshAction.triggered.connect(self.refreshAction)
        refreshAction.setToolTip("Refresh this view")
        menu.addAction(refreshAction)
        
        menu.exec_(QCursor.pos())

    # #
    #
    def connectAppAction(self):
        catia = self.catiaConnector.catia
        
        try:
            catia.connect(True)
        except Exception as e:
            logging.debug(str(e))
            return

        try:
            activeDocument = catia.getActiveDocument()
            activeProduct = activeDocument.getProduct()
            activeProduct.populate()
        except Exception as e:
            logging.debug("can not connect to active document", str(e))
            ''' delete existing children items '''
            if self.rootItem != None:
                c = self.rootItem.takeChildren()
                for t in c:
                    sip.delete(t)
            return
        
        try:
            ''' delete existing children items '''
            if self.rootItem != None:
                c = self.rootItem.takeChildren()
                for t in c:
                    sip.delete(t)

            self.activeProductItem = LinksTreeItem(self.rootItem) #QTreeWidgetItem
            oStiDBItem = catia.getCAIEngine().GetStiDBItemFromAnyObject(activeDocument.comObject)
            fileLink = FileLink()
            fileLink.setStiDbItem(oStiDBItem)
            self.activeProductItem.fromLink(fileLink)
            
        except Exception as e:
            logging.debug(str(e))
            return
        
        self.header().resizeSections(QHeaderView.ResizeToContents)


# #
#
class LinksTreeItem(QTreeWidgetItem):
    
    FULLNAME_COL = 1
    NAME_COL = 2
    PARTNUMBER_COL = 3
    DESCRIPTION_COL = 4
    RANCHBE_COL = 5
    RB_ACCESS_COL = 6
    RB_LIFESTAGE__COL = 7
    VERSION_COL = 8
    
    COLOR_GREEN = "#008000"
    COLOR_BLUE = "#006bb3"
    COLOR_RED = "#ff0000"
    COLOR_YELLOW = "#ffffcc"
    COLOR_WHITE = "#ffffff"
    COLOR_BLACK = "#000000"

    COLOR_CHECKIN = "#008000"
    COLOR_CHECKOUTED = "#ff0000"
    COLOR_NOTFOUND = "#000000"

    # #
    #
    def __init__(self, *args, **kwargs):
        QTreeWidgetItem.__init__(self, *args, **kwargs)
        
        ''' @var boolean '''
        self.loaded = False
    
        ''' @var boolean '''
        self.readOnly = True
    
        ''' @var boolean '''
        self.saved = False
        
        ''' @var FileLink '''
        self.fileLink = None
        
        ''' @var string used to get instance object from the root catia document. @see ProductInstance.factoryFromPath '''
        self.catiaPath = ""
        
        ''' Item depth in the catia tree '''
        self.catiaDepth = 1
        
        self.document = None
        self.product = None

    # #
    # @var FileLink link
    #
    def fromLink(self, fileLink:FileLink):
        if fileLink.__class__ != FileLink:
            raise(Exception("parent must be a FileLink"))
        
        self.fileLink = fileLink
        
        definition = ""
        description = ""
        fullName = fileLink.fullName
        name = fileLink.name
        filetype = fileLink.type
        partNumber = fileLink.partNumber
        
        if isinstance(fileLink.document, CatDocument):
            product = fileLink.document.getProduct()
            
            if hasattr(product, "definition"):
                definition = product.definition
                description = definition
            if hasattr(product, "descriptionRef"):
                description = definition + product.descriptionRef
        else:
            logging.debug("file link %s has none associated document", name)
        
        if filetype == ".CATProduct":
            if len(fileLink.stiDBItem.GetChildren()) > 0:
                self.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
            self.setIcon(0,Ressources.get().assemblyIcon)
        else:
            self.setIcon(0,Ressources.get().partIcon)
        
        self.setData(self.FULLNAME_COL, 0, fullName)
        self.setData(self.PARTNUMBER_COL, 0, partNumber)
        self.setData(self.NAME_COL, 0, name)
        self.setData(self.DESCRIPTION_COL, 0, description)
        self.setData(self.RANCHBE_COL, 0, "Not Connected")
        self.setData(self.RB_ACCESS_COL, 0, "")
        self.setData(self.RB_LIFESTAGE__COL, 0, "")
        
        return self

    """
    get datas from ranchbe and attach to current item
    """
    def getDataFromRanchbe(self, docfileService, spacename):
        tooltip = ""
        try:
            filename = self.fileLink.name
            docfile = docfileService.getfromname(filename, spacename)
            self.fileLink.docfile = docfile
            accessLabel = AccessCode.getName(docfile.accessCode)
            isInranchbe = "Yes"
            if int(docfile.accessCode) > 0:
                accessLabel = accessLabel+" By "+docfile.lockByUid + " Since "+docfile.locked
                tooltip = "This file is existing in Ranchbe, and it locked"
                foreground = QBrush(QColor(self.COLOR_CHECKOUTED))
            elif int(docfile.accessCode) == 0:
                tooltip = "This file is existing in Ranchbe, you must checkout it to edit"
                foreground = QBrush(QColor(self.COLOR_CHECKIN))
        except Exception as e:
            logging.debug(str(e))
            isInranchbe = "No"
            tooltip = "This file may be stored"
            accessLabel = "Not found"
            foreground = QBrush(QColor(self.COLOR_NOTFOUND))

        self.setForeground(self.FULLNAME_COL, foreground)
        self.setForeground(self.RB_ACCESS_COL, foreground)
        self.setForeground(self.RB_LIFESTAGE__COL, foreground)
        self.setForeground(self.RANCHBE_COL, foreground)

        self.setData(self.RANCHBE_COL, 0, isInranchbe)
        self.setToolTip(self.RANCHBE_COL, tooltip)
        self.setData(self.RB_ACCESS_COL, 0, accessLabel)

    """
    get fileLink associated to this item
    """
    def getFileLink(self):
        return self.fileLink

# #
#
class LinksWalker():

    # #
    # @param FileLinks links
    # @param QTreeWidgetItem parentItem
    @staticmethod
    def populateViewModelFromLinks(links, parentItem):
        for fileLink in links:
            if isinstance(fileLink.getDocument(), CatDocument):
                myitem = LinksTreeItem(parentItem)
                myitem.fromLink(fileLink)

# #
#
class ProductWalkerException(Exception):
    pass

# #
#
#
class Ressources():
    singleton = None
    
    def __init__(self):
        self.partIcon = QIcon()
        self.partIcon.addPixmap(QPixmap(":/cog"), QIcon.Normal, QIcon.Off)
        self.assemblyIcon = QIcon()
        self.assemblyIcon.addPixmap(QPixmap(":/cogAssembly"), QIcon.Normal, QIcon.Off)
        self.catiaIcon = QIcon()
        self.catiaIcon.addPixmap(QPixmap(":/cad"), QIcon.Normal, QIcon.Off)
        
    @staticmethod
    def get():
        if Ressources.singleton == None:
            Ressources.singleton = Ressources()
        return Ressources.singleton

# #
#
#
class ConverterFactory():
    
    """ static dict """
    map = {}
    """ static dict """
    registry = {}
    

    @staticmethod
    def getConverter(fromType, toType):
        fromType = fromType.replace(".", "").lower()
        toType = toType.replace(".", "").lower()
        c = fromType.title() + "To" + toType.title()
        
        if c in ConverterFactory.map:
            converterClass = ConverterFactory.map[c]
        else:
            converterClass = c

        if c in ConverterFactory.registry:
            return ConverterFactory.registry[c]
        
        try:
            converter = getattr(CatiaConverter, converterClass)()
            ConverterFactory.registry[converterClass] = converter
        except Exception as e:
            logging.debug("converterClass "+c+" is not instanciable: ", str(e))
        
        return converter;
