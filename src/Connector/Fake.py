# -*- coding: iso-8859-1 -*-
#! /usr/bin/python

from PyQt5.QtWidgets import QTreeWidget, QTreeWidgetItem, QAbstractItemView, QMenu
from PyQt5.QtGui import QIcon, QPixmap, QCursor
from PyQt5.QtCore import Qt
from Connector.Abstract import AbstractConnector
#from Application.Catia import ProductInstance, ProductDocument, FileLink

# #
#
class Fake(AbstractConnector):
    
    rootItem = None
    
    """ @var Application.Catia.Catia """
    application = None
    
    # #
    # @param Main.RbMain mainApp
    #
    def __init__(self, mainWin):
        self.name = "Fake"
        self.mainWin = mainWin
        self.ranchbe = mainWin.ranchbe
        
        self.treeWidget = QTreeWidget(mainWin.ui.appTab)
        self.treeWidget.setLineWidth(0)
        self.treeWidget.setObjectName("fakeTreeView")
        self.mainWin.ui.gridLayout_3.addWidget(self.treeWidget, 1, 0, 1, 4)
        
        #features
        self.treeWidget.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.treeWidget.setSortingEnabled(True)
        
        ##Connect
        self.treeWidget.itemExpanded.connect(self.handleExpand)
        self.treeWidget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.treeWidget.customContextMenuRequested.connect(self.openMenu)
        
        self.treeWidget.headerItem().setText(0, "Instance")
        self.treeWidget.headerItem().setText(1, "Document")
        self.treeWidget.headerItem().setText(2, "Part Number")
        self.treeWidget.headerItem().setText(3, "File")
        self.treeWidget.headerItem().setText(4, "Description")
        self.treeWidget.headerItem().setText(5, "Version")

        self.catiaIcon = QIcon()
        self.catiaIcon.addPixmap(QPixmap(":/info30.png"), QIcon.Normal, QIcon.Off)

        self.rootItem = QTreeWidgetItem(["Fake", ""])
        self.rootItem.setIcon(0,self.catiaIcon)
        self.rootItem.setDisabled(False)
        self.rootItem.rbType = "catiaroot"
        self.rootItem.loaded = True
        self.rootItem.setFlags(Qt.ItemIsEnabled) #Not selectable item
        self.treeWidget.addTopLevelItem(self.rootItem)
        self.activeProductItem = TreeWidgetCatiaItem(self.rootItem) #TreeWidgetCatiaItem QTreeWidgetItem
        self.activeProductItem.name = "product001"
        self.activeProductItem.instanceName = "product001.1"
        self.activeProductItem.compile()

    # #
    #
    def openMenu(self):
        selected = self.treeWidget.selectedItems()
        if(len(selected)==0):
            return
        
        menu = QMenu(self.mainWin)
        refreshAction = menu.addAction("Refresh")
        storeAction = menu.addAction("Store")
        checkoutAction = menu.addAction("Checkout")
        checkinAction = menu.addAction("Checkin")
        updateAction = menu.addAction("Checkin And Keep")
        cancelcoAction = menu.addAction("Cancel Checkout")
        action = menu.exec_(QCursor.pos())
        
        if action == refreshAction:
            self.refreshAction(selected)
        elif action == storeAction:
            self.storeAction(selected)
        elif action == checkoutAction:
            self.checkoutAction(selected)
        elif action == checkinAction:
            self.checkinAction(selected)
        elif action == updateAction:
            self.checkinAndKeepAction(selected)
        elif action == cancelcoAction:
            self.cancelCheckoutAction(selected)
   
    # #
    #
    # @see https://www.qtcentre.org/threads/39342-how-to-use-custom-type-items-in-QTreeWidget
    # @param QTreeWidgetItem|list item
    # @param boolean refresh
    # @return void
    #
    def handleExpand(self, item, refresh=False):
        
        if refresh == True:
            item.takeChildren()
        
        if item.loaded == False or refresh == True:
            for i in range(0,100):
                childItem = TreeWidgetCatiaItem(item)
                childItem.instanceName = "instance."+str(i)
                childItem.name = item.name+"."+str(i)
                childItem.partNumber = "partNumber"+str(i)
                childItem.compile()

# #
#
class ProductWalker:

    # #
    #
    def __init__(self):
        pass
    
    # #
    # @param Application.Catia.ProductInstance parent
    # @param string path
    # @param integer depth
    def loadChildren(self, parent):
        pass

    # #
    # @param dict children dict of ProductInstance
    # @param QTreeWidgetItem parentItem
    def populateViewModel(self, children, parentItem):
        pass

# #
#
class TreeWidgetCatiaItem(QTreeWidgetItem):
    
    ## @var boolean
    loaded = False

    ## @var boolean
    readOnly = True

    ## @var boolean
    saved = False
    
    ##
    instanceName = ""
    partNumber = ""
    fullname = ""
    description = ""
    version = ""
    
    INSTANCE_NAME_COL = 0
    NAME_COL = 1
    PARTNUMBER_COL = 2
    FULLNAME_COL = 3
    DESCRIPTION_COL = 4
    VERSION_COL = 5
    
    # #
    # @var Application.Catia.ProductInstance productInstance
    #
    def compile(self):
        self.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
        self.setData(self.INSTANCE_NAME_COL, 0, self.instanceName)
        self.setData(self.NAME_COL, 0, self.name)
        self.setData(self.PARTNUMBER_COL, 0, self.partNumber)
        self.setData(self.FULLNAME_COL, 0, self.fullname)
        self.setData(self.DESCRIPTION_COL, 0, self.description)
        self.setData(self.VERSION_COL, 0, self.version)
            
        return self
    
# #
#
class ProductWalkerException(Exception):
    pass
