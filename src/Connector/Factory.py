from Connector.Catia import Catia
from Connector.Fake import Fake

##
#
#
class ConnectorFactory():
    
    ##
    #
    def getConnector(self, name, mainApp):
        """
        @return Connector for name
        """
        try:
            connectorName = name.title()
            connectorName = "get"+connectorName+"Connector"
            func = getattr(self, connectorName)
            return func(mainApp)
        except ConnectorNotFoundException as e:
            raise Exception("connector " + connectorName + " is not available: " + str(e))
        
    ##
    #
    def getCatiaConnector(self, mainApp):
        return Catia(mainApp)

    ##
    #
    def getFakeConnector(self, mainApp):
        return Fake(mainApp)
        

class ConnectorNotFoundException(Exception):
    pass
    