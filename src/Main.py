#!/usr/bin/python
# https://fernandofreitasalves.com/how-to-create-an-application-with-auto-update-using-python-and-esky/
# https://pypi.org/project/esky/

import tempfile
import os
import glob
import re
from Tool import File
from datetime import datetime
import logging
# import sip


from PyQt5 import QtNetwork, QtWebEngineCore, QtWebEngineWidgets
from PyQt5.QtCore import Qt, QByteArray, QFileInfo, QUrl, QCoreApplication, QMimeData
from PyQt5.QtGui import QMovie, QIcon, QPixmap, QCursor
from PyQt5.QtWidgets import QMainWindow, QWidget, QLabel, QMessageBox, QAbstractItemView, QMenu, QTableWidgetItem, QTreeWidgetItem, QTreeWidget, QDialog, QDialogButtonBox, QFileDialog, QAction, QHeaderView

from ConfigDialogBoxQt5 import Ui_Config
from MainWindowQt5 import Ui_MainWindow
from ConnexionDialogBoxQt5 import Ui_ConnexionDialogBox
from Connector.Factory import ConnectorFactory
from Connector.Ranchbe import Ranchbe, ConnException, ServiceNotFoundException, DoctypeLocalCache
from Ranchbe.Entity import Project, Space, Container, Document, AccessCode, Category, Docfile
import Tool
# #from pywin import debugger


# # Main Windows
#
class RbMain(QMainWindow):
    
    IMG_PATH = ""
    
    # # 
    #
    def __init__(self, wicoti, parent=None):
        super (RbMain, self).__init__(parent)
        
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
      
        self.ui.actionVersion.setText("Version: " + wicoti.VERSION)

        self.connector = None
        self.resize(1600, 900)
        self.wicoti = wicoti
        self.ranchbe = self.wicoti.getRanchbeConnector()
        self.ranchbe.setMainApp(self)
        self.config = self.wicoti.getConfig()
            
        ''' init the doctype cache '''
        DoctypeLocalCache(os.path.join(wicoti.APPLICATION_PATH, 'data', 'cache.db'))
        
        self.context = Context(self)
        self.context.load(self.config)
        
        self.localPanel = RbLocal(self)
        self.localPanel.connect()
        self.localPanel.refreshAction()

        self.serverPanel = RbServer(self)
        self.serverPanel.connect()

        # # unactivate application tab :
        self.ui.tabWidget.removeTab(2)
        # self.applicationPanel = RbApplication(self)
        # # End of unactivate application tab
        
        self.ui.actionConfig.triggered.connect(self.getConfigWin)
        self.ui.actionConnect.triggered.connect(self.getConnectWin)
        self.ui.actionDisconnect.triggered.connect(self.logout)
        self.ui.actionworkitem.triggered.connect(self.workitemAction)
        self.ui.actionbookshop.triggered.connect(self.bookshopAction)
        self.ui.actioncadlib.triggered.connect(self.cadlibAction)
        self.ui.actionmockup.triggered.connect(self.mockupAction)
        self.ui.actionSite_Web.triggered.connect(self.gotoWebsite)
        
        ''' add ping current user 
        actionUserping = QAction(self)
        actionUserping.setObjectName("actionUserping")
        actionUserping.setText("Ping User")
        self.ui.menuConfig.addAction(actionUserping)
        actionUserping.triggered.connect(self.userpingAction)
        '''
        
        ''' show status message '''
        self.ui.statusbar.showMessage("Ready")

    # #
    #
    def showEvent(self, *args, **kwargs):
        ''' Display config dialog box '''
        # if self.wicoti.isInit == False:
            # self.getConfigWin()
        return QMainWindow.showEvent(self, *args, **kwargs)

    # #
    #
    def setAppuserModel(self, myappid):
        if (os.name == 'nt'):
            import ctypes
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    # #
    #
    def gotoWebsite(self):
        webView = self.serverPanel.webView
        qurl = QUrl()
        qurl.setUrl(self.wicoti.WEB_SITE)
        webView.load(qurl, 'get')
        self.setCurrentTab("webview")

    # #
    #
    def getConfigWin(self):
        configWin = ConfigDialogBox(self)
        configWin.setupUi()
        configWin.connect()
        configWin.load(self.config)
        configWin.showme()

    # #
    #
    def getConnectWin(self):
        connectWin = ConnectDialogBox(self)
        connectWin.setupUi()
        connectWin.connect()
        connectWin.showme()

    # #
    #
    def getWaiterWin(self):
        widget = QWidget()
        label = QLabel(widget)
        movie = QMovie(":/img/waiter/328.gif")
        label.setMovie(movie)
        return widget

    # #
    #
    def warningBox(self, title="WARNING MESSAGE", message="A Warning error is occured"):
        widget = QWidget()
        QMessageBox.warning(widget, title, message)

    # #
    #
    def errorBox(self, title="WARNING MESSAGE", message="A Warning error is occured"):
        widget = QWidget()
        QMessageBox.critical(widget, title, message)

    # #
    #
    def questionBox(self, title="WHAT TO DO?", message="What to do?"):
        widget = QWidget()
        buttonReply = QMessageBox.question(widget, title, message, QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        return buttonReply
    
    # #
    #
    def logout(self):
        ranchbe = self.ranchbe
        restService = ranchbe.restService()
        if restService.restRequest != None:
            connQurl = ranchbe.getQurl("/auth/logout")
            restService.disconnect(connQurl)
        self.serverPanel.webView.disconnect()
        label = '<span>Disconnected</span>'
        self.ui.contextInfos.setText(label)
        self.context.setUser(None)

    # #
    #
    def userpingAction(self):
        restService = self.ranchbe.restService()
        if restService.restRequest == None:
            return
        params = {"layout":"popup"}
        qurl = self.ranchbe.getQurl("/service/application/auth/getme", params)
        httpRespons = self.ranchbe.restService().get(qurl)
        webEngineView = self.serverPanel.webView.getWebEngineView()
        webEngineView.setHtml(httpRespons.content.decode("utf-8"), qurl)
        self.setCurrentTab("webview")

    # #
    #
    def workitemAction(self):
        self.context.setSpacename("workitem")
        self.context.save()

    # #
    #
    def bookshopAction(self):
        self.context.setSpacename("bookshop")
        self.context.save()

    # #
    #
    def cadlibAction(self):
        self.context.setSpacename("cadlib")
        self.context.save()

    # #
    #
    def mockupAction(self):
        self.context.setSpacename("mockup")
        self.context.save()

    # #
    #
    def testConnexion(self):
        if self.ranchbe.restService().connected == False:
            raise ConnException("Ooops! You must init connexion with Ranchbe server")

    # #
    #
    def setCurrentTab(self, name):
        if name == "webview":
            self.ui.tabWidget.setCurrentIndex(1)
            self.ui.serverTabWidget.setCurrentIndex(1)
        if name == "servertree":
            self.ui.tabWidget.setCurrentIndex(1)
            self.ui.serverTabWidget.setCurrentIndex(0)
        if name == "local":
            self.ui.tabWidget.setCurrentIndex(0)
        if name == "app":
            self.ui.tabWidget.setCurrentIndex(2)


# # Local tab
#        
class RbLocal():
    mainWin = None
    catiaConnector = None
    tableView = None
    path = ""
    
    COLUMN_NAME = 0
    COLUMN_TYPE = 1
    COLUMN_SIZE = 2
    COLUMN_MTIME = 3
    COLUMN_DESCRIPTION = 4
    COLUMN_RANCHBE = 5
    COLUMN_RB_ACCESS = 6
    COLUMN_RB_LIFESTAGE = 7
    
    def __init__(self, mainWin):
        self.mainWin = mainWin
        
        self.path = self.mainWin.config.get("workspace", "path")
        self.initWorkspace(self.path)
        
        self.tableView = self.mainWin.ui.tableViewLocal
        self.tableView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tableView.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableView.setColumnWidth(0, 300)
        # self.tableView.setColumnCount(7)
        
        _translate = QCoreApplication.translate
        item = self.tableView.horizontalHeaderItem(self.COLUMN_NAME)
        item.setText(_translate("MainWindow", "File"))
        
        item = self.tableView.horizontalHeaderItem(self.COLUMN_TYPE)
        item.setText(_translate("MainWindow", "Type"))
        
        item = self.tableView.horizontalHeaderItem(self.COLUMN_SIZE)
        item.setText(_translate("MainWindow", "Size"))
        
        item = self.tableView.horizontalHeaderItem(self.COLUMN_MTIME)
        item.setText(_translate("MainWindow", "Mtime"))
        
        item = self.tableView.horizontalHeaderItem(self.COLUMN_DESCRIPTION)
        item.setText(_translate("MainWindow", "Description"))
        
        item = self.tableView.horizontalHeaderItem(self.COLUMN_RB_ACCESS)
        item.setText(_translate("MainWindow", "Rb Access"))
        
        item = self.tableView.horizontalHeaderItem(self.COLUMN_RB_LIFESTAGE)
        item.setText(_translate("MainWindow", "Life Stage"))
        
        self.autoloadRbData = self.mainWin.config.getboolean("rbserver", "autoloadRbData")

    # #
    #
    def initWorkspace(self, path):
        if not os.path.isdir(path):
            self.mainWin.getConfigWin()
            raise Exception(path + " is not existing")
        '''
        for subdir in ["workitem", "cadlib", "bookshop", "mockup"]:
            subdir = os.path.join(path, subdir)
            try:
                os.stat(subdir)
            except:
                logging.debug("create " + subdir)
                os.mkdir(subdir)
        '''
    
    # #
    #
    def openMenu(self, position):
        menu = QMenu()
        selected = self.tableView.selectionModel().selectedRows()
        firstrow = selected[0].row()
        firstNameItem = self.tableView.item(firstrow, self.COLUMN_NAME)
        
        try:
            self.mainWin.testConnexion()
            connected = True
        except:
            connected = False
        
        ''' https://doc.qt.io/qtforpython/PySide2/QtWidgets/QAction.html#PySide2.QtWidgets.PySide2.QtWidgets.QAction.setToolTip '''
        try:
            ''' @var PyQt5.QtWidgets.QAction openAction '''
            openAction = QAction("Open")
            openAction.triggered.connect(self.openAction)
            openAction.setToolTip("Open the file")
            menu.addAction(openAction)
        except Exception as e:
            logging.debug(str(e))
        
        menu.addSeparator()
        
        if connected == True:
            rbdatasAction = QAction("Get Ranchbe Datas")
            rbdatasAction.triggered.connect(self.loadDatasFromRanchbeAction)
            rbdatasAction.setToolTip("Get ranchbe datas for this file")
            menu.addAction(rbdatasAction)
            
        menu.addSeparator()
        
        if hasattr(firstNameItem, "rbdata"):
            docfile = firstNameItem.rbdata
            user = self.mainWin.context.user
            
            detailAction = QAction("Get Document Details")
            detailAction.triggered.connect(self.getObjectDetailAction)
            detailAction.setToolTip("Get the document detail ranchbe page")
            menu.addAction(detailAction)
            
            if int(docfile.accessCode) == 1 and docfile.lockById == user.id:
                checkinAction = QAction("Checkin")
                checkinAction.triggered.connect(self.checkinAction)
                checkinAction.setToolTip("Chekin this file in ranchbe server")
                menu.addAction(checkinAction)
                
                updateAction = QAction("Checkin And Keep")
                updateAction.triggered.connect(self.checkinAndKeepAction)
                updateAction.setToolTip("Chekin this file in ranchbe server, but keep it as checkouted")
                menu.addAction(updateAction)
                
                cancelcoAction = QAction("Cancel Checkout")
                cancelcoAction.triggered.connect(self.cancelCheckoutAction)
                cancelcoAction.setToolTip("Cancel checkout; file is let in wildspace")
                menu.addAction(cancelcoAction)
            elif int(docfile.accessCode) == 0:
                checkoutAction = QAction("Checkout")
                checkoutAction.triggered.connect(self.checkoutAction)
                checkoutAction.setToolTip("Checkout this file, and copy data in wildspace")
                menu.addAction(checkoutAction)
        else:
            storeAction = QAction("Store")
            storeAction.triggered.connect(self.storeAction)
            storeAction.setToolTip("Store this file in ranchbe server")
            menu.addAction(storeAction)
            
        menu.addSeparator()
        
        deleteAction = QAction("Delete")
        deleteAction.triggered.connect(self.deleteAction)
        deleteAction.setToolTip("Delete this file from wildspace only")
        menu.addAction(deleteAction)
        
        menu.exec_(self.mainWin.ui.tableViewLocal.mapToGlobal(position))
    
    def connect(self):
        self.mainWin.ui.pushButtonRefreshLocal.clicked.connect(self.mainWin.localPanel.refreshAction)
        self.mainWin.ui.tableViewLocal.customContextMenuRequested.connect(self.openMenu)
        ''' context eitor '''
        self.mainWin.ui.tableViewLocal.itemDoubleClicked.connect(self.itemEditedAction)
        self.mainWin.ui.tableViewLocal.itemChanged.connect(self.itemRenameAction)

    def itemRenameAction(self, item):
        if hasattr(item, "edited") and item.edited == True:
            try:
                newFilename = item.text()
                newFilepath = os.path.join(self.path, newFilename)
                oldFilename = item.previousValue
                oldFilepath = os.path.join(self.path, oldFilename)
                
                if os.path.isfile(newFilepath):
                    raise Exception(newFilepath + " is existing")
                if not os.path.isfile(oldFilepath):
                    raise Exception(oldFilepath + " is not existing")
                
                os.rename(oldFilepath, newFilepath)
            except Exception as e:
                self.mainWin.errorBox("Rename Error", str(e))

            item.edited = False

    def itemEditedAction(self, item):
        item.edited = True
        item.previousValue = item.text()
        logging.debug("edited")
    
    def refreshAction(self, pattern=None):
        self.path = self.mainWin.config.get("workspace", "path")
        logging.debug("Wildspace: " + self.path)
        
        self.mainWin.ui.tableViewLocal.clearContents()
        self.mainWin.ui.tableViewLocal.hide()
        
        try:
            self.mainWin.testConnexion()
            connected = True
        except ConnException as e:
            connected = False
            logging.debug("Not connected")
        
        try:
            pattern = self.mainWin.ui.lineEditSearchLocal.displayText()
            if pattern == None or pattern == "" or pattern == True or pattern == False:
                fileList = os.listdir(self.path)
                fileList.sort()
            else:
                globPathName = os.path.join(self.path, pattern)
                fileList = glob.glob(globPathName)
                fileList.sort()
        except Exception as e:
            fileList = []
            logging.debug(str(e))

        # # Needed
        self.mainWin.ui.tableViewLocal.setRowCount(len(fileList))
        
        i = 0
        for filename in fileList:
            filename = os.path.basename(filename)
            filepath = os.path.join(self.path, filename)
            
            if os.path.isdir(filepath):
                continue
            
            try:
                mtime = datetime.fromtimestamp(os.path.getmtime(filepath))
            except Exception as e:
                logging.debug(e)
                continue
            
            """ FILENAME COLUMN """
            item = QTableWidgetItem(filename)
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsEditable)
            self.mainWin.ui.tableViewLocal.setItem(i, RbLocal.COLUMN_NAME, item)
            
            """ ACCESS COLUMN """
            rbAccessItem = QTableWidgetItem("")
            rbAccessItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.mainWin.ui.tableViewLocal.setItem(i, RbLocal.COLUMN_RB_ACCESS, rbAccessItem)
            
            """ RANCHBE COLUMN """
            rbItem = QTableWidgetItem("")
            rbItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.mainWin.ui.tableViewLocal.setItem(i, RbLocal.COLUMN_RANCHBE, rbItem)
            
            """ LIFESTAGE COLUMN """
            lifeStageItem = QTableWidgetItem("")
            lifeStageItem.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.mainWin.ui.tableViewLocal.setItem(i, RbLocal.COLUMN_RB_LIFESTAGE, lifeStageItem)
            
            try:
                if self.autoloadRbData and connected:
                    self.loadDatasFromRanchbe(filename, i)
            except Exception as e :
                logging.debug(str(e))
            
            splitExtension = os.path.splitext(filename)
            iconFile = splitExtension[1].replace(".", "") + ".gif"
            iconFile = os.path.join(RbMain.IMG_PATH, "filetype16", iconFile)
            if os.path.isfile(iconFile):
                icon = QIcon()
                icon.addPixmap(QPixmap(iconFile), QIcon.Normal, QIcon.Off)
                item.setIcon(icon)

            item = QTableWidgetItem(splitExtension[1])
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.mainWin.ui.tableViewLocal.setItem(i, RbLocal.COLUMN_TYPE, item)
            
            item = QTableWidgetItem(File.formatFileSize(os.path.getsize(filepath)))
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.mainWin.ui.tableViewLocal.setItem(i, RbLocal.COLUMN_SIZE, item)
            
            item = QTableWidgetItem(mtime.strftime("%Y-%m-%d"))
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.mainWin.ui.tableViewLocal.setItem(i, RbLocal.COLUMN_MTIME, item)
            
            i = i + 1
            
        self.mainWin.ui.tableViewLocal.setRowCount(i)
        header = self.mainWin.ui.tableViewLocal.horizontalHeader()       
        header.setSectionResizeMode(self.COLUMN_NAME, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(self.COLUMN_MTIME, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(self.COLUMN_SIZE, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(self.COLUMN_TYPE, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(self.COLUMN_RANCHBE, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(self.COLUMN_DESCRIPTION, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(self.COLUMN_DESCRIPTION, QHeaderView.Interactive)
        header.setSectionResizeMode(self.COLUMN_RB_LIFESTAGE, QHeaderView.ResizeToContents)
        self.mainWin.ui.tableViewLocal.show()

    """
    get datas from ranchbe and attach to current item
    """

    def loadDatasFromRanchbeAction(self):
        ''' @var selected: QModelIndex '''
        table = self.tableView
        selected = self.tableView.selectionModel().selectedRows()
        
        for index in selected:
            try:
                row = index.row()
                item = table.item(row, self.COLUMN_NAME)
                filename = item.text()
                self.loadDatasFromRanchbe(filename, row)
            except Exception as e:
                self.mainWin.warningBox("Error", str(e))
                return
        
    """
    get datas from ranchbe and attach to current item
    """

    def loadDatasFromRanchbe(self, filename, row):
        
        filepath = os.path.join(self.path, filename)
        spacename = self.mainWin.context.spacename
        docfileService = self.mainWin.ranchbe.getDocfileService()
        
        """ ACCESS COLUMN """
        rbAccessItem = self.tableView.item(row, RbLocal.COLUMN_RB_ACCESS)
        
        """ RANCHBE COLUMN """
        rbItem = self.tableView.item(row, RbLocal.COLUMN_RANCHBE)
        
        """ LIFESTAGE COLUMN """
        # None access because document is not loaded
        lifeStageItem = self.tableView.item(row, RbLocal.COLUMN_RB_LIFESTAGE)
        
        try:
            docfile = docfileService.getfromname(filename, spacename)
            ''' set datas to item NAME '''
            self.tableView.item(row, self.COLUMN_NAME).rbdata = docfile
        except Exception as e:
            accessLabel = "Not found in Ranchbe"
            tooltip = "This file may be stored"
            rbItem.setText(accessLabel)
            rbItem.setToolTip(tooltip)
            rbAccessItem.setText(accessLabel)
            rbAccessItem.setToolTip(tooltip)
            lifeStageItem.setText("")
            lifeStageItem.setToolTip(tooltip)
            logging.debug(str(e))
            return

        try:
            # docfile is free
            accessLabel = AccessCode.getName(docfile.accessCode)
            if int(docfile.accessCode) == 0:
                accessLabel = "Free"
                tooltip = "This file is existing in Ranchbe, you must checkout it to edit"
            elif int(docfile.accessCode) == 1:
                accessLabel = "Checkout By " + str(docfile.lockByUid) + " Since " + str(docfile.locked)
                tooltip = accessLabel
            elif int(docfile.accessCode) > 1:
                accessLabel = accessLabel + " By " + str(docfile.lockByUid) + " Since " + str(docfile.locked)
                tooltip = "This file is existing in Ranchbe, and it locked"
            else:
                accessLabel = "This file is existing in Ranchbe, you must checkout it to edit"
                tooltip = accessLabel
            
            rbAccessItem.setText(accessLabel)
            rbAccessItem.setToolTip(tooltip)
        except Exception as e:
            rbAccessItem.setText("Error occured")
            rbAccessItem.setToolTip(str(e))
            logging.debug(str(e))
            
        try:
            serverMd5 = docfile.data['md5']
            localMd5 = Tool.File.md5sum(filepath)
            if localMd5 == serverMd5:
                tooltip = "Local file is strictly same than server file"
                suffix = "not modified"
            else:
                tooltip = "Local file is not the same than the file in the ranche server"
                suffix = "modified"
            rbItem.setText(suffix)
            rbItem.setToolTip(tooltip)
        except Exception as e:
            rbItem.setText("Error occured")
            rbItem.setToolTip(str(e))
            logging.debug(str(e))
        
    # #
    #
    def storeAction(self):
        ''' @var selected: QModelIndex '''
        table = self.tableView
        selected = self.tableView.selectionModel().selectedRows()
        ranchbe = self.mainWin.ranchbe
        
        try:
            self.mainWin.testConnexion()
        except ConnException as e:
            self.mainWin.warningBox(e.title, str(e))
            return
        
        self.refreshAction()
        wsService = ranchbe.getWildspaceService()
        toStore = []
        containerId = self.mainWin.context.containerId
        spacename = self.mainWin.context.spacename

        for index in selected:
            try:
                row = index.row()
                filename = table.item(row, RbLocal.COLUMN_NAME).text()
                # isInranchbe = self.mainWin.ui.tableViewLocal.item(row, 3).text()
                filepath = os.path.join(self.path, filename)
                wsService.upload(filepath, 1)
                docfile = Docfile({
                    "name": os.path.basename(filename),
                    "description": "",
                    "containerId": containerId,
                    "spacename": spacename
                    })
                toStore.append(docfile)
                logging.debug("File is upload to wildspace: " + filename)
            except Exception as e:
                self.mainWin.warningBox("store error", "Upload of file " + filename + " failed with reason : " + str(e) + " it will be ignored and NOT STORED")
                continue
        
        if len(toStore) > 0:
            try:
                # Call ranchbe store on toStore list
                serverPanel = self.mainWin.serverPanel
                webEngineView = serverPanel.webView.getWebEngineView()
                checkinService = ranchbe.getCheckinService()
                checkinService.storefiles(toStore, self.mainWin.context, webEngineView)
                self.mainWin.ui.tabWidget.setCurrentIndex(1)
                self.mainWin.ui.serverTabWidget.setCurrentIndex(1)
            except Exception as e:
                self.mainWin.warningBox("Store error", str(e))
        
        # Refresh displayed info
        for index in selected:
            try:
                row = index.row()
                item = table.item(row, RbLocal.COLUMN_NAME)
                filename = item.text()
                self.loadDatasFromRanchbe(filename, row)
            except Exception as e:
                continue
        
    # #
    #
    def checkinAction(self, checkinandkeep=False):
        table = self.tableView
        selected = self.tableView.selectionModel().selectedRows()
        
        ranchbe = self.mainWin.ranchbe
        spacename = self.mainWin.context.spacename
        
        try:
            self.mainWin.testConnexion()
        except ConnException as e:
            self.mainWin.warningBox(e.title, str(e))
            return
        
        wsService = ranchbe.getWildspaceService()
        checkinService = ranchbe.getCheckinService()
        toCheckin = []
        unicity = []
        
        for index in selected:
            try:
                # @var integer row
                row = index.row()
                # @var QTableWidgetItem item
                item = table.item(row, 0)
                if not hasattr(item, "rbdata"):
                    continue
                if item.rbdata == None:
                    continue
                else:
                    unicity.append(item.rbdata.parentId)
            except Exception as e:
                self.mainWin.warningBox("checkin Error", str(e))
                return
        
        # dedoublonne
        unicity = list(set(unicity))
        
        # get files of selected documents
        # this make pagination forbidden
        files = []
        documents = []
        for documentId in unicity:
            documents.append(Document({"id":documentId, "spacename": spacename}))
            ''' search in table if a file associate to document is existing, if yes, upload it '''
            rowCount = table.rowCount()
            for row in range(rowCount):
                item = table.item(row, 0)
                t = hasattr(item, "rbdata")
                if t:
                    if item.rbdata.parentId == documentId:
                        files.append(item.text())
                        
        for filename in files:
            try:
                filepath = os.path.join(self.path, filename)
                wsService.upload(filepath, 1)
                toCheckin.append(os.path.basename(filename))
                logging.info("File is upload to wildspace: " + filename)
            except Exception as e:
                self.mainWin.warningBox("checkin error", "Upload of file " + filename + " failed with reason : " + str(e) + " it will be ignored and NOT STORED")
                continue
        
        if len(documents) > 0:
            try:
                # Call ranchbe store on toCheckin list
                serverPanel = self.mainWin.serverPanel
                webEngineView = serverPanel.webView.getWebEngineView()
                checkinService.checkinDocuments(documents, checkinandkeep, webEngineView)
                self.mainWin.setCurrentTab("webview")
            except Exception as e:
                self.mainWin.warningBox("checkin error", str(e))
    
    def checkinAndKeepAction(self):
        checkinandkeep = True
        return self.checkinAction(checkinandkeep)
    
    def cancelCheckoutAction(self):
        ''' @var selected: QModelIndex '''
        table = self.tableView
        selected = self.tableView.selectionModel().selectedRows()
        
        ranchbe = self.mainWin.ranchbe
        spacename = self.mainWin.context.spacename
        
        try:
            self.mainWin.testConnexion()
        except ConnException as e:
            self.mainWin.warningBox(e.title, str(e))
            return
        
        documents = []
        checkoutService = ranchbe.getCheckoutService()
        
        for index in selected:
            try:
                row = index.row()
                item = table.item(row, 0)
                if hasattr(item, "rbdata") and item.rbdata.locked:
                    documents.append(item.rbdata.parentId)
                else:
                    continue
            except Exception as e:
                self.mainWin.warningBox("Error", str(e))
                return
        
        # dedoublonne
        documents = list(set(documents))
        
        if len(documents) > 0:
            for documentId in documents:
                try:
                    checkoutService.cancelcheckoutDocument(documentId, spacename)
                except Exception as e:
                    self.mainWin.warningBox("checkin error", str(e))

        # Refresh displayed info
        for index in selected:
            try:
                row = index.row()
                item = table.item(row, RbLocal.COLUMN_NAME)
                filename = item.text()
                self.loadDatasFromRanchbe(filename, row)
            except Exception as e:
                continue

    def checkoutAction(self):
        ''' @var selected: QModelIndex '''
        table = self.tableView
        selected = self.tableView.selectionModel().selectedRows()
        
        ranchbe = self.mainWin.ranchbe
        spacename = self.mainWin.context.spacename
        
        try:
            self.mainWin.testConnexion()
        except ConnException as e:
            self.mainWin.warningBox(e.title, str(e))
            return
        
        workspace = self.mainWin.config.get("workspace", "path")
        docfileService = ranchbe.getDocfileService()
        checkoutService = ranchbe.getCheckoutService()
        documents = []
        toDownload = []
        
        for index in selected:
            try:
                row = index.row()
                item = table.item(row, RbLocal.COLUMN_NAME)
                if hasattr(item, "rbdata") and int(item.rbdata.accessCode) == 0:
                    documents.append(item.rbdata.parentId)
                else:
                    continue
            except Exception as e:
                self.mainWin.warningBox("Error", str(e))
                return
        
        # dedoublonne
        documents = list(set(documents))
        
        if len(documents) > 0:
            for documentId in documents:
                try:
                    docfiles = checkoutService.checkoutDocument(documentId, spacename)
                    for df in docfiles:
                        toDownload.append(df)
                except Exception as e:
                    self.mainWin.warningBox("checkout error", str(e))
                    
        # # @var treeItem QTreeWidgetItem
        for docfile in toDownload:
            filename = docfile.name
            filepath = os.path.join(workspace, filename)
            replaceFile = True
            if os.path.isfile(filepath):
                buttonReply = self.mainWin.questionBox("File is existing in Workspace", "The file " + filename + " is existing in workspace. \n Do you want to replace it? \n Click Yes to replace, No to Keep it")
                if buttonReply == QMessageBox.Yes:
                    replaceFile = True
                else:
                    replaceFile = False
                    
            if replaceFile:
                try:
                    filepath = docfileService.download(docfile.id, spacename, workspace)
                except Exception as e:
                    self.mainWin.warningBox("Download failed", "File " + filename + " cant be download from server. \n BE CAREFUL, the document is checkouted but file in your wildspace is not updated|created. \n You must cancel the checkout and retry")
                    logging.debug(str(e))
                    continue
        
        # Refresh displayed info
        for index in selected:
            try:
                row = index.row()
                item = table.item(row, RbLocal.COLUMN_NAME)
                filename = item.text()
                self.loadDatasFromRanchbe(filename, row)
            except Exception as e:
                continue

    # #
    # @param item QTreeWidgetItem
    #
    def getObjectDetailAction(self):
        table = self.tableView
        selected = self.tableView.selectionModel().selectedRows()

        ranchbe = self.mainWin.ranchbe
        spacename = self.mainWin.context.spacename
        
        try:
            self.mainWin.testConnexion()
        except ConnException as e:
            self.mainWin.warningBox(e.title, str(e))
            return

        try:
            documentService = ranchbe.getService("document")
        except ServiceNotFoundException as e:
            self.mainWin.warningBox("No service for this", str(e))
        
        try:
            row = selected[0].row()
            item = table.item(row, 0)
            if hasattr(item, "rbdata"):
                docfile = item.rbdata
                documentId = docfile.parentId
                spacename = self.mainWin.context.spacename
            else:
                raise Exception("None data on this item")
            
            webEngineView = self.mainWin.ui.webEngineView
            documentService.getDetails(documentId, spacename, webEngineView)
            
        except Exception as e:
            self.mainWin.warningBox("Error", str(e))
            return
        
        self.mainWin.setCurrentTab("webview")

    # # 
    #    Open the selected files
    #
    def openAction(self):
        # self.mainWin.getWaiterWin().show()
        table = self.tableView
        selected = self.tableView.selectionModel().selectedRows()
        
        for index in selected:
            try:
                row = index.row()
                item = table.item(row, 0)
                file = item.text()
                filepath = os.path.join(self.path, file)
                os.startfile(filepath, 'open')
            except NotImplementedError:
                return

    # #
    #   Delete files in wildspace
    #
    def deleteAction(self):
        ''' @var selected: QModelIndex '''
        table = self.tableView
        selected = self.tableView.selectionModel().selectedRows()
        files = []
        
        # build list of file to delete from selection
        for index in selected:
            try:
                row = index.row()
                item = table.item(row, 0)
                files.append(item.text())
            except Exception as e:
                self.mainWin.warningBox("Error", str(e))
                return

        # confirm
        msg = "Are you sure that you want delete this files :" + ",".join(files)
        buttonReply = self.mainWin.questionBox("Delete confirmation", msg)
        
        # delete
        if buttonReply == QMessageBox.Yes:
            for file in files:
                try:
                    filepath = os.path.join(self.path, file)
                    os.remove(filepath)
                except Exception as e:
                    self.mainWin.warningBox("Deletion error", "Unable to delete file " + file + "\n please, consult log for more details")
                    logging.debug(str(e))
    
        # #refresh local list
        self.refreshAction()
    
    # #
    #
    def searchAction(self):
        pattern = self.mainWin.ui.lineEditSearchLocal.displayText()
        self.refreshAction(pattern)


# # Server Tab
# 
class RbServer():
    
    # # @var RbMain
    mainWin = None

    # # @var WebView
    webView = None

    # # @var
    tab = None

    # # @var RbServerTree
    tree = None

    # #
    #
    def __init__(self, mainWin):
        self.mainWin = mainWin
        ranchbe = mainWin.ranchbe
        webEngineView = self.mainWin.ui.webEngineView
        # webEngineView.page().profile().setRequestInterceptor(HttpInterceptor())
        self.webView = WebView(webEngineView, ranchbe)
        self.tab = self.mainWin.ui.serverTab
        self.tree = RbServerTree(mainWin)
        self.tree.connect()

    # #
    #
    def connect(self):
        self.mainWin.ui.pushButtonRefreshServer.clicked.connect(self.mainWin.serverPanel.refreshAction)

    # #
    #
    def refreshAction(self):
        logging.info("refresh webview")
        webEngineView = self.mainWin.ui.webEngineView
        webEngineView.reload()


# #
#
#
#
class RbServerTree():
    
    # # @var RbMain
    mainWin = None
    
    # # @var QTreeWidget
    treeWidget = None
    
    # #
    #
    def __init__(self, mainWin):
        self.mainWin = mainWin
        self.treeWidget = self.mainWin.ui.serverTreeWidget
        
        # Drag and Drop
        self.treeWidget.setDragEnabled(True)
        self.treeWidget.setDropIndicatorShown(True)
        self.treeWidget.setDragDropMode(QTreeWidget.DragDrop)
        self.treeWidget.setDefaultDropAction(Qt.MoveAction)

        self.treeWidget.dragEnterEvent = self.dragEnterEvent
        self.treeWidget.dragMoveEvent = self.dragMoveEvent
        self.treeWidget.dropEvent = self.dropEvent
        self.treeWidget.mimeData = self.mimeData

        # Features
        self.treeWidget.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.treeWidget.setSortingEnabled(True)

        # Headers
        header = self.treeWidget.header()
        header.setDefaultSectionSize(300)
        
        self.treeWidget.headerItem().setText(RbServerTreeItem.NAME_COL, "Name")
        self.treeWidget.headerItem().setText(RbServerTreeItem.DESCRIPTION_COL, "Description")
        self.treeWidget.headerItem().setText(RbServerTreeItem.RB_ACCESS_COL, "Access")
        self.treeWidget.headerItem().setText(RbServerTreeItem.RB_LIFESTAGE__COL, "LifeStage")
        self.treeWidget.headerItem().setText(RbServerTreeItem.RB_DOCTYPE_COL, "Doctype")
        self.treeWidget.headerItem().setText(RbServerTreeItem.VERSION_COL, "Version")

        # Root item
        ranchbeIcon = QIcon()
        ranchbeIcon.addPixmap(QPixmap(":/favicon"), QIcon.Normal, QIcon.Off)
        
        rootItemRanchbe = RbServerTreeItem(['Ranchbe'])
        rootItemRanchbe.setIcon(0, ranchbeIcon)
        rootItemRanchbe.setDisabled(False)
        rootItemRanchbe.rbType = "ranchbe"
        rootItemRanchbe.loaded = True
        rootItemRanchbe.setData(0, Qt.UserRole + 1, "ranchbe type");
        rootItemRanchbe.setFlags(Qt.ItemIsEnabled)  # #Not selectable item
        self.treeWidget.addTopLevelItem(rootItemRanchbe)

        folderIcon = QIcon()
        folderIcon.addPixmap(QPixmap(":/folder"), QIcon.Normal, QIcon.Off)

        rootItemProjects = RbServerTreeItem(['Projects'])
        rootItemProjects.setIcon(0, folderIcon)
        rootItemProjects.setFlags(Qt.ItemIsEnabled)  # #Not selectable item
        rootItemProjects.rbType = "projects"
        rootItemProjects.loaded = False
        rootItemProjects.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
        rootItemRanchbe.addChild(rootItemProjects)

    # #
    #
    def mimeData(self, items=None):
        mimeData = QMimeData()
        mimeData.selectedItems = []
        mimeData.setText(items[0].rbType)
        for item in items:
            if(item.rbType == mimeData.text()):
                mimeData.selectedItems.append(item)
            else:
                mimeData.setText("Mixed")
        return mimeData

    # #
    #
    def dragEnterEvent(self, e):
        """
        On start drag
        :param e : PyQt5.QtGui.QDragEnterEvent
        :return:
        """
        if(e.mimeData().text() == 'document'):
            e.accept()
            logging.debug(e.mimeData().text(), 'dragEnterEvent')  # # PyQt5.QtCore.QMimeData
        else:
            e.ignore()
        return

    # #
    #
    def dragMoveEvent(self, e):
        """
        On move drag
        :param e : PyQt5.QtGui.QDragMoveEvent
        :return:
        """
        logging.debug('dragMoveEvent', e.mimeData().text())
        e.accept()
    
    # #
    #
    def dropEvent(self, e):
        """
        Drop files directly onto the widget
        File locations are stored in fname
        :param e: PyQt5.QtGui.QDropEvent
        :return:
        """
        
        targetedItem = self.treeWidget.itemAt(e.pos())
        if e.mimeData().hasText():
            if(targetedItem.rbType == 'category'):
                
                ranchbe = Ranchbe.get()
                service = ranchbe.getDocumentService()
                
                logging.debug('dropEvent', e.mimeData().text())
                for item in e.mimeData().selectedItems:
                    documentId = item.rbdata.id
                    spacename = item.rbdata.spacename
                    categoryId = targetedItem.rbdata.id
                    respons = service.setProperty(documentId, spacename, 'categoryId', categoryId)
                    datas = respons.getData()
                    logging.debug(datas)
                
                return QTreeWidget.dropEvent(self.treeWidget, e)

        e.ignore()
    
    # #
    #
    def dropMimeData(self, data, action, row, column, parent):
        logging.debug("dropMimeData")
        
    # #
    #
    def connect(self):
        self.treeWidget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.treeWidget.customContextMenuRequested.connect(self.openMenu)
        self.treeWidget.itemExpanded.connect(self.handleExpand)    
        self.treeWidget.itemCollapsed.connect(self.handleCollapse)
        
    # #
    #
    def getFilteredSelection(self):
        selected = self.treeWidget.selectedItems()
        if(len(selected) == 0):
            return
        rbtype = selected[0].rbType
        # # @var QTreeWidgetItem item
        for item in selected:
            if item.rbType != rbtype:
                selected.remove(item)
                item.setSelected(False)
        return selected
    
    # #
    #
    def openMenu(self):
        selected = self.treeWidget.selectedItems()
        if(len(selected) == 0):
            return
        rbtype = selected[0].rbType
        
        menu = QMenu(self.mainWin)
        if(rbtype == "document"):

            openAction = QAction("Open")
            openAction.triggered.connect(self.openAction)
            openAction.setToolTip("Open the file")
            menu.addAction(openAction)
            
            detailAction = QAction("Details")
            detailAction.triggered.connect(self.getObjectDetailAction)
            detailAction.setToolTip("Get detail page about this document")
            menu.addAction(detailAction)
            
            menu.addSeparator()
            
            if hasattr(selected[0], "rbdata") and selected[0].rbdata != None:
                document = selected[0].rbdata
                user = self.mainWin.context.user
                
                if int(document.accessCode) == 1 and document.lockById == user.id:
                    checkinAction = QAction("Checkin")
                    checkinAction.triggered.connect(self.checkinAction)
                    checkinAction.setToolTip("Chekin this file in ranchbe server")
                    menu.addAction(checkinAction)
                    
                    updateAction = QAction("Checkin And Keep")
                    updateAction.triggered.connect(self.checkinAndKeepAction)
                    updateAction.setToolTip("Chekin document files in ranchbe server, but keep it as checkouted")
                    menu.addAction(updateAction)
                    
                    cancelcoAction = QAction("Cancel Checkout")
                    cancelcoAction.triggered.connect(self.cancelCheckoutAction)
                    cancelcoAction.setToolTip("Cancel checkout; files are let in wildspace")
                    menu.addAction(cancelcoAction)
                    
                elif int(document.accessCode) == 0:
                    checkoutAction = QAction("Checkout")
                    checkoutAction.triggered.connect(self.checkoutAction)
                    checkoutAction.setToolTip("Checkout this document and copy files into wildspace")
                    menu.addAction(checkoutAction)
            
            workflowAction = QAction("Run a Workflow")
            workflowAction.triggered.connect(self.workflowAction)
            workflowAction.setToolTip("Run a workflow")
            menu.addAction(workflowAction)

            editAction = QAction("Edit")
            editAction.triggered.connect(self.editAction)
            editAction.setToolTip("Edit document")
            menu.addAction(editAction)

            menu.addSeparator()
            
        elif(rbtype == "project" or rbtype == "container"):
            if(rbtype == "container"):
                setAsContextAction = QAction("Set As Work In")
                setAsContextAction.triggered.connect(self.setAsContextAction)
                setAsContextAction.setToolTip("Set this container as actif, put it in context")
                menu.addAction(setAsContextAction)
            
            refreshAction = QAction("Refresh")
            refreshAction.triggered.connect(self.refreshAction)
            refreshAction.setToolTip("Refresh this view")
            menu.addAction(refreshAction)
            
            detailAction = QAction("Details")
            detailAction.triggered.connect(self.getObjectDetailAction)
            detailAction.setToolTip("Get detail page about this object")
            menu.addAction(detailAction)
            
        else:
            refreshAction = QAction("Refresh")
            refreshAction.triggered.connect(self.refreshAction)
            refreshAction.setToolTip("Refresh this view")
            menu.addAction(refreshAction)

        menu.exec_(QCursor.pos())

    # #
    #
    def refreshAction(self):
        selected = self.getFilteredSelection()
        self.handleExpand(selected, True)

    # #
    #
    # @see https://www.qtcentre.org/threads/39342-how-to-use-custom-type-items-in-QTreeWidget
    # @param QTreeWidgetItem|list item
    # @param boolean refresh
    # @return void
    #
    def handleExpand(self, item, refresh=False):
        try:
            self.mainWin.testConnexion()
        except ConnException as e:
            refresh = False

        if isinstance(item, list):
            items = item
        else:
            items = [item]

        for item in items:
            try:
                # # Set loaded attr to indicate that item children are yet loaded
                if not hasattr(item, "loaded"):
                    item.loaded = False
        
                # # If item is loaded and refresh is not require, do nothing
                if item.loaded == True and refresh == False:
                    logging.debug("Item " + item.text(0) + " is expand but not refresh")
                    return
                
                # # If refresh require, delete all actual children
                if refresh == True:
                    logging.debug("refresh tree item" + item.text(0))
                    item.takeChildren()
                    # #for child in children:
                        # #sip.delete(child)
                
                # # load children
                rbtype = item.rbType
                if rbtype == "projects":
                    self.loadProjects(item)
                elif rbtype == "project":
                    self.loadSpaces(item)
                elif rbtype == "space":
                    self.loadContainers(item)
                elif rbtype == "container":
                    self.loadCategories(item)
                    self.loadDocuments(item)
                    # self.loadTags(item)
                elif rbtype == "category":
                    self.loadCategories(item)
                    self.loadDocuments(item)
                    # self.loadTags(item)
                elif rbtype == "tag":
                    self.loadDocuments(item)
            except Exception as e:
                logging.debug(e)
            
    # #
    #
    def handleCollapse(self, item):
        logging.debug('handleCollapse' + str(item))
    
    # #
    #
    def loadProjects(self, parentItem):
        
        try:
            ranchbe = Ranchbe.get()
            service = ranchbe.getProjectService()
            respons = service.search()
            datas = respons.getData()
            logging.debug(datas)
            
            folderIcon = QIcon()
            folderIcon.addPixmap(QPixmap(":/folder"), QIcon.Normal, QIcon.Off)
            
            for properties in datas:
                project = Project(properties)
                treeItem = RbServerTreeItem(RbServerTreeItem.TYPE_PROJECT)
                treeItem.setData(RbServerTreeItem.NAME_COL, 0, project.name)
                treeItem.setData(RbServerTreeItem.DESCRIPTION_COL, 0, project.description)
                treeItem.setIcon(0, folderIcon)
                treeItem.rbType = "project"
                treeItem.loaded = False
                treeItem.rbdata = project
                treeItem.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
                parentItem.addChild(treeItem)
                
            parentItem.loaded = True
            
        except ConnException as e:
            self.mainWin.warningBox("Connexion error", str(e))
        except Exception as e:
            self.mainWin.errorBox("Unknow error", str(e))
        
        return self

    # #
    #
    def loadSpaces(self, parentItem):
        
        try:
            ranchbe = Ranchbe.get()
            service = ranchbe.getSpaceService()
            respons = service.search("", {0:{0:"id", 1:5, 2:"SUP"}})
            datas = respons.getData()
            logging.debug(datas)
            
            folderIcon = QIcon()
            folderIcon.addPixmap(QPixmap(":/folder"), QIcon.Normal, QIcon.Off)
            
            for properties in datas:
                space = Space(properties)
                treeItem = RbServerTreeItem(RbServerTreeItem.TYPE_SPACE)
                treeItem.setData(treeItem.NAME_COL, 0, space.name)
                treeItem.setData(treeItem.DESCRIPTION_COL, 0, space.name)
                treeItem.setIcon(0, folderIcon)
                treeItem.rbType = "space"
                treeItem.loaded = False
                treeItem.rbdata = space
                treeItem.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
                parentItem.addChild(treeItem)
                
            parentItem.loaded = True
            
        except ConnException as e:
            self.mainWin.warningBox("Connexion error", str(e))
        except Exception as e:
            self.mainWin.errorBox("Unknow error", str(e))
        
        return self

    # #
    #
    def loadContainers(self, parentItem):
        
        try:
            spaceItem = parentItem
            spacename = spaceItem.rbdata.name
            parentProject = spaceItem.parent().rbdata
            projectId = parentProject.id
            
            ranchbe = Ranchbe.get()
            service = ranchbe.getContainerService()
            respons = service.search("", {0:{0:"parentId", 1:projectId, 2:"EQUAL"}}, {"spacename":spacename})
            datas = respons.getData()
            logging.debug(datas)
            
            for properties in datas:
                container = Container(properties)
                treeItem = RbServerTreeItem(RbServerTreeItem.TYPE_CONTAINER)
                treeItem.setData(treeItem.NAME_COL, 0, container.name)
                treeItem.setData(treeItem.DESCRIPTION_COL, 0, container.description)
                treeItem.rbType = "container"
                treeItem.loaded = False
                treeItem.rbdata = container
                treeItem.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
                parentItem.addChild(treeItem)
                
            parentItem.loaded = True
            
        except ConnException as e:
            self.mainWin.warningBox("Connexion error", str(e))
        except Exception as e:
            self.mainWin.errorBox("Unknow error", str(e))
        
        return self

    # #
    #
    def loadDocuments(self, parentItem):
        
        try:
            if parentItem.rbType == "category":
                parentCategory = parentItem.rbdata
                containerItem = parentItem
                while containerItem.rbType != "container":
                    containerItem = containerItem.parent()
            else:
                parentCategory = None
                containerItem = parentItem

            parentContainer = containerItem.rbdata
            containerId = parentContainer.id
            spacename = parentContainer.spacename

            sqlfilter = {
                0:{0:"parentId", 1:containerId, 2:"EQUAL"},
                }
            
            if parentCategory == None:
                sqlfilter[1] = {0:"categoryId", 1:"", 2:"ISNULL"}
            else:
                categoryId = parentCategory.id
                sqlfilter[1] = {0:"categoryId", 1:str(categoryId), 2:"EQUAL"}
            
            ranchbe = Ranchbe.get()
            service = ranchbe.getDocumentService()
            respons = service.search("", sqlfilter, {"spacename":spacename, "withdoctype":"1"})
            datas = respons.getData()
            logging.debug(datas)
            
            for properties in datas:
                document = Document(properties)
                treeItem = RbServerTreeDocumentItem(RbServerTreeItem.TYPE_DOCUMENT)
                treeItem.loadRanchbeData(document)
                # treeItem.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
                parentItem.addChild(treeItem)
                
            parentItem.loaded = True
            
        except ConnException as e:
            self.mainWin.warningBox("Connexion error", str(e))
        except Exception as e:
            self.mainWin.errorBox("Unknow error", str(e))
        
        return self
    
    # #
    #
    def loadCategories(self, parentItem):
        
        try:
            params = {}
            
            if parentItem.rbType == "category":
                parentCategory = parentItem.rbdata
                params["parentId"] = parentCategory.id
                containerItem = parentItem
                while containerItem.rbType == "category":
                    containerItem = containerItem.parent()
            else:
                parentCategory = None
                containerItem = parentItem
            
            parentContainer = containerItem.rbdata
            containerId = parentContainer.id
            spacename = parentContainer.spacename
            
            params["spacename"] = spacename
            params["containerId"] = containerId
            
            ranchbe = Ranchbe.get()
            service = ranchbe.getCategoryService()
            respons = service.getusedbycontainer(params)
            datas = respons.getData()
            logging.debug(datas)
            
            folderIcon = QIcon()
            folderIcon.addPixmap(QPixmap(":/folder"), QIcon.Normal, QIcon.Off)
            
            for properties in datas:
                category = Category(properties)
                treeItem = RbServerTreeItem(RbServerTreeItem.TYPE_CATEGORY)
                treeItem.setData(treeItem.NAME_COL, 0, category.nodelabel)
                treeItem.setData(treeItem.DESCRIPTION_COL, 0, category.description)
                treeItem.setIcon(0, folderIcon)
                treeItem.rbType = "category"
                treeItem.loaded = False
                treeItem.rbdata = category
                treeItem.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator)
                parentItem.addChild(treeItem)
            parentItem.loaded = True
            
        except ConnException as e:
            self.mainWin.warningBox("Connexion error", str(e))
        except Exception as e:
            self.mainWin.errorBox("Unknow error", str(e))
        
        return self

    # # 
    #    Open the selected files
    #
    def openAction(self):
        ranchbe = self.mainWin.ranchbe
        selected = self.getFilteredSelection()
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            self.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return
        
        docfileService = ranchbe.getDocfileService()
        
        # # @var treeItem QTreeWidgetItem
        for treeItem in selected:
            try:
                rbobject = treeItem.rbdata
                objId = rbobject.id
                spacename = rbobject.spacename
                docfiles = docfileService.getOfDocument(objId, spacename)
                mainDf = docfiles[0]
                toDir = tempfile.gettempdir()
                filepath = docfileService.download(mainDf.id, spacename, toDir)
                os.startfile(filepath, 'open')
            except NotImplementedError:
                self.mainWin.warningBox("This cant be used on linux/MAC OS")
                return
            except Exception as e:
                self.mainWin.warningBox(str(e))
    
    def editAction(self):
        ranchbe = self.mainWin.ranchbe
        selected = self.getFilteredSelection()
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            self.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return
        
        documentService = ranchbe.getDocumentService()
        
        # # @var treeItem QTreeWidgetItem
        try:
            treeItem = selected[0]
            if treeItem.rbType == "document":
                webEngineView = self.mainWin.ui.webEngineView
                document = treeItem.rbdata
                documentService.getEditPage(document.id, document.spacename, webEngineView)
        except NotImplementedError:
            self.mainWin.warningBox("This cant be used on linux/MAC OS")
            return
        except Exception as e:
            self.mainWin.warningBox(str(e))
        
        self.mainWin.setCurrentTab("webview")
      
    # #
    #
    def checkoutAction(self):
        ranchbe = self.mainWin.ranchbe
        selected = self.getFilteredSelection()
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            self.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return
        
        workspace = self.mainWin.config.get("workspace", "path")
        docfileService = ranchbe.getDocfileService()
        checkoutService = ranchbe.getCheckoutService()
        toDownload = []
        
        # # @var treeItem QTreeWidgetItem
        unicity = []
        for treeItem in selected:
            try:
                document = treeItem.rbdata
                documentId = document.id
                spacename = document.spacename
                if documentId not in unicity :
                    docfiles = checkoutService.checkoutDocument(documentId, spacename)
                    unicity.append(documentId)
                    for df in docfiles:
                        toDownload.append(df)
            except Exception as e:
                self.mainWin.warningBox("checkout error", str(e))
                continue
        
        # # @var treeItem QTreeWidgetItem
        for docfile in toDownload:
            filename = docfile.name
            filepath = os.path.join(workspace, filename)
            replaceFile = True
            if os.path.isfile(filepath):
                buttonReply = self.mainWin.questionBox("File is existing in Workspace", "The file " + filename + " is existing in workspace. \n Do you want to replace it? \n Click Yes to replace, No to Keep it")
                if buttonReply == QMessageBox.Yes:
                    replaceFile = True
                else:
                    replaceFile = False
                    
            if replaceFile:
                try:
                    filepath = docfileService.download(docfile.id, spacename, workspace)
                except Exception as e:
                    self.mainWin.warningBox("Download failed", "File " + filename + " cant be download from server. \n BE CAREFUL, the document is checkouted but file in your wildspace is not updated|created. \n You must cancel the checkout and retry")
                    logging.debug(str(e))
                    continue
        
        for treeItem in selected:
            if treeItem.__class__.__name__ == "RbServerTreeDocumentItem":
                logging.debug("refresh RbServerTreeDocumentItem")
                treeItem.refresh()

        # self.mainWin.setCurrentTab("local")
        self.mainWin.localPanel.refreshAction()

    # #
    #
    def cancelCheckoutAction(self):
        ranchbe = self.mainWin.ranchbe
        selected = self.getFilteredSelection()
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            self.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return
        
        checkoutService = ranchbe.getCheckoutService()
        
        # # @var treeItem QTreeWidgetItem
        for treeItem in selected:
            try:
                document = treeItem.rbdata
                documentId = document.id
                spacename = document.spacename
                checkoutService.cancelcheckoutDocument(documentId, spacename)
            except Exception as e:
                self.mainWin.warningBox("cancel checkout error", str(e))
                continue
        
        # # refresh view
        for treeItem in selected:
            if treeItem.__class__.__name__ == "RbServerTreeDocumentItem":
                logging.debug("refresh RbServerTreeDocumentItem")
                treeItem.refresh()
            
        self.mainWin.localPanel.refreshAction()
        self.mainWin.warningBox("Cancel Checkout Terminated", "Please, note that file are always in your wildspace")

    # #
    #
    def checkinAction(self, checkinandkeep=False):
        ranchbe = self.mainWin.ranchbe
        selected = self.getFilteredSelection()
        
        try:
            self.mainWin.testConnexion()
        except ConnException as e:
            self.mainWin.warningBox(e.title, str(e))
            return
        
        wsService = ranchbe.getWildspaceService()
        workspace = self.mainWin.config.get("workspace", "path")
        docfileService = ranchbe.getDocfileService()
        checkinService = ranchbe.getCheckinService()
        toRefresh = []
        toCheckin = []
        documents = []
        
        # # @var treeItem QTreeWidgetItem
        unicity = []
        for treeItem in selected:
            parentItem = treeItem.parent()
            if parentItem not in toRefresh:
                toRefresh.append(parentItem)

            try:
                document = treeItem.rbdata
                documentId = document.id
                if documentId not in unicity :
                    unicity.append(documentId)
                    documents.append(document)
            except Exception as e:
                self.mainWin.warningBox("checkout error", str(e))
                continue

        # get files of selected documents
        # this make pagination forbidden
        docfiles = []
        for document in documents:
            '''get docfiles of document '''
            docfiles = docfiles + docfileService.getOfDocument(document.id, document.spacename)
            
        for docfile in docfiles:
            try:
                filename = docfile.name
                filepath = os.path.join(workspace, filename)
                wsService.upload(filepath, 1)
                toCheckin.append(os.path.basename(filename))
                logging.debug("File is upload to wildspace: " + filename)
            except Exception as e:
                self.mainWin.warningBox("checkin error", "Upload of file " + filename + " failed with reason : " + str(e) + " it will be ignored and NOT STORED")
                continue
        
        if len(documents) > 0:
            try:
                # Call ranchbe store on toCheckin list
                serverPanel = self.mainWin.serverPanel
                webEngineView = serverPanel.webView.getWebEngineView()
                checkinService.checkinDocuments(documents, checkinandkeep, webEngineView)
                self.mainWin.setCurrentTab("webview")
            except Exception as e:
                self.mainWin.warningBox("checkin error", str(e))
    
    def checkinAndKeepAction(self):
        checkinandkeep = True
        return self.checkinAction(checkinandkeep)

    # #
    # @param item QTreeWidgetItem
    #
    def getObjectDetailAction(self):
        ranchbe = self.mainWin.ranchbe
        selected = self.getFilteredSelection()
        
        # # @var treeItem QTreeWidgetItem
        treeItem = selected[0]
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            self.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return
        
        try:
            rbType = treeItem.rbType
            service = ranchbe.getService(rbType)
        except ServiceNotFoundException as e:
            self.mainWin.warningBox("No service for this", str(e))
        
        try:
            myObject = treeItem.rbdata
            myId = myObject.id
            webEngineView = self.mainWin.ui.webEngineView
            if hasattr(myObject, "spacename"):
                spacename = myObject.spacename
                service.getDetails(myId, spacename, webEngineView)
            else:
                service.getDetails(myId, webEngineView)
        except Exception as e:
            self.mainWin.warningBox(str(e))

        self.mainWin.setCurrentTab("webview")

    # #
    #
    def workflowAction(self):
        ranchbe = self.mainWin.ranchbe
        selected = self.getFilteredSelection()
        
        # Put files in ranchbe server widldspace
        if ranchbe.restService().connected == False:
            self.mainWin.warningBox("Connexion failed", "Ooops! You must init connexion with Ranchbe server")
            return

        # Init helpers services
        workflowService = ranchbe.getWorkflowService()
        documents = []
        
        ''' Get docfile from filename '''
        # # @var treeItem QTreeWidgetItem
        unicity = []
        for treeItem in selected:
            document = treeItem.rbdata
            
            if document.id not in unicity :
                unicity.append(document.id)
                try:
                    accessCode = int(document.accessCode)
                    if accessCode == 0 or accessCode == 5:
                        documents.append(document)
                    else:
                        raise Exception("Document is not free")
                except Exception as e:
                    self.mainWin.warningBox("workflowAction error", document.name + ": " + str(e))
                    continue
        
        webEngineView = self.mainWin.serverPanel.webView.getWebEngineView()
        workflowService.startFromDocument(documents, webEngineView)
        self.mainWin.setCurrentTab("webview")

    # #
    #
    def setAsContextAction(self):
        selected = self.getFilteredSelection()
        
        treeItem = selected[0]
        if treeItem.rbType != "container":
            self.mainWin.ui.statusbar.showMessage("You must select a container")
            return
        
        container = treeItem.rbdata
        self.mainWin.context.setContainer(container)
        self.mainWin.context.save()


# #
#
class RbServerTreeItem(QTreeWidgetItem):
    
    NAME_COL = 0
    DESCRIPTION_COL = 1
    RB_ACCESS_COL = 2
    RB_LIFESTAGE__COL = 3
    RB_DOCTYPE_COL = 4
    VERSION_COL = 5
    
    TYPE_SPACE = QTreeWidgetItem.UserType + 1
    TYPE_PROJECT = QTreeWidgetItem.UserType + 5
    TYPE_CONTAINER = QTreeWidgetItem.UserType + 10
    TYPE_CATEGORY = QTreeWidgetItem.UserType + 15
    TYPE_DOCUMENT = QTreeWidgetItem.UserType + 20
    TYPE_DOCFILE = QTreeWidgetItem.UserType + 25

    # #
    #
    def __init__(self, *args, **kwargs):
        QTreeWidgetItem.__init__(self, *args, **kwargs)
        folderIcon = QIcon()
        folderIcon.addPixmap(QPixmap(":/folder"), QIcon.Normal, QIcon.Off)
        self.setIcon(0, folderIcon)
        
        """ @var boolean """
        self.loaded = False
    
        """ @var boolean """
        self.readOnly = True
    
        """ @var boolean """
        self.saved = False
        
        """ @var string """
        self.rbType = None
        
        """ @var """
        self.rbdata = None

# #
#
class RbServerTreeDocumentItem(RbServerTreeItem):
    
    # #
    #
    def __init__(self, *args, **kwargs):
        RbServerTreeItem.__init__(self, *args, **kwargs)
        self.rbType = "document"
        
    # #
    #
    def refresh(self):
        service = Ranchbe.get().getDocumentService()
        document = service.getEntity(self.rbdata.id, self.rbdata.spacename)
        self.loadRanchbeData(document)
        self.emitDataChanged()

    # #
    #
    def loadRanchbeData(self, document):
        fullLabel = document.name + " v" + document.version + "." + document.iteration  # + " (#" + document.id + ")"
        accessLabel = AccessCode.getName(document.accessCode)
        
        if int(document.accessCode) == 1:
            accessLabel = accessLabel + " By " + document.lockByUid + " Since " + document.locked
            
        if hasattr(document, "dtname"):
            dtname = document.dtname
        else:
            dtname = None
        if hasattr(document, "icon"):
            icon = document.icon
        else:
            icon = None

        try:
            if dtname == None or icon == None:
                doctype = DoctypeLocalCache.get().getFromId(document.doctypeId)
                icon = doctype[DoctypeLocalCache.COL_ICON]
                dtname = doctype[DoctypeLocalCache.COL_NAME]
        except Exception as e:
            logging.debug(str(e))
        
        if icon:
            documentIcon = QIcon()
            icon = os.path.join(RbMain.IMG_PATH, "filetype16", icon)
            documentIcon.addPixmap(QPixmap(icon), QIcon.Normal, QIcon.Off)
            self.setIcon(0, documentIcon)
        
        ''' reload data from server '''
        self.setData(self.NAME_COL, 0, fullLabel)
        self.setData(self.DESCRIPTION_COL, 0, document.description)
        self.setData(self.RB_ACCESS_COL, 0, accessLabel)
        self.setData(self.RB_LIFESTAGE__COL, 0, document.lifeStage)
        self.setData(self.RB_DOCTYPE_COL, 0, dtname)
        self.loaded = True
        self.rbdata = document
        self.rbType = "document"


# # Application Tab
#
class RbApplication():
    
    # #
    #
    def __init__(self, mainWin):
        """ @var RbMain """
        self.mainWin = mainWin
        cname = mainWin.config.get("application", "connector")
        factory = ConnectorFactory()
        """ @var Connector.Abstract.AbstractConnector """
        self.connector = factory.getConnector(cname, mainWin)

        
# #
#
#        
class ConnectDialogBox(Ui_ConnexionDialogBox):
    
    # #
    #
    def __init__(self, mainWin):
        super (Ui_ConnexionDialogBox, self).__init__()
        self.mainWin = mainWin
        self.rootWidget = QDialog()
    
    def setupUi(self):
        Ui_ConnexionDialogBox.setupUi(self, self.rootWidget)
        
        user = Ranchbe.get().config.get("rbserver", "user")
        self.userNameLineEdit.insert(user)
        
        spacename = Ranchbe.get().config.get("workspace", "spacename")
        index = self.spacenameSelect.findText(spacename, Qt.MatchFixedString)
        if (index != -1) :  # -1 for not found
            self.spacenameSelect.setCurrentIndex(index)
        
    def connect(self):
        self.validateButton.clicked.connect(self.connectToServer)

    def showme(self):
        self.rootWidget.exec_()
    
    def connectToServer(self):
        username = self.userNameLineEdit.text()
        password = self.passwordLineEdit.text()
        spacename = self.spacenameSelect.currentText()
        
        config = self.mainWin.config
        config.set("rbserver", "user", username)  # #saved in setSpacename
        self.mainWin.context.setSpacename(spacename).save()

        # # init rest service
        ranchbe = Ranchbe.get()
        restService = ranchbe.restService()
        connQurl = ranchbe.getConnectQurl(username, password)
        try:
            restService.connect(connQurl)
        except Exception as e:
            logging.debug(str(e))
            self.mainWin.warningBox("Connexion failed", "Check url, username and password, and log out windows")
            
        if restService.connected:
            cookies = restService.getCookies()
            sessionCookie = cookies[0]
            
            # #Connect server tab
            serverPanel = ranchbe.getMainApp().serverPanel
            serverPanel.webView.sessionCookie = [sessionCookie[0], sessionCookie[1], config.configParser.get("rbserver", "host")]
            serverPanel.webView.connect()

            localPanel = ranchbe.getMainApp().localPanel
            localPanel.refreshAction()
            
            self.mainWin.ui.statusbar.showMessage("Connected")
            if self.mainWin.context.containerId:
                self.mainWin.context.load(self.mainWin.config)
            else:
                self.mainWin.context.statusConnectedEmptyContext()
            
            user = ranchbe.getService("user").getCurrent()
            self.mainWin.context.setUser(user)
        else:
            pass
        
        self.rootWidget.close()


# #
#
class ConfigDialogBox(Ui_Config):

    # #
    #
    def __init__(self, mainWin):
        super(Ui_Config, self).__init__()
        self.mainWin = mainWin
        self.rootWidget = QDialog()
        self.configParser = None
        self.config = None
    
    def setupUi(self):
        Ui_Config.setupUi(self, self.rootWidget)
        
    def connect(self):
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.save)

    def showme(self):
        self.rootWidget.exec_()
    
    def load(self, config):
        self.config = config
        configParser = config.configParser
        self.wildspacePath.insert(configParser.get("workspace", "path"))
        self.rbUrl.insert(configParser.get("rbserver", "url"))
        self.rbUser.insert(configParser.get("rbserver", "user"))
        
        try:
            if(configParser.getboolean("rbserver", "autoloadRbData")):
                self.autoloadRbDataCb.setChecked(True)
            else:
                self.autoloadRbDataCb.setChecked(False)
        except:
                self.autoloadRbDataCb.setChecked(False)

        if(configParser.getboolean("proxy", "use")):
            self.proxyUse.setChecked(True)
        else:
            self.proxyUse.setChecked(False)

        self.proxyHost.insert(configParser.get("proxy", "host"))
        self.proxyUser.insert(configParser.get("proxy", "user"))
        self.proxyPassword.insert(configParser.get("proxy", "password"))
        self.proxyPort.insert(configParser.get("proxy", "port"))

    def save(self):
        configParser = self.config.configParser
        
        wildpsace = self.wildspacePath.text()
        if not os.path.exists(wildpsace):
            self.mainWin.warningBox("Configuration warning", wildpsace + " is not existing")
            
        configParser.set("workspace", "path", self.wildspacePath.text())
        
        qurl = QUrl(self.rbUrl.text())
        configParser.set("rbserver", "url", qurl.toString())
        configParser.set("rbserver", "basepath", qurl.path())
        configParser.set("rbserver", "scheme", qurl.scheme())
        configParser.set("rbserver", "host", qurl.host())
        configParser.set("rbserver", "port", str(qurl.port()))
        
        # #
        if self.autoloadRbDataCb.checkState() > 0:
            autoloadRbData = True
            configParser.set("rbserver", "autoloadRbData", "True")
        else:
            autoloadRbData = False
            configParser.set("rbserver", "autoloadRbData", "False")
            
        try:
            self.mainWin.localPanel.autoloadRbData = autoloadRbData
        except:
            pass
        
        configParser.set("rbserver", "user", self.rbUser.text())
        if self.proxyUse.checkState() > 0:
            configParser.set("proxy", "use", "True")
        else:
            configParser.set("proxy", "use", "False")
        configParser.set("proxy", "host", self.proxyHost.text())
        configParser.set("proxy", "user", self.proxyUser.text())
        configParser.set("proxy", "password", self.proxyPassword.text())
        configParser.set("proxy", "port", self.proxyPort.text())
        
        self.config.save()


# #
#
class HttpInterceptor(QtWebEngineCore.QWebEngineUrlRequestInterceptor):
    
    # #
    #
    def __init__(self, parent=None):
        super().__init__(parent)

    # #
    # @var QtWebEngineCore.QWebEngineUrlRequestInfo info
    # @return void
    #
    def interceptRequest(self, info):
        logging.debug(info.requestUrl())


# #
#
class WebView(object):
    
    # # @var QMainWindow
    mainWin = None
    
    # #
    #
    def __init__(self, webEngineView, ranchbe):
        self.cookies = []
        
        """ @var QtNetwork.QNetworkCookie """
        self.sessionCookie = None
        
        self.ranchbe = ranchbe
        
        """ @var QtWebEngineWidgets.QWebEngineView """
        self.webEngineView = webEngineView
        
        self.webEngineView.page().profile().clearHttpCache()
        
        self.connected = False
        
    # #
    #
    def clearHttpCache(self):
        self.webEngineView.page().profile().clearHttpCache()
        return self

    # #
    #
    def getWebEngineView(self):
        if self.webEngineView == None:
            self.webEngineView = QtWebEngineWidgets.QWebEngineView()
        return self.webEngineView
    
    # #
    #
    def setCookie(self, name, value, domain):
        webEngineView = self.getWebEngineView()
        cookieStore = webEngineView.page().profile().cookieStore()
        sessionCookie = QtNetwork.QNetworkCookie()
        sessionCookie.setName(bytes(name, 'ascii'))
        sessionCookie.setValue(bytes(value, 'ascii'))
        sessionCookie.setDomain(domain)
        sessionCookie.setSecure(False)
        sessionCookie.setHttpOnly(False)
        cookieStore.setCookie(sessionCookie)
        return self
    
    # #
    #
    def cleanupCookie(self):
        webEngineView = self.getWebEngineView()
        cookieStore = webEngineView.page().profile().cookieStore()
        cookieStore.deleteAllCookies()
        return self
    
    # #
    # @var QtNetwork.QNetworkCookie cookie
    def onCookieAdded(self, cookie):
        logging.info("WEBVIEW Cookie name", bytearray(cookie.name()).decode())
        logging.info("WEBVIEW Cookie value", bytearray(cookie.value()).decode())
        logging.info("path", cookie.path())
        logging.info("domain", cookie.domain())
        logging.info("session cookie", cookie.isSessionCookie())
        logging.info("expirationDate", cookie.expirationDate().toString())
        logging.info("secure", cookie.isSecure())
        logging.info("httponly", cookie.isHttpOnly())
        return self
    
    # #
    #
    def connect(self):
        self.cleanupCookie()
        webEngineView = self.getWebEngineView()
        webEngineView.page().loadFinished.connect(self.onLoadFinished)
        profile = webEngineView.page().profile()
        profile.cookieStore().cookieAdded.connect(self.onCookieAdded)  # @var QWebEngineCookieStore
        profile.downloadRequested.connect(self.onDownload)
        
        qurl = self.ranchbe.getQurl('/home/splash', {'layout':'popup'})
        self.setCookie(self.sessionCookie[0], self.sessionCookie[1], self.sessionCookie[2])
        self.load(qurl, "get")
        
        return self

    # #
    #
    def onLoadFinished(self, ok):
        if not ok:
            logging.info("load failed")
        urlPath = self.getWebEngineView().url().path()
        logging.info("onLoadFinished:" + urlPath)
        basepath = self.ranchbe.config.get("rbserver", "basepath")
        urlPath = urlPath.replace(basepath, "")
        
        self.getWebEngineView().page().runJavaScript("$('nav.navbar').remove();")
        
        if urlPath == "/home/splash":
            pass
        elif urlPath == "/ged/document/lockmanager/checkin":
            pass
        elif urlPath.startswith("/ged/document/smartstore/store"):
            self.getWebEngineView().page().runJavaScript("Ranchbe.wicoti();")
        elif urlPath.startswith("/ged/project/detail"):
            self.getWebEngineView().page().runJavaScript("Ranchbe.wicoti();")
        elif urlPath.startswith("/ged/container/detail"):
            self.getWebEngineView().page().runJavaScript("Ranchbe.wicoti();")
        elif re.match("/ged/document/manager/[a-z]+/edit/[0-9]+", urlPath) != None:
            pass
        elif urlPath.startswith("/ged/document/detail"):
            self.getWebEngineView().page().runJavaScript("Ranchbe.wicoti();")
        elif urlPath.startswith("/ged/docfile/version"):
            self.getWebEngineView().page().runJavaScript("Ranchbe.wicoti();")
        elif urlPath.startswith("/docflow"):
            self.getWebEngineView().page().runJavaScript("Ranchbe.wicoti();")
        elif urlPath.startswith("/service/application/auth"):
            self.getWebEngineView().page().runJavaScript("Ranchbe.wicoti();")
        elif urlPath.startswith("/auth/logout"):
            pass
        elif urlPath.startswith("/auth/login"):
            pass
        elif urlPath.startswith("text/html"):
            pass
        else:
            logging.info("blocked page: " + urlPath)
            self.ranchbe.webSplashScreen(self.getWebEngineView().page())
            # #self.getWebEngineView().page().setHtml("<html><body><h1>This page can not be explored by RbCadConnector :"+urlPath+"</h1></body></html>")
            return

    # #
    #
    def disconnect(self):
        qurl = self.ranchbe.getQurl("/auth/logout", {})
        self.load(qurl, "get")
        self.getWebEngineView().page().setHtml("<html><body><h1>Disconnected</h1></body></html>")
        self.connected = False
        self.cleanupCookie()
        return self

    # #
    #
    def load(self, qurl, method="get"):
        webEngineView = self.getWebEngineView()
        request = QtWebEngineCore.QWebEngineHttpRequest()
        
        if method == "post":
            logging.info(qurl.query())
            if(qurl.hasQuery()):
                a = QByteArray()
                a.append(qurl.query())
                request.setPostData(a)
                qurl.setQuery("")
            request.setMethod(QtWebEngineCore.QWebEngineHttpRequest.Post)
            request.setHeader(b"Content-type", b"application/x-www-form-urlencoded")
        else:
            request.setMethod(QtWebEngineCore.QWebEngineHttpRequest.Get)
            
        request.setUrl(qurl)
        webEngineView.load(request)
        return self

    # # @param QWebEngineDownloadItem
    def onDownload(self, download):
        oldPath = download.path()
        suffix = QFileInfo(oldPath).suffix()
        path, _ = QFileDialog.getSaveFileName(self.webEngineView, "Save File", oldPath, "*." + suffix)
        if path:
            download.setPath(path)
            download.accept()        


class Context():

    # #
    #    
    def __init__(self, mainWin):
        self.mainWin = mainWin
        self.config = None
        self.user = None
        self.container = None
        self.containerId = None
        self.spacename = None
        
    # #
    #
    def setSpacename(self, spacename):
        self.spacename = spacename
        self.config.set("workspace", "spacename", spacename)
        self.mainWin.ui.menuSpacename.setTitle("You are Working In " + spacename.title())
        try:
            self.mainWin.testConnexion()
            self.mainWin.localPanel.refreshAction()
        except ConnException:
            pass
        return self

    # #
    #
    def setContainer(self, container):
        self.containerId = container.id
        self.config.set("workspace", "containerId", container.id)
        self.container = container
        self.setSpacename(container.spacename)
        return self

    # #
    #
    def setUser(self, user):
        self.user = user
        return self

    # #
    #
    def load(self, config):
        self.config = config
        # spacename = config.get('workspace', 'spacename')
        # self.setSpacename(spacename)
        # self.containerId = config.get('workspace', 'containerId')
        
        try:
            self.mainWin.testConnexion()
            # ranchbe = self.mainWin.ranchbe
            # containerService = ranchbe.getService("container")
            # self.container = containerService.getEntity(self.containerId, spacename)
            # self.statusActif(self.container)
        except ConnException:
            self.statusNotConnected()
        except Exception as e:
            logging.debug(str(e))
            
        return self
    
    # #
    #
    def save(self):
        if self.container:
            self.statusActif(self.container)
        self.config.save()
        return self

    # #
    #
    def statusNotConnected(self):
        msg = "<h1>Not Connected</h1>"
        self.mainWin.ui.contextInfos.setText(msg)

    # #
    #
    def statusConnectedEmptyContext(self):
        msg = "<h3>Connected to Ranchbe, but none container selected</h3>"
        self.mainWin.ui.contextInfos.setText(msg)

    # #
    #
    def statusActif(self, container):
        msg = '<span>You are working in Container : </span>'
        msg = msg + '<span style="font-size:15pt; font-weight:600; color:#aa0000;">' + container.spacename + '/' + container.name + '</span>'
        self.mainWin.ui.contextInfos.setText(msg)
