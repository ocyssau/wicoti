SET mypath=%~dp0
cd %mypath:~0,-1%
pyuic5 ./QtDesigner/MainWindow.ui > src/MainWindowQt5.py
pyuic5 ./QtDesigner/ConfigDialogBox.ui > src/ConfigDialogBoxQt5.py
pyuic5 ./QtDesigner/ConnexionDialogBox.ui > src/ConnexionDialogBoxQt5.py

pyrcc5 ./QtDesigner/Main.qrc > src/Main_rc.py

pause
