[Setup]
AppName=wicoti
AppVersion=1.1
AppCopyright=Sier 2019
AppId={{49243C9C-86BA-4464-8512-6B518B8E3208}
ShowLanguageDialog=no
DefaultGroupName=Ranchbe
VersionInfoVersion=1.1
VersionInfoCompany=Sier
VersionInfoDescription=Connector to ranchbe server
MinVersion=0,6.1
DefaultDirName={pf}\wikoti
OutputDir=C:\Users\o_cyssau\git\wicoti
OutputBaseFilename=wicotiSetup1.1
AppPublisher=Sier-Olivier Cyssau
AppPublisherURL=https://bitbucket.org/ocyssau/wicoti/wiki/Home
AppSupportURL=https://bitbucket.org/ocyssau/wicoti/wiki/Home
AppUpdatesURL=https://bitbucket.org/ocyssau/wicoti/downloads/
RestartApplications=True
UninstallDisplayName=Ranchbe-Wicoti
UninstallDisplayIcon={uninstallexe}
VersionInfoProductName=Wicoti
VersionInfoProductVersion=1.1
VersionInfoProductTextVersion=1.1

[Files]
Source: ".\dist\Wicoti\*"; DestDir: "{app}\Wicoti"; Flags: ignoreversion createallsubdirs recursesubdirs
Source: ".\dist\data\*"; DestDir: "{app}\data"; Flags: ignoreversion createallsubdirs recursesubdirs
Source: ".\src\config.dist.ini"; DestDir: "{localappdata}"; DestName: "config.ini"

[Icons]
Name: "{group}\Ranchbe"; Filename: "{app}\Wicoti\Wicoti.exe"; WorkingDir: "{userappdata}"
